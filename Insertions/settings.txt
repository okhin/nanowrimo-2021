{
    "autoSave": false,
    "autoSaveDelay": 5,
    "autoSaveNoChanges": true,
    "autoSaveNoChangesDelay": 5,
    "corkBackground": {
        "color": "#000000",
        "image": "spacedreams.jpg"
    },
    "corkSizeFactor": 100,
    "corkStyle": "new",
    "countSpaces": true,
    "defaultTextType": "md",
    "dict": "PyEnchant:en_US",
    "dontShowDeleteWarning": false,
    "folderView": "cork",
    "frequencyAnalyzer": {
        "phraseMax": 5,
        "phraseMin": 2,
        "wordExclude": "a, and, or",
        "wordMin": 3
    },
    "fullScreenTheme": "spacedreams",
    "fullscreenSettings": {
        "autohide-bottom": true,
        "autohide-left": true,
        "autohide-top": true,
        "clock-show-seconds": true,
        "progress-auto-show": true,
        "title-show-full-path": true
    },
    "lastTab": 0,
    "openIndexes": [
        0,
        [
            "39"
        ],
        null
    ],
    "outlineViewColumns": [
        0,
        5,
        8,
        9,
        11,
        12,
        13,
        7
    ],
    "progressChars": false,
    "revisions": {
        "keep": false,
        "rules": {
            "2592000": 86400.0,
            "3600": 600.0,
            "600": 60.0,
            "86400": 3600.0,
            "null": 604800.0
        },
        "smartremove": true
    },
    "saveOnQuit": true,
    "saveToZip": false,
    "spellcheck": false,
    "textEditor": {
        "alwaysCenter": false,
        "background": "#ffffff",
        "backgroundTransparent": false,
        "cursorNotBlinking": false,
        "cursorWidth": 1,
        "focusMode": "paragraph",
        "font": "Cantarell,11,-1,5,50,0,0,0,0,0",
        "fontColor": "#000000",
        "indent": false,
        "lineSpacing": 100,
        "marginsLR": 0,
        "marginsTB": 20,
        "maxWidth": 600,
        "misspelled": "#F00",
        "spacingAbove": 5,
        "spacingBelow": 5,
        "tabWidth": 20,
        "textAlignment": 0
    },
    "viewMode": "fiction",
    "viewSettings": {
        "Cork": {
            "Background": "Nothing",
            "Border": "Nothing",
            "Corner": "Label",
            "Icon": "Nothing",
            "Text": "Nothing"
        },
        "Outline": {
            "Background": "Nothing",
            "Icon": "Nothing",
            "Text": "Compile"
        },
        "Tree": {
            "Background": "Nothing",
            "Icon": "Nothing",
            "InfoFolder": "Nothing",
            "InfoText": "Nothing",
            "Text": "Compile",
            "iconSize": 24
        }
    }
}