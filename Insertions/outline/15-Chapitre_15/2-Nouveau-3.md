title:          Nouveau_3
ID:             66
type:           md
compile:        2
charCount:      5782



"Hey Kam, tu … tu veux passer la soirée avec moi ?" Demande Echo alors qu'elle accroche son harnais au hauban sur lequel Kam est installée pour souder la structure porteuse des Vasimr sur le hauban central qui accuillera réservoirs et radiateurs cylindriques une fois l'assemblage fait.

"Ben, oui. Bien sûr. Pourquoi tu demande ?" Répond Kam, concentrée sur le faisceau de plasma qui recouvre les structures métalliques d'une couche supplémentaires de particules métalliques chargées.

"Parce que je suis pas bein du tout. J'étais dans les serres. Et ça va pas."

"D'accord. Ben oui, bien sûr que je veux bien passer la soirée avec toi." Dis Kam, continuant son minutieux travail de soudure "Et tu sais … Peut-être que Yas à raison sur le fait qu'on a pas la possibilité d'aller mal dans l'espace, pas sans mettre tout le monde en danger. Limite, j'en suis l'illustration parfaite." Ajoute-t-elle, se remémorant l'état dans lequel elle étais quand elle a calculé le vecteur d'approche de Rock. "Mais en fait, je suis pas d'accord. Et tu sais quoi, j'ai envie de prouver à ta mère qu'elle se plante. Et aux autres."

"Qu'est-ce qui te fais penser qu'on peux?"

"Fuck My Lives. Tout est dedans. Je sais tu va me dire, je l'écoute trop" plus de quatre vingt écoutes ces deux dernières semaines, c'est plus que trop pour certains personnes, mais c'est juste parfait comme musique, comme texte, comme expérience, surtout en ce moment. "Bref. En dépit de tout ce qui lui est arrivé, et du fait qu'elle a complètement craqué, qu'elle est cassée, elle est toujours là et elle a survécu. Je suis pas sûre de savoir pourquoi, mais c'est possible. Et même si notre relation est un peu étrange en ce moment, je sais qu'on pourra s'en sortir tant qu'on est là avec l'autre. Et moi j'ai pas l'intention de bouger, donc c'est à toi de voir."

"Song for Peg" répond Echo, remarquant que kam lui récite les paroles du refrain.

"Oui. Et ?"

"Rien. T'as besoin d'un coup de main ?"

"Si par coup de main tu veux parler de mettre tes doigts partout sur moi, oui. Si tu veux vraiment aider cela dit, on a du parallélisme et de l'interférence à gérer. Moi j'ai finit ici." Kam éteins le plasma, laissant les structures rougeoyante irradier leur chaleur dans le vide spatial.

"Hey, ça te va si on se pose deux minutes ici, à regarder les étoiles ? Comme quand on étais toutes les trois au début ?" Demande Echo, attrapant la main recouverte d'un gant lourd de Kam. Ça fait une éternité qu'elles ne se sont pas posées à regarder la voix lactées, les galaxies, les planètes et les nébuleuses, à se demander quelles sont celles qui ont déjà explosées ou se sont effondrées et dont elle ne voient que le souvenirs lumineux traversant les gigantesques étendues pas si vide que ça du cosmos avant de leur parvenir, souvenir glissant parfois dans le rouge alors qu'elles s'éloignent de la source du rayonnement à une vitesse supérieure à celle de la lumière.

Collant son casque à celui d'Echo — ce qu'il y a de plus proche d'un câlin quand on est en combi lourde — Kam la prend dans les énormes bras de sa combinaison adaptée à sa morphologie. Elle est encore un peu maladroite par moment, mais elle commence à se sentir bien pour la première fois dans une combinaison lourde. Et aussi à sentir le volume de la combinaison d'Echo, callée sous les assistances hydrauliques du harnais externe.

Shi lui manque aussi, surtout dans ces moments là. Quand elles réduisaient leur monde à juste elles trois, et que plus rien d'autre n'avait vraiment d'importance. Kam n'est cependant pas autant impliquée émotionnellement que Echo, elle n'a jamais vraiment su comment faire, et elle ne comprends pas forcément l'intensité de la douleur que ressent Echo. Et elle se sent un peu à l'écart parfois. Mais elle reste là, chacune gère comme elle peut. Sa façon de gérer est de faire payer les corpos. Probablement. La radicalisation politique, l'action directe.

Ou c'est ce qu'elle pensait ces derniers mois, mais elle ne veut pas abandonner Echo, elle veut rester avec elle, et donc, elle ne veut pas prendre le risque de se faire arrêter ou  pire. Elle cherche donc à construire quelque chose. Et si Shi n'est plus là pour faire le support psy, alors elle peut essayer.

"Hey les filles, vous faites quoi ?", la voix de Peg dans les communicateurs. Elle a terminé l'assemblage de son côté de la config de Sandy. Kam cherche le regard d'Echo qui articule un silencieux "OK" de l'autre côté de sa visière.

"Rien. On regardait les étoiles." Répond Kam. Se demandant si elle a envie que Peg vienne les rejoindre et donc lui laissant l'initiative.

"Oh, bonne idée." Répond Peg, que Kam s'imagine déjà se mettre en route. "Mais … Vous voulez peut-être rester tranquille toutes les deux ?" Peg attrape la sangle de déambulation qui est encore inerte, et commence à remonter vers les modules de tête.

"Non, c'est bon. Viens." Après tout, elles font aprties du même équipage. Du même pod. Des memes relations sentimentales, autant l'inclure dans ce genre de moment, sinon ça n'as pas de sens. "Nyasid, si tu veux venir, tu peux aussi." Mais Kam sait qu'il ne répondra pas. Il est … trop en décallage avec elles pour rester, et elles n'arrivent pas à l'inclure.

"Merci de l'invitation. Mais … " Une hésitation. Se pourrait-il qu'il surprenne le reste de l'équipage. "Non. Je … Je vous laisse pour ce soir, mais faudra qu'on parle plus tard. D'accord ?"

"D'accord, prend soin de toi". C'est Peg qui a répondu, avant que Kam ne réagisse. Echo reste muette, a fixer les étoiles, à voyager là-bas, loin de toute cette douleur. Elle sait que ça finira par passer, mais pour le moment, elle a just eenvie de s'échaper. Avec kam, Shi — ou son souvenir — Peg et n'importe qui d'autre qui veut bien faire un bout de chemin avec elle.