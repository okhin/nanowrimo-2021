title:          Nouveau_2
ID:             65
type:           md
compile:        2
charCount:      4666



Faisant face à une des micro-serres des modules d'environnement, Echo essaye de sortir de ses rêveries. Ça n'a jamais été trop son truc les hydroponiques, mais il faut bien que quelqu'un s'en charge. Et pour le moment c'est Nyasid qui s'en ocucpe. Il a établi un motif qui permet une bonne répartition de l'Azote, mais ce n'est pas comme ça que Shi aurait fait. Echo n'y connait pas grand chose en hydroponique, mais elle essaye de se rappeler ce que lui a appris Shi lors de leurs heures passées ensemble dans les serres de KRI.

Et tout ce qu'elle sait c'est que ce n'est pas comme ça qu'il faut faire. Elle devrait pouvoir sortir une guilde suffisante pour un bon cycle d'Azote, mais son esprit est accaparé par le fait qu'elle doit se confronter qu fait que plus jamais elle ne pourra voir ces bacs buissonants de fleurs, de micro-insectes, de fruits, de feuilles, avec les associations complexes qui étaient la signature de Shi.

Il lui faut quelques minutes pour réaliser qu'elle pleure, les graines qu'elle essayait de planter roulant sur le sol, ses jambes tremblantes peinant à la soutenir dans la micro gravité de la corolle de la configuration, laissant échapper entre des sanglots des "C'est pas comme ça qu'il faut faire" autant destiné à Nyasid qu'à elle même.

Ce dernier, ayant accroché ses outils de travail à son harnais, contourne doucement la partie ouverte du sol et s'approchant d'Echo s'accroupit légèrement pour se mettre à sa hauteur.

"D'accord. On fais pas comme ça." Lui dit-il doucement. "Kam m'a prévenue que ça pourrait arriver, c'est OK, prend le temps et dis-moi ce qu'il ne faut pas faire, d'accord ?" Echo se blottit contre lui, à défaut de quelqu'un d'autre, il a le mérite d'être là.

"Je sais pas. je sais plus. Elle, elle savait. Et … Et j'ai pas envie qu'elle disparaisse, j'ai pas envie qu'elle ne soit plus là, et tu fais peut-être bien, mais c'est pas comme ǎ qu'elle aurait fait." La douleur laissé par le vide dans les émotions d'Echo irradie dans le reste de sa psychée, évacuant l'énergie emmagasinée par Echo pour ignorer le problème. Elle se cramponne au bras de Nyasid, plantant ses doigts dans le tissus épais de sa combinaison souple, couverte de poussière à cause des plantations.

Nyasid grimace légèrement sous la douleur de la poigne d'Echo, habituée à serrer ls contrôles de fronds orbitales en haute gravité. Mais il la laisse pleurer. Par convection, la douleur d'Echo se déverse sur lui, et à eux deux ils peuvent dissiper suffisament de cette douleur qui paralyse Echo et la maintiens dans un étât d'immédiateté, incapable de voir à long terme.

Les sanglots d'Echo se calment, et sa poigne se décrispe, laissant le sang circuler de nouveau librement dans le bras de Nyasid. "Bon, ça va mieux ?" demande-t-il. "Pas vraiment. Si. Un peu." Répond-elle.

"Bon. Alors il va falloir qu'on fasse quelque chose toi et moi. On va pas effacer Shi de ce vaisseau d'accord, j'en ai pas envie. Je ne la connaissait pas personnellement, mais elle est importante pour toi. Donc on va faire des jardins comme elle l'aurait voulu. À ta sauce, à la mienne, mais en partant de ce qu'elle faisait. Ça te va ?"

Echo hoche la tête, essuyant ses larmes. Elle ne sait pas forcément comment faire, mais elle veut le faire. Au moins comme un homage, comme un endroit où se souvenir. Ou passer des moments, peut-être même des bons moments un jour. Plus tard. En attendant il va falloir relire les notes de Shi, et Echo ne se sent pas la force de continuer à faire face à son fantôme.

"Je … Je peux te laisser t'en occuper ?" Dis Echo, toujours enfoncé dans les bras du jardinier / logisticien.

"Oui, absolument. J'ai les schémas qu'elle a publié. Je vais partir de ça. Et va te dégourdir les pattes quelque part où tu ne seras pas saturées de souvenirs de Shi. Tu peux rester si tu veux, tu ne me gènes pas, mais tu as besoin de changer d'air là je pense."

Et alors qu'Echo sort du module environnemental, Nyasid, tout en se téléchargeant les notes de Shi dans son module de poignet, se demande si il a raison de rejoindre cet équipage dont le point central semble etre en pleine rupture — à raison, mais en état de rupture tout de même. Il a beaucoup d'affection pour Peg, mais c'est juste une pote. Et il ne sait pas si il a envie de devoir faire partie de tout ça.

Mais jusqu'ici c'est un peu le seul éqauipage du coin qui semble prêt à ne pas chercher à reconstruire, et juste à partir, à bouger, à faire quelque chose. Et peut-être que le prix à payer pour vouloir faire changer les choses, c'est se faire casser en morceau. Et plus que de devoir gérer les problèmes des autres, c'est cette perspective qui lui fait peur.
