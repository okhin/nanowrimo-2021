title:          Nouveau
ID:             63
type:           md
compile:        2
charCount:      5911


"Oui, on fais attention à nous." Echo s'est isolée dans ce qui sera le nœud central de la configuration Sandy et elle discute avec Yas. Ça fait trois semaines qu'elle refuse de prendre l'appel, mais Kam et Peg ne lui laissent plus trop le choix. Donc elles l'ont laissée seule, pendant qu'elle s'occupent à ramener le double Vasmir à opposition de phase qui servira de propulsion à l'ensemble. Double Vasmir que Yas leur a permis de śecuriser. C'est une tech que même PIS n'as pas encore réussi à produire à un prix abordable pour les spaceux, et seuls les personnes ayant accès à suffisament de connectivité avec les milieux des puits — comme sa mère — peuvent mettre la main sur des modèles suffisament puissant pour faire autre chose que juste frimer.

"D'accord, d'accord, si tu le dis, je te fais confiance." Le visage de Yas recule légèrement dans le champ de la caméra, alors qu'elle recule légèrement, illustrant le fait qu'elle abandonne ce sujet. Et donnant à Echo la sensation de se voir dans un miroir. Pas un miroir exact, certaines de ses caractéristiques physique sont issues de ses autres parents, mais les caractéristiques philogénétique de Yas sont celles qui ont été princpalement utilisées pour sa conception. Elle n'est légalement pas un clone de Yas, mais c'est vraiment pour jouer avec la paperasse et éviter les sanctions.

Mais surtout, cette image la renvoit au fait que ses parents ont eu — ou dans le cas de Yas ont toujours — vécu dans la sphère d'unfluence des puits et amassé une petite fortune. La vie a fait que le pod s'est doucement éclaté, mais ils gardent contact et Yas est essentiellement la banque du groupe social. En échange de quoi elle se permet généralement de donner son avis sur tout, notamment sur la façon dont les autres vivent leur vie.

"Dis moi, reprend Yas, je sais qu'on est pas dans les meilleurs termes, et j'aimerai un jour que ce ne soit plus le cas, mais je sais ce que c'est de perdre quelqu'un de proche. Et je te souhaite d'être entourée de personnes qui peuvent t'aider à survivre à ça." Tiens, un changement de tactique de la part de Yas, se demande Echo, ou alors elle est vraiment intéressée par sa vie ?

"Ouais, ça va. Je pense que je leur tape un peu sur le système là. Et … "

"Comme ton père." Le problème des communications directes, c'est qu'on peut interrompre l'autre. Et Yas étant quelque part à la surface de 1-Cérès — probablement dans un des spas spatiaux qui font penser aux puiseux qu'ils savent ce qu'est la vie dans l'espace — elle peut interrompre Echo dans ses messages. "Pas qu'il me tapait particulièrement sur le système. Mais il en était persuadé. Et il a essayé de changer ça, de gérer ce qu'il pensait être des problèmes seul, avec les outils qu'il a pu trouvé et il a commencé, petit à petit à essayer de corriger sa personnalité." Le timbre de Yas sautille un peu trahissant l'émotion qui l'assaille.

"Je ne suis pas mon père hein, je fais mes propres choix."

"J'en suis parfaitement consciente. Mais il n'empêche, demande de l'aide. Tu en as le droit. Tu n'es pas un poids pour ceux et celles qui t'aime, même si ce n'est pas simple. Je sais que vous ne pouvez pas trop vous permettre de prendre le temps du deuil, ou ce genre de choses, pour ne pas mettre en danger l'équipage, mais essaye au moins de ne pas effacer ta personnalité."

"Et comment tu saurais ce qu'on est pas censé faire hein ? Tu fout jamais le pied en orbite."

"Et ça me manque. Mais je ne peux pas. Je ne peux pas me mettre dans une situation où il suffit que je baisse ma vigilance pour mourir. Parce que c'est exactement ce qu'il se passera à terme. Il suffit que je m'arrête et que je me pose et de laisser l'espace et l'entropie faire le travail pour que je me laisse mourir. Dans les puits, dans les villes, mettre fin à ses jours demande un peu plus d'implication." C'est la première fois que Yas parle de ses pensées suicidaires avec Echo. C'est pas quelque chose qu'Echo ignorait, Pierre et Ark l'ont évoqués à plusieurs reprises, et c'est aussi pour ça qu'Echo garde le contact avec Yas, indépendamment de leurs différences de vues et d'opinions.

Un silence pesant s'installe, à peine perturbé par le léger bruit électromagnétique ambiant.

"Tu veux en parler ?" Demande Echo, ne sachant pas vraiment si elle même veut en parler.

"Oui, mais plus tard. Et ne t'inquiètes pas trop pour moi. Je suis peut-être, comment dis-tu, une pimbêche conservatrice piègèe par la gravité, mais je suis une grande fille aussi et je voit une thérapeute assez efficace." C'est bien un truc de puiseux ne peut s'empecher de penser Echo, avoir une personne intégralement dédiée à la santé mentale. Dans les communautés spatiales, on a pas le choix de se reposer sur les assistants et le reste de l'équipage. C'est pour ça que tous les équipages sont généralement construit comme des structures sociales, et non pas comme des entreprises. Pas de contrat de primes, pas de recrutement. On vole avec les personnes avec qui on vit.

"Bon. Tant que tu as ce qu'il te faut. Il va falloir que j'y aille, histoire de voir comment Peg s'en sort avec une soudeuse à plasma, et vérifier qu'elles ne bousille pas les Vasmir que tu nous as fournis. Merci pour ça au fait."

"D'accord. Réfléchit à ce que je t'ai dit d'accord ? Essaye de trouver un peu de temps pour prendre soin de toi. Je n'ai pas envie de voir encore une autre personne a qui je tiens laisser sa personnalité se déliter pour pouvoir faire face au vide."

"Oui, promis, ne pas finir distante comme Ark. Noté. À plus." Et Echo termine la communication avant que Yas ne puisse répondre quoi que ce soit. Elle a raison, ce qui énerve Echo d'autant plus, parce qu'il va bien falloir qu'elle essaye de gérer la perte de Shi, au lieu de refuser de s'y confronter, en restant dans l'urgence.

Mais en attendant, il va bien falloir finir cette configuration, et toutes les mains sont nécessaires pour ça.
