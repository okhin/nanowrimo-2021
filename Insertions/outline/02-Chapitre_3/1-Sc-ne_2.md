title:          Scène 2
ID:             13
type:           md
compile:        2
charCount:      3029



Elle n'as pas le temps de réfléchir aux questions métaphysiques avant d'être ramenée à la brûlante réalité, alors qu'une ampoule d'adrénaline viens d'être vidé dans son système sanguin.

La première chose dont elle se rend compte est l'absence de gravité. La seconde est que son assistant est de retour en ligne.

"La prochaine fois que tu veux manger des G, tu te limites, ça t'éviteras de tomber dans les pommes. La bonne nouvelle c'est que je pense que tu as suffisament perturbé la horde. je suppose que tu n'as rien à foutre du taff que ça va nécessiter et …"

La voix s'arrête au moment où Ikomi arrache l'unité de stockage de la console de bord du Hit Me.

"Tu fais moins la maligne maintenant hein ? Connasse."

"Oui, mais ça ne règleras pas tes problèmes."

Bordel, des hallucinations. Manquait plus que ça pense Ikomi.

"Non. C'est … Écoute, laisse les contrôlleur approcher ton véhicule et le ramener sur une orbite stable d'accord ?"

"Orbite stable?" Ikomi affiche une entoptique avec sa trajectoire. Vitesse de libération atteinte. " Merde."

"Non. Tu n'as pas eu cette idée."

Et pourquoi pas ? Ikomi pourrais en profiter pour partir, sa trajectoire l'éjecte certes de la ceinture, mais au moins elle devrait être débarassée de la horde.

"Et tu vas manger quoi?"

Le système de survie est fait pour ça. Ya des barres de protéines dans le caisson de bord.

"Non. Non, tu n'as plus ces barres, tu les as balancées à la tronche du mec qui voulait je ne sais pas quoi. Mais bref, tu n'as pas de quoi manger. Tu as à peine un recycleur d'air et un système de cyclage d'eau grise. Ta fronde c'est plus pour faire joli que pour aller vraiment dans l'espace."

Mais c'est pas ça le vrai problème. Le vrai problème c'est qu'elle est dans un état mental catastrophique et qu'elle parle à une version de son assistant qui est soit une hallucination, soit un système implanté dans sa conscience assemblée à partir de fragments et de souvenirs d'autres version d'elle-même.

"Oui, c'est un peu l'idée. Mais oui, tu es aussi complètemet folle. Et tu sais que même si tu trouverais de quoi maintenir ton corps en fonctionnement par je ne sais quelle méthode de pseudo-zen que tu affectionnes, psychologiquement tu tiendra pas dix minutes."

Déjà, le pseudo-zen, Ikomi ne sais même pas si elle aime ça. Si ça se trouve c'est encore un script.

"Tu peux penser ça oui. Il n'empêche que tu aimes ça. Écoute, il faut vraiment que tu retourne dans la zone d'influence de 1-Cérès. On peut modifier les scripts. Laisse moi accéder aux communications."

L'intonation de l'assistant à changé. Peut-être est-ce pace qu'il n'existe que dasn sa tête et que, par habitude, Ikomi préfère parler avec lui de ses problee et lui donner une voix. Ou peut-être est-il réellement et authentiquement inquiet de la santé physique d'Ikomi. Une boucle de feedback qui a du s'activer quelque part, un équilibre rompu entre le potentiel gain pécunier par la marchandisation de ce scandale et le coût de perdre l'une de leur plus grande source de revenus.
