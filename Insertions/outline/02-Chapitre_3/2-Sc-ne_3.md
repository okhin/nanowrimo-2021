title:          Scène 3
ID:             14
type:           md
compile:        2
charCount:      4241



Message entrant. De l'audio uniquement. De Peg. La seule personne qui compte un peu pour Ikomi. Ou qui est censée compter pour elle.

"Hey. J'espère que tu auras ce message. J'imagines que tu ne sais plus où tu en es et, vu ta trajectoire, je suppose que tu as choisi de ne pas revenir." Une pause. Longue. "Je sais plus si la relation qu'on a, ou qu'on a eu est un script, un souvenir implanté, ou le seul truc un peu authentique et fou dans cette vie. Et je sais que Batang va probablement m'envoyer les flics de la PI au cul pour ça, mais fonce meuf. Arraches-toi d'ici. Va ailleurs. Surtout, surtout, ne reste pas juste pour moi d'accord ? Je vais m'en sortir ici. Toi pas." Une autre pause. "Je t'aime. Je crois." Le silence.

Une sensation qui vient de sa poitrine commence saisir Ikomi puis à prendre sa larynx, ses cordes vocales. Elle ne peut plus parler. Une force incomensurable semble la forcer à se mettre en boule. Une pression sur ses globes occulaires la force à pleurer. La tristesse est une force plus puissante que la gravité. Elle ne reverra probablement pas Peg, la police de la propriété intellectuelle ne plaisante que rarement avec les personnes qui assiste à l'évasion de propriété intellectuelle. Meme si c'est pour leur donner un simple message de soutien.

Elle ne peux plus faire demi-tour.

"Si. Tu peux. Tu as ce qu'il faut de carburant pour corriger ton orbite. Tu peux revenir."

"On sait bien que non." Jouant du manche et de ses centrales inerteilles, elle aligne le Hit Me sur son vecteur prograde et enclenche les injecteurs qui éjecte l'hydrogène qui sera surchauffé par la réaction de fisison de l'hexaflurorure d'uranium. 0.7G d'accélération, c'est bien assez.

Sa trajectoire change sur l'entoptique. Et la courbe s'élance de plus en plus loin. Deux minutes de combustion et déjà le Hit Me projette d'atteindre le système Jovien. Il faudrait cinq minutes de plus à ce rythme pour qu'Ikomi se retrouve sur une trajectoire de sortie de l'héliosphère, mais elle se retrouve à cours d'hydrogène un peu avant. La fin subite de l'accélération fait que le corps d'Ikomi est projettée contre son harnais, avant que l'inertie ne finisse par établir un consensus et que tout ce qui n'était pas attaché se soit retrouvé porjeté contre la coque avant.

"Merde."

La fin de ses réserves d'hydrogène n'indiquent pas la fin de ses réserves d'énergie, mais ce n'est pas une solution idéale. L'apesanteur aide ses idées à s'éclaircir, elle a toujours préféré ça à la micro grav ou, pire, à la gravité punitive des Puits. Les estimations de l'ordinateur de bord lui donne une semaine de courant. Plus en réduisant les systèmes de survie au minimum et en basculant les systèmes de com en mode d'urgence uniquement, transformant la pièce de technologie qu'est le Hit Me en une gigantesque brique capable d'à peine plus que d'émettre un SOS. Une gigantesque brique nucléaire.

Avant que son assistant — ou son subconscient  — n'agisse trop vite, elle se lance vers la zone de stockage d'urgence de ce cokcpit qui, maintenant qu'elle le réalise, n'est vraiment pas si grand pour y passer trop de temps. Surtout avec les entoptiques pincipalement désactivés et confronté au noir mat des surfaces habituellement habitées de décorations et de reproduction environnementale pour faciliter la navigation et limiter la perte d'orientation.

Amortissant légèrement son approche de son bras, elle ouvre le placard et sors le quitte d'injection ainsi que la combinaison de drogues d'urgence utilisés pour ralentir le métabolisme et faire durer les réserves. Elle charge un narcorythme avec les chem-mem des souvenirs de Peg. Paradoxalement, ce sont les derniers souvenirs qu'il lui restera d'elle, la dernière fois qu'elle pourra revivre ça, après ça,ils seront perdus. Elle mélange dans l'injecteur pressurisé le narcorythme et les hiberneurs avant de s'injecter le tout dans les veines, pour sombrer dans un espcèce de rêve plus ou moins conscient, habité des souvenirs que Peg lui avait dit de garder pour une occasion spéciale.

Et, pendant qu'Ikomi glisse lentement hors du temps et d'une perception de la réalité, le Hit Me, sur sa trajectoire de collision avec Jupiter, émet joyeusement et à qui veut bien l'entendre un SOS.