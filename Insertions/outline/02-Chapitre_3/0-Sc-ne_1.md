title:          Scène 1
ID:             6
type:           md
compile:        2
charCount:      5992


La horde ne lâche pas Ikomi. Elle a beau pousser le Hit Me à pleine puissance, ses trajectoires sont trop approximatives pour les semer. D'autant que le cœur n'y est pas vraiment. Enfin si. Mais là, elle en peut juste plus. Les chem-mem lui retournent la tête, elle le sait. Les scripts permanents, les assistants qui ne la lâchent jamais. Mais ça, elle peut gérer. Ou alors ce sont les chem-mem qui lui font penser qu'elle peut gérer. Mais elle sait qu'elle peut gérer.

Ce qu'elle ne peut pas gérer, c'est la horde. Des fans qui cherchent à la protéger des apparazzis. Des papparazzis qui cherchent à la pousser hors-script et à avoir une XP que personne d'autres n'a jamais eu. Les obsessionels qui veulent qu'elle partage leur amour, même si ce n'est pas d'elle qu'ils sont amoureux mais de ce construct qu'elle interpète depuis … Depuis combien de temps déjà ?

Depuis qu'elle a signé avec Shiitake Overdrive du moins. Elle a signé un contrat avec elle, ça elle s'en souvient, les chem-mem ne peuvent pas lui enlever ça. Mais les détails sont flous. Est-ce qu'elle a signé avec Batang Xao, son agent, ou avec X-Chris l'agent de la précédente incarnation d'Ikomi ? Ou encore avec quelqu'un d'autre ?

L'alarme de proximité hurle de toute la puissance dont elle est capable. Suivi rapidement après du bruit sourd de l'impact d'une combi dure sur la coque du Hit Me, envoyant celui-ci hors de sa trajectoire contrôlé en amorce de vrille. La conscience corporelle d'Ikomi s'allume soudainement, et elle se retrouve dans l'instant, aggripant les contrôles pour tenter de compenser la vrille.

Les gyroscopes sont immédiatement mis à contribution, et l'inertie qu'ils accumulent déplacent sensoblement le centre de masse du Hit Me, ralentissant la vrille alors qu'Ikomi tire sur les commandes mécaniques de stabilisation. Elle glisse sur une trajectoire en spirale, s'éloignant des chantiers d'assemblage qui peuplent l'orbite de 4-Vesta. Le blip de la combinaison dure qu'elle a impacté à lui moins de chance, les contrôlleurs d'attitude des combinaisons dures ne sont pas fait pour compenser ce genre de vrille. Tant pis pour lui pense Ikomi, ils n'avaient qu'à pas surcharger leurs packs de propulsions dans le seul but de rattraper le Hit Me.

Marre. Il faut que j'arrête.

"Appelle Batang Xao" demande-t-elle.

"Non. Je ne peux pas te laisser faire ça. Calmes-toi d'abord. Ranges-toi."

Lui répond son assistant, ses routines toutes concentrées à maintenir le script sur les rails et à préserver les intérêts de Shiitake Overdrive.

"Si je me ranges, c'est pour leur défoncer la tête à coup de pied."

"Tu sais très bien que tu ne pourras pas le faire. Ranges-toi."

Elle a raison. Manœuvrer sa fronde dans une poursuite chaotique dans des coins sans trop de risque est une chose, se confronter physiquement en zéro-G en est une autre. Et les chem-mem qui la font disjoncter l'empêcherotn de toutes façon de pouvoir se battre.

"Tu as deux choix. Tu te ranges et on peut encore suaver ce massacre. On a un script pour ça. Tu l'as déjà fait."

Le lâcher prise. Une crise de nerf, une fuite et une course poursuite jusqu'à un accident léger, des fans qui la sauve, elle qui baise avec eux dans une romance sans lendemain hautement médiatisée, et le retour à la routine avec les Jam's. Et l'XP de ses ébats en vente partout. Et sa tronche affichée partout, dans une marche de la honte permanente.

Elle l'a déjà fait. Mais là c'est pas pareil. Elle ne sait pas vraient pourquoi, mais c'est pas pareil. PEut-être que sa personnalité n'en peut plus de devoir bouffer tout ces trucs. Ou peut-être qu'elle aspire à autre chose qu'obtenir un énième contrat publicitaire de lingerie ou de sex toy ou de maquillage waterproof qui ne vous lâchera jamais.

"L'autre option c'est le plus gros crash. Avecs pertes civiles, arrestations et fin de contrat. Personne n'en entednra parler, tu seras mise à la porte avec interdiction d'utiliser les produits déposés par Shiitake Overdrive. Tu te plains de pas faire tes choix ? Ben voilà."

Ikomi sent l'exaspération dans la voix synthétique. Ça fait longtemps qu'elle a effacé toutes les options de customisation de son assistant, c'est comme ça qu'elle se défoule. Il n'as plus de routines d'empathie depuis un bail, plsu vraiment de personnalité non plus. C'est juste un agglomérats algorithmiques qui tourne en boucle sur des scripts et du droit des contrats.

"Oh, ta gueule." balance-t-elle, basculant l'assistant en hors-ligne. Il reviendra, Shiitake Overdrive le remts toujours en ligne. Mais au moins, Ikomi bénéficiera de quelques minutes seule avec elle-même. Et la horde qui lui colle au cul.

"Allez vous faire foutre", leur hurle-t-elle avant d'effectuer une rotation du Hit Me autour de son axe normal. Plein gaz, à contre-sens. L'ampoule nucléaire surchauffe, cramant des grammes de Thorium en quelques secondes, surchauffant les verres de la cloche d'attténuation, et écrasant Ikomi dans son fauteuil automorphe sous les G de l'accélération. Son écran de contrôle affiche les vitesses relatives de ses poursuivants qui grimpent rapidement dans les centaines de mètres secondes.

L'autopilote essaye de limiter les collisions, mobilisant les centrales inertielles, tirant de toutes leur inerties accumulées sur les commandes du palonier et du manche auquel Ikomi est cramponée. Mais elle maintient le cap. Toute sa rage, sa hargne, et celle de plusieurs autres est concentrées sur un unique point focal, droit devant elle, en plein vecteur prograde.

Au bout de quelques secondes de lutte contre le système mécanique, son système conscient abandonne sous la surcharge cognitive et l'éjecte du Hit Me. Ou du moins c'est la sensation qu'elle a. Elle nentend plus l'ampoule nucléaire vibrer et agiter le vaisseau. Elle n'entend pas le sifflement des batteries qui se déchargent pour alimenter les centrales inertielles. Elle n'entend plus son cœur battre. Pendant quelques instants elle est affranchie du poids écrasnt d'exister et d'être elle.
