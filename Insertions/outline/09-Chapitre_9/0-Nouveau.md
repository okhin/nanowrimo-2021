title:          Nouveau
ID:             42
type:           md
compile:        2
charCount:      4470


Le plasma d'hydrogène ionisé du WatUp freine le cargo pour le ramener à une vitesse relative au Unknown Artist permettant une approche précise. Sorti des chantiers spatiaux de PIS, le WatUp a une maniabilité bien supérieure à celle du Unknow, c'est donc à lui d'effectuer les manœuvres d'amarrage.

A mesure que l'écart entre les deux vaisseaux se ruit, Hapse est devenu de pus en plus impatient de retrouver Ark. Une certaine curiosité s'est également emparé de lui quand Ark lui a envoyé il y a deux semaines un message lui signifiant avoir réussi à récupérer le RP-16 et son contenu : la star Ikomi, et Hapse ne peut que s'empecher de sourire à l'idée de l'ours qui lui sert de partenaire doive s'occuper d'une personne à la personnalité aussi affirmée. Une forme d'inquiétude aussi, parce qu'il n'est pas vraiment possible, de son point de vue que les choses se soient bien passées, et il va probablement falloir offrir une soupape sociale aux deux individus qui habitent maintenant le Unknown Artist.

En dessous des dix dv de vitesses relative, et à moins d'un kilomètre du Unknown, Hapse coupe les arcjets et les injecteurs d'hydrogène pour manœuvrer uniquement aux systèmes de contrôles d'attitude et de trajectoires et procéder au retournement du WatUp pour présenter son amarre frontale au Unknown. Les jets d'eau pressurisé et surchauffés s'activent sous le doité précis de Hapse qui manœuvre l'entiéreté du vaisseau en utilisant les vieux contrôles manches et palonniers des premiers systèmes de vol, anachronismes au sein du poste depilotage moderne du WatUp, mais Hapse préfère un retour tactile à de simples contrôle entoptiques.

A deux cents mètres de leur point de rendez-vous, Hapse finit par atteindre zéro mètres secondes relatifs. Dans le faisceau des projecteurs du WatUp il peut voir la silhouette élongée du Unknown Artist, ses radiateurs et panneaux solaires déployés, faisant paraitre ridicule en taille ls massive silhouette du WatUp.

Et Hapse sait qu'il va falloir abandonner la propulsion du Unnown Artist, ainsi qu'unebonne partie de sa masse si ils veulent pouvoir rejoindre 4-Vesta avant que leurs systèmes de survie ne soient étirés trop finement pour les maintenir tous les trois en vie.  D'autant que le Hit Me, à peine visible à l'échelle du gigantesque Unknown Artist, n'apporte probablement pas grand chose.

Lentement, les mastodontes s'alignent l'un avec l'autre. L'ancrage avant du WatUp frôlant lentement la proue du Unknown Artist, autour de laquelle tournent lentement le module d'habitation et le système de survie. L'Unknown Artist s'approche a quelques millimètres par secondes. Quelques millimètres par secondes qui ne servent qu'à augmenter l'impatience d'Hapse, mais qui permettent de parfaitement aligner les ancres des deux vaisseaux.

Contact. La tête d'ancrage du WatUp appuie doucement sur les leviers de verrouillage du Unknown Army. La première tentative n'est pas suffisante pour armer le dispositif et garder les deux engins verrouillés. Bougeant les contrôle du bout de ses doigts experts Hapse recule légèrement et repositionne la tête de son ancre avant de relancer le WatUp en avant et impacter, un peu plus fortement et un peu plus profondément le Unknown.

Sous les impulsions mécanique, l'ancre du vaisseau finit par céder et envoie ses clamps saisir l'attache du WatUp, serrant l'amarre en son sein, verrouillant les deux vaisseaux dans une étreinte toute en tension, tremblant l'un contre l'autre. Le sphincter atmosphérique du WatUp s'ouvre et laisse échappé une décharge de gaz vers l'intérieur du long courrier auquel il ets maintenant accouplé.

Une fois les systèmes d'amarrage scellés, les ombiliques télescopiques du WatUp sortent de leur logement, et tatonnent à la recherche des pots de connexions du Unknown Artist. Ondulant quelques instants avant de connecter leurs lèvres d'étanchéité aux tétons de la poupe du vaisseau, permettant le partage de différents fluides des vaisseaux, de connecterleurs grilles énergétiques et de permettre aux intercoms de fonctionner.

Les sas d'entrées sont maintenant alignés, un manchon téléscopique, étiré depuis la poupe de l'Unknown Artist, les protège du vide radioactif dans lequel ils évoluent. Les vibrations qui agitent les deux vaisseaux finissent par se dissiper après que leurs systèmes de correction d'attitudes finissent par se synchroniser, travaillant à l'unisson pour stabiliser l'Unknown et le WatUp, formant maintenant un seul vaisseau.
