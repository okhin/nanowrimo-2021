title:          Nouveau_2
ID:             43
type:           md
compile:        2
charCount:      3055



Ark agrippe Hapse par le bras et se propulse vers l'intérieur du WatUp, entraînant derrière lui la seconde personne qu'il voit depuis presque deux ans. Sans avoir échangé un mot, il embrasse les lèvres fines d'Hapse. Atrapant le baudrier d'Ark, ce dernier se laisse emmener dans une lente spirale, dérivant vers la passerelle, goûtant les lèvres saturées d'iode d'Ark, faisant glisser la fermeture de la seconde peau du pilote de l'Unknown Army vers le bas, faufilant sa main fine sur la peau rugueuse, sous laquelles des nodules de tissus cancéreux ont commencés à se former.

Ark, utilisant les sangles du harnais d'Hapse, l'attache contre la cloison babord et entreprend de le déshabiller, déchirant la seconde peau de ses ongles habitués à dénuder les câbles de son vaisseau. Hapse laisse échapper un léger cri lorsu'il trace une fine arabseque rouge sang autour du téton durci par l'excitation du capitaine, collé à la paroi froide et dure de son vaissaeau. Serrant les jambes autour de son partenaire, Hapse attrape de ses dents la langue d'Ark et presse leurs bassins l'un contre l'autre.

S'appuyant de la main sur la poitrine d'Hapse, qui laisse mantenant échapper quelques billes écarlates, Ark se dégage de l'étreinte de son partenaire et, de son autre main, commence à lui griffer l'intérieur de la cuisse, avant de se saisir de son pénis, gorgé de sang. Hapse halète. Plaqué contre le mur, il ne peut que se laisser faire alors que la langue d'Ark descend doucement vers sa bite.

Ils baisent. Longuement. Lorsque le corps de Hapse finit par s'effondrer sous les coupures, morsures et stimulations, ils continuent de baiser. Lorsque Hapse manque de s'étouffer en suçant Ark, et alors que l'oxygène commence à lui manquer, ils continuent. Ils continuent jusqu'à ce qu'Hapse ne soit plus capable de réfléchir, ou de réagir. Leurs corps attachés l'un à l'autre par un enchevêtrement de sangles de nylons, le sang d'Hapse flottant autour d'eux, se mélangeant au sperme, à la sueur et à la salive, dans un brouillard de micro-billes colorées, dérivant lentement vers les recycleurs d'air. Leurs corps meurtris par la baise et les radiations, ils respirent en cadence, en cuiller, Hapse dans les bras d'Ark.

Ils n'ont toujours pas échangés un mot. Mais ils n'ont pas besoin de parler pour communiquer. Leurs systèmes hormonqux et leurs phéromones font de bien meilleur vecteurs d'information que n'importe quelle langue symbolique, du moins dans cette situation.

"Salut" murmure Ark dans l'oreille d'Hapse. "Tu m'as manqué".

"J'ai pu voir ça oui. Ça m'avais manqué tout ça. Il faudrait faire ça plus souvent."

"Quand tu veux. Mais il sertait plus sage de passer par l'infirmerie d'abord non? Enfin, si vraiment tu veux un deuxième round, moi, tu sais, je peux te rendre ce service hein."

"Nan, pas maintenant. J'ai juste envie d'un calin." Et de voir les bleus que tu m'as laissé aussi, pense Hapse, s'enfonçant encore plus profondément dans les bras du mineur de silcates, avant de détacher les sangles qui les maintiennent contre la cloison.
