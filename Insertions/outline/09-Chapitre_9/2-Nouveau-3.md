title:          Nouveau_3
ID:             44
type:           md
compile:        2
charCount:      7280



"Tu m'as pas dit, ça se passe comment avec ta colocataire", demande Hapse, sortant de la douche, dans l'habitat maintenant soumis à une force de coriolis.

"Ça se passe. Le Unknown est assez grand pour deux. Et elle a passé la première semaine prostrée. Son organisme en shutdown. J'ai du la perfuser. Et là, elle passe son temps à pleurer."

"Et toi ? Tu gères comment ?"

"Il y a pas vraiment de choses à gérer." Répond Ark, n'ayant pas vraiment envie d'élaborer sur les bad trip de chem-mem qu'il s'ai fait, ni sur le fait qu'il est elle, quelque part dans sa conscience du moins, trop d'expérience traumatiques enregistrées. Pas assez d'identité au départ.

"D'accord. On en reparleras plus tard." Dis Hapse enfilant sa combi légère. "Je vais aller faire connaissance avec ton hôte du coup. Tu l'as installée où?"

"Infirmerie."

De toutes les qualitś d'Ark, l'hospitalité n'est pas la première. La pauvre Ikomi doit bien se demander sur quel genre d'humain elle est tombée si elle est confinée à l'infirmerie du Unknown. Si on peut vraiment parler d'une infirmerie. Comme pour beaucoup de solo, l'infirmerie du Unknown est essentiellement un placard aux parois écrans, contenant une atmosphère indépendante du reste du vaisseau et, donc, protégé par un sas. Probablement moins de si mètres cubes d'espace, optimisés pour de la chirurgie ambulatoire auto-performée. Pas vraiment des quartiers d'habitations, mais le genre d'espaces confinés qui rassurent les distants et les solos en chocs.

En revanche, et comme le suppose Hapse, probablement pas le meilleur endroit pour une personne en état de choc, qui n'a jamais mis les pieds dans un vaisseau de distants. Et qui a passé plusieurs mois en hibernation avant d'être réveillée par un incommu.

Au moment oú il pénètre dans le sas de l'infirmerie, il réalise que ses craintes étaient en deça de la réalité. Il reconnaît la star interplanétaire — au sens littéral — instantanément.  Et les nombreux échanges sur LNR lui revienne. Les questions, els argumenattions, les engueulades, les suppositions, mais, en particulier, l'absence totale de considération pour ce par quoi passait alors Ikomi. Pas totale, mais ce n'a toujours été qu'un point mineur des discussions.

Elle est allongée sur la paillasse métallique, torse nu, la combinaison souple aux couleurs du Hit Me nouée autour de la taille. Elle fixe le plafond les yeux grands ouverts, et elle ne montre aucune réaction lorsque, dans le chuintement pneumatique des portes de l'Unknown Artist, Hapse se décide à entrer.

"Hey" dit-il rentrant dans l'infirmerie. Pas de réponse. Les constantes d'Ikomi sont acceptables, du moins d'après l'afficheur mural. Un taux de cortisol un peu élevé, signe de stress, mais sinon Ikomi va bien. D'un point de vue métabolique et biologique.

"Je m'appelle Hapse, je suis un — comment définir ça — partenaire ? Oui, partenaire me paraît bien. Un partenaire d'Ark. Je suis venu voir comment tu allais, je sais qu'il peut-être un peu apre par moment. Et te proposer de t'installer ailleurs que dans cette infirmerie."

Ikomi pivote la tête pour regarder le visage marqué de lègères lacérations d'Hapse.

"Ikomi, je crois. Je suis pas très sûre de qui je suis ces jours-ci." Le sevrage de chem-mem lui apporte une certaine lucidité, même si, du coup, elle ne sait plus vraiment ce qu'elel ressent ou si elle ressent quelque chose. Ses réflexes scriptés lui disent qu'elle devrait montrer encore plus de son corps au nouveal arrivant, pour le pousser à lui sauter dessus. C'est comme ça qu'elle est censé se sortir de ces situations normalement. Mais le conditionnement est affaibli et elle s'arrête alors qu'elle s'est redressée sur la couche métallique, avant qu'elle n'enlève le reste de sa combi.

Hapse lui prend les mains et s'accroupi pour se mettre à sa hauteur.

"Tu n'es pas obligée. Je n'ai qu'une petite idée de ce que tu as pu subir, et je ne suis pas la meilleure personne pour t'aider, je peux juste être désolé pour ce qu'il t'es arrivé. Tu n'es pas responsable de ce qu'il t'es arrivé. Et Ark aussi, même si il a une façon particulière de le montrer."

Ikomi commence à pleurer et dégage ses mains de celles de Hapse pour s'isoler physiquement. Elle ne sait pas gérer la proximité physique autrement que par le sexe, et elle n'as pas envie.

"Je te rajoute sur le manifeste du vaisseau. Tu fais partie de et équipage, au moins jusqu'à ce qu'on puisse faire escale quelque part. Pas le choix. Et je te préviens, ça va pas être un trajet simple. Mais pas le choix." Haspe balance une entoptique sur le mur écran et, après quelques avoir rempli plusieurs écrans, il transfère les accréditatiosn dans le communicateur de poignet d'Ikomi.

"Et tu devrais vraiment sortir d'ici et visiter un peu, le Unknown Artist est vraiment étrange, mais fascinant. À l'image de son capitaine je suppose. Et j'aime ce tas de ferraille à peu près autant que la personne qui le dirige."

"Je peux te poser une question ?" Demande Ikomi.

"Bien sûr."

"Il m'a dit qu'il était moi." dit-elle, ajoutant à voix basse "Il me fait un peu peur aussi."

"Je pense que des narcorythmes étranges lui on fait penser ça. Ou vivre certaines expériences qu'il a projetté ainsi, il faudrait faire un tour dans sa bibliothèque de chem-mem, mais je ne suis pas certain que ce soit une bonne idée pour toi. Et oui, il fait peur. C'est comme ça qu'il fait pour ne pas s'attacher et pour pouvoir repartir en solo faire pousser les silicates dans les nuages radioactifs d'Io."

"T'es un psy ?"

"Non. Mais je suis la ligne de survie de pas mal de monde. Et j'ai beaucoup de temps pour lire aussi, et écouter. On ne peut faire un peu que ça avec ce lag."

"Et tu parles toujours autant?"

"Oui. C'est ma façon de gérer les situations stressantes. Je parle, ça me laisse l'esprit occupé avec le sujet de discussion et pas avec le reste. Mais si je parles trop, dis le, je omprends très bien le besoin d'être seul. Je parcourt des millions de kilomètres pour ça."

"Non non, ça va. J'ais plus l'habitude." Un sourire se dessine sur les lèvres d'Ikomi. Pas le genre de sourrire copyrighté qui finit sur les holographe géant, quelque chose de spontané.

"Bon. Je te laisse t'installer oú tu veux. À titre perso, je préfère la zéro-G de l'épine dorsale. Si tu trouves des containers vide, mets-y les affaires que tu as."

"J'ai que ce que tu as sous les yeux. C'est pas comme si j'avais prévu une virée."

"D'accord". Hapse entreprend alors d'amener Ikomi sur la passerelle du Unknown Army, passerelle baignée d'une lumière rouge sombre, et lui indique quelques compartiments qui stockent des combinaisons et des secondes peau.

Il lui monre aussi l'interface de bord, et, une fois qu'elle commence un peu à se sentir à l'aise, il la laisse explorer, sa combinaison pourpre et jaune ceinturée à la taille. Il a passé deux heures avec elle, deux heures à batailler contre l'impulsion de la toucher, de la caresser, de lui passer la main sur les fesses. Il sait que ce sont des phéromones, mais il va falloir faire quelque chose à ce sujet. Secouant la tete vigoureusement pour chasser ses pensées, il descend vers le plafond et se dirige vers le cœur de l'Unknow Gravity Dog, ils ont assez perdu de temps et il va falloir commencer à sérieusement bosser leur transfert.