title:          Nouveau_4
ID:             24
type:           md
compile:        2
charCount:      3099



Il y a définitivement un problème d'Azote gazeux dans la serre. Les pois et les haricots jaunissent. Ce n'est pas forcément un problème immédiat. Mais moins d'Azote pour les fabacées, c'est moins d'Azote dans les sols pour les autres plantes. Donc moins de feuilles. Moins de photosynthèse et donc moins de fixation de CO². Et ça, c'est un plus gros problème.

Ark a quelques semaines pour agir et trouver la source du problème. Après quoi ils seront deux puis trois sur le système de survie, et il va falloir beaucoup plus de capacité d'abosrption du dioxyde de carbone. Il va falloir lancer des cultures d'Aloé Vera. Et donc remplacer certaines plantes.

Et augmenter la pression atmosphérique et la quantité d'Azote dans l'air. Et relancer des haricots, les versions rapides qui grandissent en douze jours. Couper les plants jaunis, vérifier la vie microbienne, recycler les déchets de coupe. Et basculer partiellement sur des ration de survie, au moins quelques jours, parce qu'il va falloir faire de la place pour ces aloés qui auront pour job d'absorber l'excès de CO².

Ses mains tremblent quand elle commence à couper les pieds de haricots. Elle avait treize ans quand ils ont commencés à ne plus écouter quand elle leur disait non, et que ça ne ressemblait plus du tout à un jeu. Ils étaient quatre et elle récoltait les haricots rouges qu'elle n'as plus jamais pu manger depuis. Sauf qu'elle en mange encore. Ce n;est probablement pas son expérience, mais elle s'en souvient. De la douleur surtout, pas vraiment de la honte. Et de la colère. Et de l'envie de fuir et de partir vivre ailleurs. Ça elle comprend.

Ark pose les sécateurs et se frotte les yeux, oubliant l'espace d'un instant que ce sont des plaques de carbones réactifs. Elle doit se changer les idées et ses chem-mems sont un peu hardcore pour gérer ce genre de situation. Il va bien falloir avancer dans tout ce jardinage cependant. Et réharmoniser les MET pour se préparer à l'insertion. Mais il va falloir attendre encore un peu que les batteries de sa combinaison dure soient pleines.

C'est le problème de devoir naviguer si loin du soleil avec ces panneaxu solaires, même si ils atteingnent presqu'un kilomètre carré de surface, ils ne produisent que quelques kilowatts de jus, remplissant les batteries du vaisseau encore plus lentement que les banquiers des Puits lorsqu'on leur demande quelques eks pour voler plus loin. Il faut donc prioriser et, pour le moment, le système de survie est prioritaire. Les batteries des MET sont prioritaires, tant pis si ils ne sont pas harmonisés. Ce serait plus comfortable si ils l'étaient, la combustion coûterais moins d'eau.

Les pensées douloureuses finissent toujorus par s'estomper sous la nécessité de maintenir en état un vaisseau. Haspe lui demande toujours pourquoi elle est tant attachée à ce tas de ferraille. Mais c'est parceque chaque élément de ce tas de ferraille est lié à une de ses histoires qu'elle absorbe pour construire sa personnalité. l'Unknown Artist est à son image : une collection étéroclite de restes de vaisseaux ayant eu une autre vie auparavant.