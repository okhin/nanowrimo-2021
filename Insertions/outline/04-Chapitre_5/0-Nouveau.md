title:          Nouveau
ID:             21
type:           md
compile:        2
charCount:      2143


La cabine du Unknown Artist est plongée dans le noir, tournant lentement au bout de son bras, fournissant un semblant de gravité à l'ensemble. Ark est occupé à maintenir les cultures hydroponiques de kale, navets, betteraves et pois divers. Trois mois depuis qu'il et sorti de la sphère d'influence de Jupiter et il commence à saturer de ce batch certes productif, mais dont les saveurs commence à le lasser. Et changer le mélange atmoshpérique juste pour des problèmatqieus de saveurs est un peu trop, même pour lui.

Hapse devrait pouvoir lui fournir quelques graines, et ils devraient se croiser dans encore quelques semaines. Les feuilles des fabacées semblent cependant montrer les limites de leurs capacité d'absorption d'azote atmosphérique. Ark note mentalement de penser à revérifier le mélange atmosphérique des serres. C'est sans doute mieux, mais on est jamais trop prudente.

Elle se rappelle son enfance dans les gigantesque container agricoles dans lesquels elle a passé une partie de son enfance. Au moins au milieu des plants et des drônes de récoltes elle n'était pas emmerdé par ses garçons qui insitaient pour être un peu trop près d'elle, alors elle a appris à différencier les poacés, reconnaître les besoins des plantes et leurs façon de les montrer, mais aussi que rien n'est plus fragile qu'un écosystème trop simple.

Les chem-mems s'implantent lentement dans le cortex d'Ark, s'accrochant à ce qu'il reste de son sens de l'identité, alterant la façon dont elle se voit. Et en ce moment, elle préfère se rappeler ces souvenirs chimiques d'une enfance difficile dans les fermes en orbite martienne. Elle ne sait pas comment elle devient cette personne en étant cette petite fille, mais ce n'est pas ce qui importe.

Ce qui importe c'est ça connaissance des plantes. Et sa volonté de se barrer de ce piège gravitationnel et conservateur dans lequel elle se trouve.

Son biomoniteur vibre. De manière continue. Le code d'urgence. Elle referme la serre, range les outils dans le compartiment magnétique au mur et, pousse une betterave jaune dan le filet attaché à l'un des mousquetons en titane pendant de sa combinaison.
