title:          Nouveau_3
ID:             23
type:           md
POV:            6
compile:        2
charCount:      4098



Les calculs d'Hapse confirment ceux d'Ark. À eux deux, ils peuvent ramener le RK-16 sur une orbite d'insertion avec 4-Vesta. Ce qu'ignore Ark, qui est maintenant à moins de deux heures de lag du WatUp, c'est la charge d'Hapse. À vide, ils peuvent ramener le RK-16. Et, si la fronde est en état de fonctionner, et qu'ils peuvent la fourguer, cela pourrait permettre à Hapse de ne pas perdre trop d'ek sur le trajet.

Il est probable que 'Unknown Artist y laisse des plumes. Mais le fait que cet engin continue de fonctionner après plus de cinquante ans, dont trente entre les mains d'une personne incapable de savoir quand un projet est terminé et qui a passé la plupart de son temps entre Jupiter et Vesta a "améliorer" ce qui n'est plus qu'une suite de réparations de bricolages. Ça n'enlève rien aux qualités de l'Unknown Artist, mais le lien que peut avoir sa pilote-mécano avc son vaisseau est quelque peu fusionelle.

Celle-ci, puisqu'il semble que c'est la route mémorielle sur laquelle semble se trouver Ark, aura peut-être du mal à accepter la nécessité de devoir se séparer d'une partie du vaisseau. Il est peu probable que celui-ci ne survive aux accélérations nécessaires aux insertions orbitales qu'il leur faudra faire. Et Hapse n'est pas pressé de devoir annoncer à Ark qu'elle va devoir sacrifier une partie de son organisme. Même si cette partie n'est qu'un amas de débris spatiaux collés les uns aux autres au gel de fulerène.

Hapse ferme les yeux pour réfléchir quelques instants. Il il y a toujours la possibilité de laisser sa cargaison sur sa trajectoire actuelle, et payer les collecteurs à 4-Vesta et dans la famille voisine pour les collecter. À supposer qu'ils arrivent sur 4-Vesta dans un futur proche. Il faut agir relativement vite, le plus tôt la combustion démarre, le moins de dv devront être perdus en corrections de trajectoires plus tard. Surtout avec ces moteurs un peu lent à mettre en route.

Il reste un truc à faire cependant, ça fait deux jours qu'Hapse dort dessus, mais le refus de la coop de financer une fronde pour Echo est un coup dur pour Ark et il n'a pas vraiment lâcher les chem-meme depuis. Mais il va bien falloir annoncer à Echo de ne pas compter sur eux. Love and Rage pourrais probablement mettre quelques ek dans l'idée, le fait de ridiculiser les Puiseurs est toujours un bon moyen d'obtenir un financement de LNR. Ou même de PIS — qui adorerait tester quelques unes de leurs navettes dans des contextes un peu plus stressant que trimballer les cadres exécutifs Martiens — si on peut sécuriser une partie du financement du moins. C'est quelque chose qui va nécessiter plusieurs centaines de méga octets de discussion encore, jusqu'au moment oà quelqu'un finira par créer un fond de soutien.

"Message. Destinataire Echo. Sur 1-Ceres."

"Salut Echo, c'est Haspe. Ark est … Tu la connais. Elle a un peu de mal avec le fait d'annoncer les mauvaises nouvelles. Ou les nouvelles. Bref … La coop ne veut pas avoir à faire avec quoi que ce soit comme du sponsoring d'évènements culturels. C'est nul, mais bon, on va pas les changer, ils sont bien trop éloignés de la ceinture d'astéroïdes pour comprendre ce qu'il s'y passe. Et ils ont leurs propres problèmes à gérer.

"Je sais, c'est nul. Par contre, je pense que tu peux vendre l'idée à Love And Range. Je vais lancer un groupe de discussion sur le sujet, et on peut commencer à collecter des eks. Pause."

Parler du RK-16 ou pas ? Selon le status du vaisseau il sera plus ou moins simple de prétendre le conserver. Et il faut le ramener, ce qui est loin d'être fait pour le moment.

"Reprise. Il est cependant probable que j'ai un truc pour toi qui devrait aider. On devrais être sur 4-Vesta avec Ark d'ici quelques mois. Peut-être un peu moins, on doit faire un détour et pour le moment on va essayer de tous rentrer en un seul morceau. On pourrait utiliser tes compétences en trajectoires orbitale.

"Anyway. Désolé d'être le porteur de mauvaises nouvelles. J'espère que les choses se passent bien sur 1-Cérès en tout cas, ou du mieux que possible étant donné les circonstances. Fin. Envoi."
