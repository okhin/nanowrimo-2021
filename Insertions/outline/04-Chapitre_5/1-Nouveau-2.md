title:          Nouveau_2
ID:             22
type:           md
compile:        2
charCount:      2472



Accrochée à la console de com, Ark regarde le tableau de com. Un SOS. Ici. Alors que le prochain habitat est à des mois de voyages. La trajectoire orbitale affichée indique un départ de Cérès. Le code transpondeur est celui d'une fronde, un modèle RK-16. Pas quelque chose pour traîner ici. Quelque soit la situation, les personnes à bord vont être dans un état étrange. En supposant qu'elles ont réussit à s'hiberner, parce que sinon, le RK-16 sera juste un cercueil un peu cher.

Au moins, l'orbite de l'Unknown Artist et du RK-16 sont toutes les deux progrades. Ça devrait rendre l'approche faisable au lieu d'être simplement impossible avec les réserves de dv dont dispose Ark. Suffisament pour stabiliser une orbite en tout cas. Pas assez pour atteindre la ceinture. Les systèmes de survie de l'Unknown peuvent tenir la charge avec deux personnes à bord. originellment le module d'habitations est dimensionné pour un équipage de quatre, mais la serre est beaucoup trop petite pour ça.

Il va falloir être fine, se dit Ark. Mon ratio poids puissance est asthmatique, c'est le principal inconvénient de ces MET. Et si le RK-16 est en rade, il y a peu de chance qu'il puisse venir s'amarrer.

Calculer une trajectoire d'interception n'est pas la partie la plus complexe. En calculer une qui permette une approche de quelque chose qui est si petit, sans collision, avec un vaisseau qui a à peu près la maniabilité d'une enclume en uranium et qui n'est pas du tout conçu pour ce genre de chose.

Mais l'alternative serait de laisser les malheureux à bords du RK-16 dériver jusqu'à ce que leurs systèmes d'hibernation — Ark a décidé qu'ils avaient pu hiberner avant de se retrouver en panne — les abandonnes. Et ça pourrait être Echo. Peut-être. Probablement pas. Aucune chance qu'elle fasse une telle erreur de trajectoire qui la mette dans une telle situation. Mais ça pourrait finit par décider Ark.

Et le RK-16 en soi vaut le coût de faire le crochet dans le pire des cas. Même si le ramener dans la sphère d'influence de la coop pour récupération est en soi un autre problème pour lequel Ark pense ne pas avoir le dv pour cependant.

Et toute cette soupe orbitale va compromettre ses chances de voir Hapse. À moins qu'il ne vienne donner un coup de main. La poussée du Gravity Dog est certainement suffisante pour ramener le RK-16. Suffisante pour casser l'épine dorsale du Unknown Artist aussi. Dans tous les cas, il va falloir prévenir Hapse du changement d'orbite.
