title:          Nouveau_2
ID:             18
type:           md
compile:        2
charCount:      2412



C'est lorsque Lonely Cowgirls passe qu'Echo reviens. Elle doit être l'une des rares personnes à ne pas porter de combinaison si près d'une cloison, mais elle aime trop la mode Lunaire pour accepter de porter quelque chose de plus épais que les hauts échancrés en simili-coton et les cuissardes au motifs géométriques et colorés pour les cacher sous une seconde peau. Même si la différence de gravité fait qu'elle est obligé de fixer à l'adhésif corporel la plupart de ses fringues.

Elle amène avec elle trois bouteilles d'Hydrazine™ pour partager avec Shi et Kam, qu'elle trouve en train de s'embrasser, engoncée dans la banquette en mousse à mémoire de forme. Elle pose les bouteilles sur la table et commence à têter la sienne. Le goût est étrange, ça devrait être proche de fuit rouge ou autres — du moins c'est ce que dit l'étiquette de la bouteille : "Taste like Argon, but with strawberries" —  mais là ça ressemble plus à un aromate hautement volatile, ou à du kerozen. Probablement un effet seondaire du pack de drogue qu'elle prend en ce moment pour stabiliser son métabolisme et limiter les rejets et les radiations. Et qui la force à rester coincée dans une atmosphère avec autant d'Azote dans l'air.

Plus que de devoir demander de l'aide à son père, ou que de rager à cause d'Alejandro the third qui l'a balancées dans la glace, c'est le fait de devoir rester confinée dans cette partie de KBI qui l'enrage le plus.

"Bon, je vais danser moi. Je vais pas laisser passer du DLM." Laissant son Hydrazine et ses partenaires, elle rejoint la cage de danse d'un pas forcément léger. Tout est question d'accroche pour danser, on ne peut pas simplement effectuer une pression sur le sol et spérer rester au sol, du coup il faut s'attacher à quelque chose. Ou à quelqu'un, mais cette personne doit aussi être accrochée quelque part. C'est pour ça qu'Echo porte toujours un baudrier de pilote. Les sangles, mousquetons et points d'ancrage permettent de toujours tout garder à portée de main. Ou de pouvoir s'assurer de ne pas s'éloigner d'une barre autour de laquelle on tourne, laissant les chauffeurs corpos accoudés au bar profiter d'un peu trop de son anatomie, les adhésifs cutanées ayant une limite.

Mais elle s'en fout. Elle profite du refrain entêtant de Lonely Cowgirls et évacue le stress et l'anxiété accumulée alors que son métabolisme se retrouve soumis à un taux d'alcool en hausse constante.
