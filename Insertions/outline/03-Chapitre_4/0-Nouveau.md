title:          Nouveau
ID:             17
type:           md
compile:        2
charCount:      4806


Les néons du Xenon Girl diffusent une subtile lumière pourpre au-dessus de la table, accorchée à la paroi externe d'un des quatres tores en rotation synchrone de KBI, le principal port spatial en orbite de 1 Cérès.

Étrange de toujours utiliser ce terme — néon — alors que le tube diffusant une lumière colorée n'est qu'une succession de semi conducteurs diffusant de la lumière se surprend à penser Kas en attendant qu'Echo ne revienne du bureau de transmission. Elle l'a rejointe avec Shi à sa sortie du quartier de quarantaine médicale, et elles passent donc toutes les trois un peu de temps ensemble, en gravité basse le temps que le système sanguin reconstruit d'Echo ne se rééquilibre après la brutalité du demi G qu'elle a subit pendant les trois rotations nécessaire à sa regénération.

"T'es encore perdue dans tes pensée Kam, ça va ?" Lui demande Shi, posant sa main sur celle de Kam, étendant et partageant ainsi leur zone d'intimité, signifiant à Kam qu'elle peut parler à Shi de ses probèmes. Comme Shi le fait si souvent.

"Ouais, je pense. Je réfléchissait juste au fait qu'il n'y a plus vraiment de néon dans ces néons qui nous éclaire."

- Ouais. Je suppose qu'on ne peut pas vraiment s'affranchir si facilement d'une habitude culturelle hein?
- Je suppose que non, en effet."


Il y a autre chose. Shi le sait, et le ressent dans la posture tendue de Kam. Elle la connait trop pour ne pas le voir, mais elle sait aussi que chercher à pousser Kam à parler n'est qu'une invitation pour celle-ci à éviter les sujets. Elle parlera si elle en a besoin. À elle, à Echo ou à l'une ou l'autre de ses plans cul dans la salle de contrôle et de coordination.

Oui, Shi est jalouse. Un peu. pas suffisament pour l'admettre. Elle préfèrerait que Kam et Echo lui parle plus facilement de leurs vies, de leurs problèmes, de leurs envies, au lieu de toujours devoir attendre dans un silence géné que les choses finissent par se dire, bien souvent dans les larmes.

Et si Shi est jalouse, c'est aprce qu'en fait elle a eu peur. Quand elle a vu le Trasher se disloquer dans cet astéroïde — ou glace-téroïde — son cœur s'est arré quelques secondes. Techniquement, son biomoniteur a détecté une légère tachychardie et une augmentation du de son rythme cardiaque parfaitement en lien avec ses émotions, mais de son point de vue, c'est comme si son cœur s'était arrêté. Ce qui a sauvé Echo c'est Kam, qui a su garder son calme, diriger les secours et guider Echo dans les procédures de décompressoin rapide. Pas qu'elle en est besoin, mais dans ces situations il est important de ne pas supposer qu'une pilote soit en état de réfléchir.

Et si Kam a pu faire ça, alors que Shi se pissait — littéralement ­— dessus de choc, c'est parce que Kam a quelque chose que Shi n'as pas : un détachement qui lui permet de rester sereine face aux pires des situations. C'est pour ça qu'elle s'éclate autant dans ses gardes au poste de contrôle, à calculer les approches et guider les cargos dans les orbites basses de 1 Cérès, rassurant les pilotes qui, parfois, ne sont plsu habitués à avoir plus de 2 ping radars sur leur écrans après des mois de travrsée solitaire, priorisant les appels de détresse lorsque ceux-ci arrive et, toujours, de sa voix assurée mais réconfortante, de ramener à bon port ces mégatonnes de carburants, de bouffes et de produits divers qui iront ensuite approvisionner différentes stations.

Et le fait que Kam porte le harnais de manière bien plus classe qu'elle ne l'aide pas.

"Hey, t'es avec moi?" Demande Kam à Shi après ce qui semble être des secondes interminables.

"Non. Désolée. Je …"

"C'est Echo qui te préoccupe hein?"

"Pas que. J'ai pas forcément besoin d'en parler là, j'ai eu peur, c'est normal. Je me demande comment toi tu tiens le choc par contre."

"Parce que tu ne m'as pas vu m'effondrer. C'est ça?"

Mm-mm fait Shi de la tête.

"Je me suis effondrée. Avant. À chaque fois qu'elle monte dans une de ses frondes pour s'élancer à beaucoup trop de G et beaucoup trop près de beaucoup trop d'obstacle. L'accident est juste la conséquence inévitable et, une fois que ça arrive, je peux passer en auto-pilote et tout faire pour sortir les gens de là. Mon problème c'est quand elle rentre à bon port."

"Ton problème c'est quand elle monte dans sa fronde."

"Oui. Mais on ne peut pas vraiment l'empêcher. C'est même pour ça que tu l'aime je crois. Son petit cul, ses grands yeux noirs et son sale caractère obstiné. Non?"

"Ouais. Et le fait qu'elle arrive à aller là-dehors, seule sans jamais hésiter"

Kam ne répond pas. Car elle sait que Echo hésite. Mais elle sait aussi qu'elle ne veut pas montrer ses hésitations à Shi, ou à quiconque. Kam le sait parce qu'Echo a tendance à parler la nuit, et elle est la seule du trio à ne pas être synchronisée sur leur cycle circadien.
