title:          Nouveau_3
ID:             19
type:           md
compile:        2
charCount:      3283



Shi a rejoint Echo lorsque Criminal of The Evening blastait de partout. La texture douce de sa seconde peau se frottant à ce qui est encore essentiellement de la proto-peau des cuisses d'Echo, les deux étant attachées l'une à l'autre.

"Tu m'as fait peur tu sais?" Lui dit Shi, un peu à brûle pourpoint, via leur canal perso. Pour ne pas que le reste du monde l'entende.

"Je sais." Répond Echo, une pointe d'exaspération dans la voix. "Mais danse avec moi tu veux? On en parlera plus tard non?"

Shi sait que ce plus tard veut dire jamais. Mais elle sait aussi qu'Echo continueras ses courses. Elle sait aussi qu'elle en a marre de 1 Cerès, de trainer au Xenon Girl, au Blueberry, au XXX ou ailleurs.

"Je sais que je t'ai fait peur." Reprend Echo après un instant. "Je ne peux pas faire autrement. Je ne peux pas juste rester assise et ne pas prendre de risque. J'aime ce que je fais. Je m'éclate à ce que je fais. Et je suis la meilleure à ce que je fais. Tu le sais depuis le jour où tu m'as parlé pour la première fois. Je comprend pourquoi tu as peur, et je ne peux pas l'empêcher, juste l'accepter et essayer de te rassurer comme je peux. Et de te dire que je fais tout ce que je peux pour revenir en un seul morceau. C'est pas assez, ça ne le sera jamais, mais c'est comme ça."

Echo se retiens d'ajouter la suite. Mais le pense quand même. Elle sait que Shi ne restera pas sur 1-Cérès longtemps, qu'elle partira dès qu'elle aura une opportunité. Et comme elle est douée dans ce qu'elle fait — la curation musicale — ce n'est q'une quetsion de mois. Et c'est ce qui est la source de la principale angoisse d'Echo en ce moment.

Une main qu'elle reonnaîtrait entre mille, presque littéralement, se pose sur son épaule et la sors de ses pensées. Kam les as rejointe. Mais aors qu'Echo cherche ses lèvres pour l'embrasser, elle remarque instantanément la tension et le stress qui se sont manifestés chez Kam, son communicateur est en code rouge et, de là où se trouve Echo, les notificatiosn semblent s'accumuler par dizaines.

"Faut que j'y aille. C'est le bordel, ils ont besoin de tous le svolontaires au triage. J sais pas quand je reviens. On se retrouve chez Shi."

Pas de places aux questions. Une urgence au triage c'est mauvais signe. Ça ne peut qu'être un vaisseau en panne ou des collisions graves, dans des orbite strop proches d'un des habitats qui traine autour de 1-Cérès.

L'ambiance n'est plus la même. Les pilotes qui, il y a encore quelque minutes, discutaient sport, de politiques des Puits, des galères de contrats ou, simplemement mataient les gens qui dansent, se sont activés. La plupart d'entre eux, même si ils sont sous contrats, s'enregistrent dans les brigades de volontaires. Ils ont tous des compétences pour aider dans ces cas là et, en général par respect et pour leur laisser le temps de se reposer, ils ne sont que rarement mobilisés. Le fait que le transit les sollicites n'est pas bon signe.

Echo parcoure frénétiquement ses flux. Pas de notifications pour elle.

"On t'as sorti des réserves avec Kam. Tu pourrait pas aller dans le poste de contrôle, tu as besoin de gravité encore deux jours. Et tu ne pourras pas aider." Lui annonce Shi face à son désarroi. "Et tu es coincée avec moi. Condamnée à suivre l'action de loin. Pour une fois."
