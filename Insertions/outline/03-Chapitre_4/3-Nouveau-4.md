title:          Nouveau_4
ID:             20
type:           md
compile:        2
charCount:      5105



Kam est hors-ligne lorsqu'elle finit par remonter un ds rayons et arriver au poste de contrôle avec la dizaine de volontaire présent. Elle a récupéré sa combi souple, et à enfiler son casque, procédure d'urgence. Harnachée à son poste de communication elle se connecte à l'enoptique du dispatch.

C'est encore pire que ce qu'elle pensait. La plupart des objets impliqués n'ont pas de transpondeurs mais sont définitivement propulsés vu le strajectoires qu'ils suivent. Ils suivent la trace d'un transpondeur qui se déplace beaucoup trop vite dans une zone dans laquelle sont essentiellement stationnés des réservoirs d'hdyrogène et d'oxygène.

Elle sait cependant ce qu'elle a sous les yeux. C'est la horde. Encore. Ils ont normalement été bannis de cette zone, mais sans réels moyens de les contrôler ils continuerotn de suivre leurs cibles où qu'elle aille. Tant pis pour les coûts, tout pour une photo. Ou une bonne poursuite dont ils pourront toujours revendre l'XP à un moment. Et juste à la pointe une fronde à la trajectoire erratique. Mais avec un transpondeur. Kam peut travailler avec ça.

"Hit Me, ici triage. Avez-vous besoin d'assistance?"

Le vaisseau ne répond pas.

"Hit Me, ici le triage de KBI. Avez-vous besoin d'assistance?"

La connexion est établie, amis pas de réponse du vaisseau.

"Hit Me, ici le triage. Sans réponse de votre part nous allons envoyer des secours. Avez-vous besoin d'assistance?"

Sans répondre, l'écho du Hit Me et son vecteur d'orientation semblent s'animer. Non, pense Kam. Non, il ne va pas faire …

Avant que la pensée ne se termine le vecteur vitesse du Hit Me s'inverse, le dirigeant à plsueirus centaines de metres scondes vers la horde, s'apprétant à éparpiller ses poursuivants aussi sûrement qu'un neutron percutant un noyau d'Uranium.

Ça va être un carnage. Et la plupart des poursuivants n'ont pas vriament de véhicules leur permettant d'esquiver l'impact à venir ou de se stabiliser après la collision. Et une chose est sûre, le pilote du Hit Me, quelque soit son étât, refuse de collaborer.

Le poste de triage n'a jamais été aussi silencieux. Il n'y a guère que le bruits de ventilations des unités de calculs cherchant des solutions orbitale au chaos qui semble s'installer dans la zone des entrepôts.

"Libérez-moi des orbites basses, il faut déorbiter ces réservoirs avant que quelque chose ne les percute. " balance Kam dans les coms, calme comme souvent dans ces situations. Une fois le choc passé, son esprit est clair. Pas moyen de négocier avec le Hit Me qui, à ce rythme, va atteindre la vitsse de libération dans quelques secondes, il sera toujorus temps de trouver une solution pour ça plus tard.

Pas moyen non plus de faire manœuvrer les connards de la horde. Certains sont déjà en tran d'essayer de se remettre en orbite, mais il y en a au moins trois qui semblent ne pas agir pour corriger leur trajectoires, probablement inconscients. Ou morts. Ils seront récupérés plus tard.

Reste la réduction de risque. Pas le temps de calculer des trajectoires trop précises, mais statistiquement il faut bouger les containers. Ça va coûter une blinde en carburant, mais c'est ça ou des explosions en chaîne de carburant qui enverront des débris en orbite, rendant l'orbite basse de 1-Cérès inutilisable pour plusieurs années.

"C'est la faute de cette connasse ça" balance l'un des volontaires.

"Hey, vos gueules. On en sait pas ce qu'il se passe dans ce vaisseau, la horde n'avais probablement pas à la pousser si loin et moi je suis là pour essayer de réduire la merde. Alors maintenant on peut manifester sa désaprobation, mais moi j'ai besoin que vous e viriez les orbites, et que vous laissiez le canal de com libre. Clair?"

Il faut agir vite pour maintenir la cohésion d'un groupe anarchique. Et parfois il faut accepter une position de commandement. Et c'est ce qui se joue là.

"Clair?" Reprend kam, toisant les visières des autres volontaires du dispatch.

"Clair." Répond Ishar. "Et je te libère 25 et 27 dans deux minutes, tu devrais pouvoir y parquer quelques trucs déjà."

"Pour 29 ça va être chaud, trop de touristes, mais on peut s'en servir de parking. 31 devrait aller." Ajoute Qiang.

33 c'est là que traine KBI. La horde est en 34. À contresens. Ça va être une longue garde. Et elle préfèreait clairement être avec Echo. Ou Shi. Ou même avec Ishar, qui est souvent là pour file run coup de main et qui est bien plus que simplement gentil avec elle, se prend elle à penser.

"Un verre quand c'est fini?" Envoie-t-elle en faisceau rapproché à Ishar.

Pas de réponse. Il ets trop occupé à expliquer aux pilotes des navettes stationnées en 25 qu'ils vont devoir cramer du fuel, que oui c'est une situation d'urgence et que non ils n'ont pas le temps de prévenir leur hiérarchie. En plus de dix dialectes des Puits. personne ne veut jamais être celui qui crame le carburant pour éviter une collision, espérant toujorus que l'autre le fera avant.

Ça va vraiement être une longue soirée pense-t-elle en commençant a donner les séquences de combustion aux containers de carburant, les écartants du danger immédiat de la horde.