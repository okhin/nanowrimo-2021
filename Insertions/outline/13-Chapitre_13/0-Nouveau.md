title:          Nouveau
ID:             56
type:           md
compile:        2
charCount:      3344


Flottant librement dans l'atmosphère pressurisée de la poupe de l'Unknown Gravity Dog, Ikomi joue avec les niveaux des samples extraits de la mémoire de son assistnt. Ceux qui devaient constituer son album, celui qu'elle aurait voulu sortir au lieu des hits When you stop loving et Confident me qui ont fait la fortune de son agent et de Shiitake Overdrive.

Pas qu'elle ai grand chose à repprocher ces albums, en soi — même si elle aurai préféré que les paroles soient plus à propos de vouloir que de simplement être — mais ce n'est pas la musique qu'elle veut faire.

Ce projet n'est jamais allé bien loin, son agent l'ayant systématiquement découragée et dévalorisée. C'est pour ça qu'elle écoute cette musique au casque, elle ne veut pas que d'autres qu'elle écoutent ça pour le moment, elle n'est pas prête. Alors pour le moment, elle mixe, juste pour elle-même, en essayant de retrouver quelques sensations.

Ça fait une semaine qu'elle se passe et repasse les boucles musicales, et qu'elle laisse son esprit dériver en free style sur les rythmes et les mélodies étrangement déformées par l'Helium. Pas de quoi faire des paroles encore, mais juste histoire e se mettre en place, de reprendre les habitudes.

Et, si elle doit un jour sortir cet album, ou ce morceau, ou quoi que ce soit, elle doit aussi se poser la quetsion de savoir quoi raconter.  On ne veut pas entendre parler de routine, mais de ce qui la fait dérailler. Elle sait que quelqu'un lui a dit ça un jour. A elle ou à une autre version d'Ikomi avant elle. Peut-être que ce n'est pas ce qui fait vendre, mais raconter sa routine à elle lui paraît intŕessant.

Ce n'est pas tous les jours, après tout, que l'on entreprend de démonter le vaisseau que l'on utilise pour se déplacer. Ce n'est pas non plus tous les jours que l'on ne sait pas quels sont ses souvenirs et ceux d'autres. Ou que l'on s'aperçois que peu de gens se préoccupe du fait que l'incident l'impliquant à KRI à globalement disparu des principaux systèmes de médias, obnubilés par sa dernière romance avec une actrice  payée pour jouer le rôle de la nouvelle copine cachée de la star.

Peut-être que c'est de ça qu'elle devrait aprler dans ses textes. D'elle, de ce qu'elle aurait pu être, de ce qu'elle a subit. Peut-être que Hapse a raison quand il lui dit qu'elle devrait dire sa version. Mais si elle s'est remise sur de la pod et de la composition, c'est à cause d'Ark. Qui ne cesse de lui renvoyer une sorte de reflet d'elle-même. Pas un reflet parfait, plus comme celui à la surfce d'un lac d'oxygène dont l'ébulition et l'évaporation brouille et déforme l'image, grossissant certaines parties, en déformant d'autres, exagérant des défauts minimes, cachant sous les bulles éclatantes les cicatrices. Mais qand même un reflet.

Ça et le fait que l'ablation des parties inefficaces de l'Unknown Artist ont été terminées, et que maintenant ils attendent d'arriver à leur point d'injection. Il y a toujours des choses à faire, il s'agît d'un vaisseau spatial après tout, mais cela leur prend moins de temps. Et surtout, ils vivent en décalés, pour avoir toujours quelqu'un de quart.

Il faut donc s'occuper, et, après avoir testé les jeux auquel joue Hapse ou avoir essayé de comprendre ce à quoi Ark occupe ses temps libre, elle s'est remise à la musique. Et elle avait oublié à quel point elle aime ça.
