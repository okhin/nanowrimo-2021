title:          Nouveau_2
ID:             57
type:           md
compile:        2
charCount:      5558



"T'en penses quoi ?" Demande Ikomi à Ark alors qu'ils finissent d'écouter l'arrangement de Fuck My Lives qu'elle a peaufiné ces derniers jours. Il enclenche les rivets explosifs qui déssolidarasent la colonne vertébrale du Unknown Artist du reste des vaisseaux. Ark tiens particulièrement à le faire lui-même, Hapse manœuvre l'attelage pour éviter une collision avec la colonne de vieux containers d'une centaine de mètre qui s'éloigne maintenant doucement d'eux et Ikomi se réveille. Elle a filé le lien à Ark pour qu'il écoute et lui donne son avis il y a deux jours.

"Musicalement, ça va. Je pense. Je suis pas la personne la mieux placée pour ça tu sais ?"

"Ben c'est toi ou Hapse."

"Ou n'importe qui d'autre de branché sur le bruit électromagnétique humain. Hapse t'as bien filé les accès ?"

"Vu comme ça. Tu veux pas me donner ton avis ?"

"Mon avis c'est que j'en ai pas. C'est agréable à écouter, mais sans les textes c'est … Il manque quelque chose non ?"

"Je suis pas encore prête pour ça."

"Tu le seras jamais, donc n'attends pas." Un blanc dans la conversation. Ark range la torche à plasma et se prépare à rentrer dans les hydroponiques de la proue du Unknown Gravity Dog, là où, quelques minutes plus tôt, se trouvait encore le reste de son vaisseau. Il ne le vit pas aussi bien que ce qu'il l'affirme aux autres, ou à lui-même, mais ce n'est pas quelque chose dont il a besoin de discuter. C'est un deuil, ça ne se discute pas, ça se vit.

"Bon. Moi j'en penses que si tu mets tes tripes dans ta musique alors ça sera bien. Et si c'est mon expertise musciale est tout ce dont tu as besoin, j'aimerais rester seul un peu."

"Pas de soucis" répond Ikomi, dans le vide, Ark n'ayant pas attendu de réponse avant de couper la communication.

Il va bien falloir qu'elle mette des mots sur ses musiques. Qu'elle mette ses tripes dans les pistes numériques, qu'elle affronte ses peurs et ses passés. Et ce n'est pas Ark ou Hapse qui pourra l'aider à le faire. Elle est seule pour ça. Et elle aimerait tellement ne pas l'être, que Peg soit là avec elle pour aller au bout du truc.

Sans vraiment y prêter attention, elle regarde la latence entre le Unknown Gravity Dog et 1-Ceres. Encore plus de trois heures. Aucune conversation n'est vraiment possible, juste des messages. Une inspiration. Expiration.

"Message." Hapse lui a donné accèes au contrôle vocal du vaisseau, elle ne s'en sortait pas avec l'interface uniquement tactile d'Ark. Son moniteur de poignet clignote légèrement, etl un curseur de terminal attendant une entrée utilisateur.

"Salut! C'est Ikomi. L'originale, pas la copyrighté de Shiitake. Ou si, peut-être. Bref, la meuf sur qui tu as eu un crush il y a trop longtemps et qui a probablement créé un tout petit peu de chaos sur KRI." Non, je ne peux pas le faire comme ça pense-t-elle.

Le curseur clignote doucement quelques secondes. Elle reprend.

"Bon, désolée. C'est brouillon. Je … Je ne sais plus grand chose de moi ou de ma vie, juste que j'ai envie de te voir. Et que je me suis remis à faire de la musique. Je suis … quelque part, en sécurité. Et je voudrais dire des choses, hurler des trucs au public, mais je pense qu'il faut que je te les dise à toi. Du coup voilà. C'est là dans la pièce jointe. Fin Enregistrement. Attacher fichier Ikomi\Fuck_my_lives_lyrics. Destinataire" Bonne question ça. Avec KRI dans l'état dans lequel elle est, comment acheminer le message ? "En attente. Archivage dans Ikomi\Message_Peg".

Elle verra ça avec Hapse. En attendant, le Unknown gravity Dog est en train de procéder aux manœuvres de rotatiosn qui va précéder la longue séquence d'accélérations pour les mettre sur la bonne trajectoire. Elle se dirige donc vers le module de poupe qui a été collectivement désigné comme passerelle, pour se préparer à subir quelques G pour la première fois depuis trop longtemps.

Les réacteurs à fusion de deutérium montent en température, afin de pouvoir expulser le flot ionisé dans les arcjets et de maximiser la poussée du vaisseau. Dix minutes après qu'Ikomi n'ait rejoint Hapse et Ark, ce dernier serrant fort la main de son compagnon alors qu'il s'apprête à dire adieu à l'essentiel de son foyer, le deutérium est propulsés dans les champs électriques des arcjets, infligeant une poussée de 1,3 G et collant les passagers à leurs siège.

Ils vont subir cette accélération quelques jours, et il leur faudra ensuite effectuer plusieurs combustions pour corriger leurs trajectoire. Et il faudra freiner dans trois ou quatre mois, selon l'importance des corrections, pour ne pas arriver trop vite autour de 4-vesta.

Pendant ces quelques jours, ils devront tous les trois vivre dans les quartiers d'Hapse, la passerelle étant maintenant dotée d'une gravité verticale. Ikomi s'est installé un petit compartiment isolé. Non pas tellement pour offrir à Ark et Hapse une intimité, elle a appris qu'ils sont aprfaitement capable d'oublier que quelqu'un d'autre est présent dans le même espace qu'eux quand ils commencent à baiser, mais justement pour qu'elle puisse elle avoir une certaine distance sociale.

Elle apprend qu'elle aime bien au final ne pas nécessairement se ballader à moitié à poil sous le regard des autres. Qu'elle n'est du moins pas obligée contractuellement de le faire. Et elle préfère pour le moment essayer de rassembler les morceaux de ce qui pourrait, peut-être, à terme constituer une personnalité mais qui n'est, pour le moment qu'un ensemble de pièces de puzzle à assembler, sans référence. Et probablement un de ces puzzle entièrement blanc, de 10 000 pièces.
