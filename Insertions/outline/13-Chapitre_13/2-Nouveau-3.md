title:          Nouveau_3
ID:             58
type:           md
compile:        2
charCount:      7974



Piquant dans l'autocuiseur une part de ragout de haricots et de kale — à peu près le seul plat que sait cuisiner Ark et probablement la seule chose que peu produire pour le moment ses modules hydroponique — et écoutant du 3 Way Down au casque, Ikomi est surprise par hapse qui vient chercher son café et qui, après quelques instants, corrige sa posture pour prendre une distance physique, temps nécessaire pour qu'il réqlise que ses instincts le pousse à vouloir absolument la toucher. Et probablement plus.

"Ça me déragerait pas, tu sais" lui dit-elle remarquant le glissement de position qu'il pensait imperceptible.

"C'est pas tellement la question. Est-ce que tu en as envie est-ce que j'en ai envie, et quelle partie de ça est lié à ton chevalier, quelle partie aux chem-mem et quelle partie à notre envie réelle de … de quoi d'ailleurs ?" Répond-il s'asseyant de l'autre côté de la table, après avoir mis de côté le poste à soudure et remiser dans un coin le filtre à CO² éventré sur lequel travaille Ark.

"Je sais pas. C'est pas vraiment important, c;est juste de la baise hein."

"Et ça te dis pas d'avoir ton mot à dire ?"

"Je l'ai dit. Ça me dérange pas. Ça peut même être fun. T'as l'air réceptif, mais de te prendre la tête, c'est pas grave et tant pis, on baise pas." Elle enfourne un peu plus de ragout dans son système digestif. Au moins Ark maîtrise la science de l'assaisonnement.

"C'est … " commence Hapse

"Écoute, c'est sympa, vraiment, j'apprécie que tu te soucie de ce dont j'ai envie, ça me change. Mais ça fait quoi maintenant, trois mois, quatre mois ? Tu sais ce que j'en pense. Je sais que si je ne veux pas, tu respecteras ça. Je sas que mon chevalier te fais des trucs pas cool, mais honnêtement, on peut pas tout blâmer sur ces phéromones adaptive. Tu veux toucher ma peau, mes seins, mon cul, ma bouche, ou je sais pas quoi. Ça me dérange pas. Est-ce que je le veux ? Je sais pas. je sais pas grand chose de ce que je veux.

"Et comme dit, si je ne le sent pas, que je me sent mal ou que, au final, je m'emmerde, je sait qu'on s'arrêtera. Du coup, je réitère, on baise ?"

Hapse fuit le regard d'Ikomi et se concentre sur son pack de café ammoniac. Pas du vrai café, pas de la vraie ammoniaque non plus. Il ne sait pas vraiment quoi penser de cette situation. Il sait qu'elle a raison, que, certes, les phéromones jouent peut-être un peu sur la relation. Mais en vrai, il n'as pas les outils pour gérer cette situation. Ça ne correspond pas à ses petits schémas de relations orbitales, ça ne ressemble pas aux elations très négociées qu'il entretien généralement, dans lesquelles tout est questionné et discuté, parce que c'est ainsi que se font les discussions à plusieurs heures de décallage. Et Ikomi est hermétique à ce schéma, cette socialisation décalée, éllongée par les orbites excentriques des distants s'insérant dans la vie des autres à la vitesse de la lumière, mais en maintenant plusieurs unités astronomiques de zone d'intimité autour d'eux.

"Che n'ai pas bejoin d'ếtre chauvée tu chais", lui dit Ikomi, la bouche pleine, confirmant encore qu'elle n'a que faire de la distance et interrompant le flot de pensées de Hapse. Après avoir dégluti, elle reprend. "Bon, peut-être que si. Mais tout le monde le pense et agit comme si j'avais besoin de l'être, toi y compris. Et c'est usant. Je ne suis que quelqu'un devant être sauvée, pas une personne pouvant avoir son mot à dire. Et j'apprécie ce que tu fais, mais s'il te plaît, arrête d'essayer de me sauver."

"J'essaye pas de te sauver …" commence à répondre Hapse, sur la défensive.

"Si. Tu essayes de me sauver. Tu essayes de sauver Ark aussi. Tu essayes de sauver tout le monde. C'est probablement comme ça que tu prend ton pied, je sais pas. Ou alors c'est un trip de contrôle, c'est ta façon de donner du sens à ta vie. Ou je ne sais quel délire tu utilise pour justifier ça. Et je ne sais pas comment marche ta relation avec Ark, ça me regarde asse peu au final tant que vous y trouve votre compte, mais moi j'en ai marre des gens qui veulent me sauver."

Ça fait mal. Se faire dire les choses sans prendre le temps de les enrober, telles qu'elles sont, ça fait mal. Et même si c'est vrai, ça n'en reste pas moins douloureux. Hapse se renferme sur lui, s'affaissent sur sa chaise, se fondant presque avc la structure de la cabine.

"Hey", lui dit Ikomi, "ça change pas que j'apprécie ta compagnie. Juste … je sais pas. J'ai pas pris ces narcorythmes depuis longtemps, je suis un peu à cran, et j'en ai marre qu'on pense pour moi." Elle se sert une tasse de vin de kale — Ark commence à vraiment en faire quelque chose d'appréciable ou alors c'est l'habitude. "Écoutes. Reprenons au début, si ça te va ?"

"Au début de quoi ?" demande Hapse, sa volonté de poursuivre cette conversation s'amenuisant de secondes en secondes, une douleur començant à naître dans sa poitrine, un poids se posant sur ses poumons. Ikomi se penche en avant, sa combinaison largement ouverte, montrant sa peau brune, exposant ses seins à qui ne soutiendrait pas son regard, tendant une main ouverte en direction de Hapse, comme pour initier une poignée de main.

"Ikomi, enchantée de te rencontrer. On baise ?" Dit-elle, un sourire au lèvres, attendant que Hapse sorte de sa transe. Quelques secondes paasent avant que celui-ci ne se recompose, ne retrouve une respiration lui permettant de réfléchir.

"Oh, et puis merde." marmonne-t-il, avant de prendre quelques grandes inspirations, "Hapse. Enchanté." Il saisit la main d'Ikomi, la serre et l'entraîne vers lui, glissant son autre bras autour de la taille de la star déchue, contre sa peau transpirante — il faudra vraiment qu'ils songent à ajuster la température des systèmes de régulation climatique mais là maintenant, ce n'est plus trop ce à quoi Hapse à envie de penser.

Ils roulent sur la table, dispersant les plats et assemblages électromécanique en cours de remontage sur le sol, la combinaison d'Ikomi les rejoignant rapidement, alors qu'elle s'installe à califourchon sur le corps couverts des marques de ses ébats violent avec Ark, faisant glisser son bassin sur celui de Hapse, laissant sa bite s'infiltrer dans sa chatte avec la lenteur de vaisseaux en ancrage, corrigeant régulièrement leur trajectoire, reculant lentement, pour s'aligner correctement, et reprendre leur progression, milimètre par millimètre.

Les dents de Hapse se plante sur ses tétons, dans sa nuque. Ikomi réagit, par automatisme, habitude et abandon à ses sensations, déroulant les scripts internalisés des XPorn qui ont "fuités"  ou des clips à usage commercial pour promouvoir marques de lingerie, parfum ou maquillage à toute épreuve. Elle ralenti et fini par s'arrêter, les mains posées sur le torse de Hapse qui est encore partiellement vétu de sa combinaison.

"Désolée mais … c'est étrange. On peut … " Elle ne sait pas vraiment comment finir cette phrase. S'arrêter, essayer autre chose ? La familiarité de la sensation la rassure, mais les raisons de cette familiarité la perturbe. Elle aime bien se sentir en contrôle, les sensations que tout cela lui procure, mais ça ne va pas. Trop commercial, trop lisse, trop scripté."

"Ok, on fait une pause, pas de problème. Pas besoin d'en parler." Dit Hapse, en sueur, soulevant avec difficulté sa partenaire dont le poids est augmenté par la forte gravité de l'accélération du Unknown Gravity Dog.

"Non, reste là. Prend moi dans tes bras, et fait moi juste un câlin. Je crois que j'ai juste besoin de ça. C'est OK pour toi ?"

Sans ajouter de mots, Hapse prend Ikomi dans ses bras, en cuiller, et ils restent là, sur le froid métallique de la table, Ikomi absorbant la chaleur d'un Hapse chauffé à blanc qui ressent le besoin de parler mais qui, pour une fois, préfère se taire et juste rester là, contre Ikomi, synchronisant inconciemment leurs respirations et rythmes cardiaques, laissant retomber un peu la tension qui s'est construite entre eux depuis leur cohabitation en pesanteur forcée.