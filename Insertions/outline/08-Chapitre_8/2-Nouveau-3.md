title:          Nouveau_3
ID:             39
type:           md
compile:        2
charCount:      3443



Le sas gonflable se déploi doucement autour d'Ark, s'étendant telle une verrue blanchâtre à la surface du Hit Me. Il conserve sa combi lourde alors que l'atmosphère s'approche de celledu Hit Me, car il n'as pas le temps pour les procédures longues pour adapter son système sanguin à l'atmoshpère d'oxygène pure de l'autre côté de la porte en titane. 0.29 bar et l'ombilique arrête d'augmenter la pression.

D'une impulsion sur son système de commande, Ark déclenche l'ouverture de la porte avant du Hit Me. Alors que le diaphragme commence son ouverture, une boule s'installe dans l'estomac d'Ark. Un retour d'angoisse. Une apréhension. L'intérieur du sas se baigne lentement d'une lumière que les capteurs occulaires d'Ark traduisent comme étant rouge. Vibations. Deux courtes. Une longue. Le système médical détecte une augmentation des niveaux d'oxygène dans le système circulatoire d'Ark. Elle se rappelle l'intérieur de la cabine. Les incessantes poursuites. Le besoin de se défoncer pour éviter de réaliser ce qu'il lui arrive. L'envie de se faire mal pour exercer du contrôle sur ses sensations.

Le corps inerte d'Ikomi dérive vers Ark sous le léger mouvement d'air créé par l'ouverture de la porte. La dissonance cognitive atteint son paroxysme. sans jamais avoir mis les pieds à l'intérieur du minuscule habitacle, celui-ci lui est familier. La console éventrée lui saute aux yeux instantanément. Elle, en revanche, semble obnubilée et concentrée sur cette console. La voir éventrée, dériver, inerte, incapable de parler, de communiquer, de lui dire quoi faire, déclenche une forme de soulagement.

Elle peut se voir flotter vers elle. Elle reconnaît l'image que lui renvoie le miroir qu'elle est obligée de contempler plusieurs heures par jours. Elle reconnaît l'image contractuelle sans se rappeller si elle a toujours été comme ça. Elle se rappelle assembler des silicates en orbite autour d'Io. Elle se rappelle visiter les communautés distantes qui construisent les macro-processeurs dans les Troyens. Elle se rappelle aussi que ces souvenirs ne sont pas les siens. À moins que ce ne soit l'inverse.

Un léger spasme musculaire parcourt le corps d'Ikomi — une faible stimulation  permettant aux cycles biologques fortement ralentis par leprocessus d'hibernation d'assesser de l'étât de fonctionnement des muscles — avant de retourner à son immobilisme. Sa longue tresse arc en ciel s'enroule autour de sa nuque, la cartouche de l'injecteur de drogues flottant non loin de sa tête.

Combattant sa confusion, Ark se concentre sur la tâche à venir. Il arrache le bracelet de communication du corps inerte, pas besoin qu'un assistant vienne s'ajouter au bazard ambiant, et le remplace par un autre, au design plus massif et lance les routines de diagnsotique médical. Il appaire l'appareil aux systèmes de préservation du cerceuil qui, va servir de caisson hyperbare, le temps d'adapter le système sanguin et respiratoire de l'icône pop à l'atmosphère d'helium et d'oxygène du Unknown Artist et, bataillant contre les sensations contradictoires entre ses systèmes mémoriels et sensoriels, réussi à installer Ikomi dans le cercueil et à démarrer le treuil qui va le faire voyager le lon de l'ombilique jusque dans le sas latéral de l'Unknown. Et, avant de dépressuriser le sas gonflable et l'intérieur du Hit Me, Ark attrape la console de la fronde, espérant en apprendre un peu plus sur les raisons de la dérive du Hit Me jusqu'ici.
