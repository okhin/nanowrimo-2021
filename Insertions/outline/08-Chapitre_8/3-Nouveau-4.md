title:          Nouveau_4
ID:             40
type:           md
compile:        2
charCount:      5530



Alors que sa température corporelle remonte et que son métabolisme redémarre, Ikomi reprend lentement conscience. Elle est harnachée sur quelque chose de dur, qui n'est définitivement pas le poste de pilotage de sa fronde, mais probablement plus quelque chose comme l'un des lits de sevrage du Médina qui lui sont bien trop familier.

Quelque chose ne va pas cependant. Le bruit, ou plutôt l'absence de bruit. Elle reprend conscience, il devrait y avoir un assistant pour l'acceuillir, ou un docteur, ou quelqu'un. Peg peut-être. Elle aimerai bien que Peg soit là, maintenant. Mais il n'y a personne pour lui parler, elle est apparement seule alors que les différents agenst chimiques purgent son système des derniers résidus des drogues d'hibernation.

Elle ouvre les yeux. Ce n'est définitivement pas le Médina. Pas de fenêtre, pas de peinture propre, juste la charpente en acier et des panneaux en carbone couverts de sangles, de sigles et de différents outils. Une sensation de pression sur son bras là où devrait se trouver son système de com, qui a été remplacé par un appareil dont la conception semble être issue du siècle dernier.

Au moins elle peut se détacher. Elle se débarasse des sangles, se redresse et tombe au sol, ses jmabes refusant de fonctionner. Le sol est rugueux, brut, froid, métallique. Rien autour d'elle ne semble être conçu pour rassurer un patient ou donner une quelconque sensation de comfort. Elle doit être dans une prison quelconque. Shiitake Overdrive en a eu marre d'elle et ils ont finalement décidé de la remplacer par une autre pourpée à laquelleils vont implanter la personnalité d'Ikomi qui leur permetra, encore, de vendre plus de musique, de scandales et d'XPorn.

"Il y a quelqu'un ?" Sa voix sonne étrangement. Pas comme celle qui correpond au timbre d'Ikomi en tout cas. Elle palpe son larynx mais ne détecte aucune nouvelle cicatrice ou altération.

"À l'aide !" crie-t-elle d'une voix caquetante, au bord des larmes.

La porte de l'infirmerie s'ouvre et laisse apparaître lasilhouette trapue d'Ark, revétu de sa seconde peau. Il s'approche d'Ikomi et lui tend doucement la main pour l'aider à se relever. Pour la proemière fois en plusieurs dizaines de mois, il utilise sa voix pour parler directement à quelqu'un.

"Doucement, il va te falloir un peu de temps avant de pouvoir marcher, tu as passé beaucoup de temps à dériver et tu souffres d'une légère atrophie musculaire."

"Qui êtes vous ?" demande Ikomi, refusant de prendre la main tendue.

"Oh, pardon. Désolé, je … je n'ai pas l'habitude d'avoir du monde à bord. Pardon. Appele moi Ark. Tu es chez moi, dans le Unknown Artist, et je t'ao extraite de ta capsule il y a deux jours, alors que tu étais en train de dériver entre Jupiter et la ceinture."

"Vous n'êtes pas avec Shiitake ?"

"Non. Je ne suis avec personne. Enfin, il y a bien la coop, mais ils son loin d'ici. Et il y a Haspe aussi, qui devrait nous rejoindre dans les prochaines semaines. En attendant, il y a toi et moi. Et c'est tout." Devant son hésitation, et après une pause, il reprend.

"Écoute, il n'y a personne dans les cent cinquantes milles kilomètres alentours, à part le WatUp de Haspe. En attendant, ces juste toi et moi." Les phéromones d'Ikomi commencent à pénétrer son système hormonal et sa posture change, il se rapproche de la star, et avant qu'il en ait conscience, à déjà posé sa main sur sa cuisse. Elle se raidit instantanément, mais elle est piégée, elle nepeut pas marcher, elle est à sa merci.

Sentant la tension s'installer, Ark prend conscience de la situation, et recule immédiatement. "Merde, pardon, désolé, je ne sais paspourquoi j'ai fait ça."

"Phéromones" répond Ikomi dans un sanglot.

"D'accord. On va trouver une solution à ça d'accord ? Il va falloir qu'on partage l'espace ici de toutes façon. Je … Tu veux que je t'aide à remonter sur la banquette ?"

Elle ne répond pas. Ne pas reconnaître sa voix, ne pas reconnaître l'endroit oú elle s'est révilleée, ne pas reconnaître les gens autour d'elle, tout cela est trop pour elle. Elle est hors-script, son organisme purgé des narcorythme manifeste sa désapprobation aux niveaux de stress à laquelle elle est exposée, et elle s'effonde au sol, dans un état de conscience réduite.

Ark, prudemment, la ramasse et, alors qu'elle est dans ses bras, ses instincts lui rugissent de s'occuper d'elle, de la caresser pour la rassurer, de la serrer fort dans ses bras, de l'embrasser, de s'allonger à côté d'elle et de sentir sa peau contre la siennepour la protéger des autres. Des réminiscences traumatiques s'opposent à ce désir synthétique et il installe Ikomi sur la banquette dont elle est tombée, la recouvre d'une couverture plombée pour l'aider à se concentrer sur elle même, et sort de la pièce pour la laisser seule récupérer.

Et, pour la première fois depuis bien trop longtemps, il se précipite aux toilettes, vomir l'anxiété que les chem-mem lui font subir depuis quelques jours. Il sait maintenant pourquoi il subit cette dissonance. Il a revérifié ce matin la formulation des narcorythmes. Ils sont calibrés sur les XP commercialisé par Shiitake Overdrive et qui reprennent les sensations subjectives d'Ikomi. Ark espère que ces expériences sont, pour la plupart, des histoires, doublées par des acteurs et des assistants, et non pas un xtrait direct de ce qu'elle a du subir, mais elle n'est pas folle. Elle a vécu tout ça. Et c'est pour ça qu'il est en train de vider le contenu de son estomac dans le système de récupération des eaux grises du Unnown Artist.