title:          Nouveau
ID:             37
type:           md
compile:        2
charCount:      7578


Cinquante mètre par secondes de vitesse relative. Finalement Ark est en approche finale du RK-16. Encore quelques kilomètres les séparent, mais l'approche finale devrait être simple. Tant que le RK-16 ne tourne passur lui-même.

Il est d'ailleurs tant d'établir le contact avec le transpondeur. Et peut-etre l'assistant qui fait fonctionner encore la fronde. La signature ergétique est relativement faible, le rayonnement thermique ne dépassant que de peu les deux cents kelvins, mais elle est suffisante pour signifier un générateur d'éergie fonctionnel à bord du vaisseau, et donc un minimum de systèmes de survie.

Le transpondeur est enregistré sous le nom de Hit Me, et propriété de Shiitake Overdrive. Le SOS est rapidement confirmé comme proveant bien de ce vaisseau par l'assistant de Park.

Quarante mètre seconde et maintenant en dessous des deux kilomètres. Park arrive peut-être un peu vite, mais il veut dépasser la fronde, une sensation de déjà vu et un mauvais pressentiment commençant à s'installer dans son système hormonal. C'était pas ce vaisseau, ni le Unknwon Artist. Ce n'était probablement pas lui, mais il se souvent avoir déjà ouvert un vaisseau à la dérive, et il ne veut pas se souvenir de ce qu'il a vu à l;intérieur.

Trente mètre par seconde. Ark a ajouté des MET au freinage. Le répartiteur gueule un peu et le Unknown Artist commence à légèrement osciller sous les ratées de la posusées, mais la trajectoire est toujours correcte. Les batteries arrivent en dessous des dix pour cents de charge, mais au moins à cette distance, les rayonnements solaires sont un peu plus intéressant à récolter.

Vingt mètres par secondes. Ark enfile sa combinaison dure et son casque. Il a basculé les commandes de l'Unknown sur son terminal de poignet. Il verrouille son casque et une douleur dans son  système gastrique se réveille. L'anxiété prend lentement le contrôle de son système sympathique et il lui faut un effort de volonté considérable pour forcer son esprit à ne pas aller dériver dans le noir narcorythmique. Il est conscient de son absence de respiration depuis quelques secondes. Des vibrations de la coque. Des rayonnements infrarouges qui émanent du Hit Me.

Ce nom lui rappelle quelque chose, mais il ne parviens pas à remettre le doigt dessus. C'est un nom qui doit avoir de l'importance dans l'espace mémétique des puits, et qui n'en aplus le temps de parvenir jusqu'au distants, remplacé par de nombreux autres mèmes éphémères que semblent apprécier les internes et les Puiseux en particulier.

Dix mètres par secondes. Le Hit Me est à peine visible par la fenêtre, dissimulé dans l'ombre des feuilles solaires qui le coupe de tout photon pouvant permettre une identification visuelle. Le spectre électromagnétique est un peu pus intéressant, à cette distance Ark peut commencer à distinguer les signatures des différents systèmes.

Cin mètres par secondes. L'angoisse termine de consumer Ark qui commence à trembler. Quatre. Il se rappelle. Les chem-mem finissent par éroder sa volonté. Trois. Deux. Un. Vitesse relative nulle. Distance entre les deux vaisseaux cent trente mètres. Ark est libéré de la gravité verticale, mais son système émotionnel est trop occupé à s'effondrer sous les souvenirs pour qu'il le remarque.

Ce ne sont pas les souvenirs en eux-même qui sont la source de cette douleur. C'est la dissonance cognitive. L'écart entre la situation telle qu'il se rappelle et celle qu'il est en train de vivre. La différence de point de vue, de personnes. Face au sas toujours, mais sans combinaison, juste vétu — ou vétue ? Les souvenirs sont imprécis, confus, manquant de chronologie — du simple drap couvrant les lits durs de l'infirmere du Médina.

Et l'appel du vide, juste de l'autre côté de la double porte. L'envie de sortir de cet enfer. La nécessité que tout s'arrête, que la horde cesse de la harceler — définitivement la alors que les remémorations se font plus cohérentes. Et pourtant ce n'est pas sa première sortie dans l'espace. Ce n'est pas son premier sauvetage non plus. Il sait que sa ombi lourde le protège de tout ce qu'il manque dehors, et plus globalement de tout ce qui, malgrès tout, est présent. Mais elle veut en finir. Elle n'en peut plus. Marre des scripts, ds allers-retours sur le Médina pour se désintoxiquer.

Inspirant profondément Ark ouvre la porte du premier sas. Elle sent le froid du métal récemment exposé au vide sous ses pieds. Il assure son ombilical au Unknown Artist. Le baromètre différenciel lui indique que la pression du sas et de l'extérieur sont bientôt équivalente. Elle imagine le froid commencer à brûler sa peau Puis une main, brûlante sur son poignet, et quelqu'un qui crie son nom, la tirant à l'intérieur de la chambre du Médina criant son nom, qu'Ark ne parviens pas à distinguer.

Seul face au vide. Le Hit Me à portée de sespropulseurs individuels, Ark laisse l'angoisse s'estomper. Son esprit éclaircit des pensées parasites maintenant, loin de Haspe, Echo ou KRI. Les pensées suicidaires vont mettre quelques minutes à s'estomper, mais maintenant, elle sait qu'elle est importante pour elle. Elle peut redevnir une suite de composés chimiques inertes qui seront évacués par le métabolisme d'Ark, tout en aillant une brique instable dans les fondations d'une personnalité construite sur de nombreuses autres éléments inadaptés.

Poussant du pouce le joystick de commande de ses propulseurs, Ark s'élance doucement vers le Hit Me. Les outils d'ouvertures sont fermement attachés dans son harnais. Les bretelles abritent différentes batteries à décharge rapide, pas grand chose mais suffisament de joules pour amorcer la combustion des thermites. Un passe numérique avec tous les exploits à peu près à jour qui devraient permettre de forcer les sécurités du sas et au moins entrer. Une bonne grosse clef universelle en acier devrait permettre de gérer les surprises à l'intérieur.

Arrivant par en dessous, Ark caresse le Hit Me, évitant d'exercer trop de pression sur la coque du vaisseau pour ne pas s'envoyer flotter au loin. Pas d'avarie externe visible, mais Ark le savait déjà. En revanche la finition du RK-16 est irréprochable, le travail de peinture est sans doute à la hauteur du travail des ouvriers et robots qui ont assemblé cette merveille de précision.

Les angles sont juste suffisament doux pour être visible, sans pour autant déformer la ligne. Les vérins du cardan du moteur sont cachés sous des écailles métallique qui peuvent s'ouvrir et se déformer, donnant au vaisseau l'aspect organique qui font la spécificité des lignes RP. Plus de soixante pour cents de la surface du vaisseau peut se reconfigurer ainsi, pivotant le bloc moteur et la cloche d'expulsion des gazs sous pression de presque quatre vingt dix degré. Au repos cependant, les séparations entre toutes ces plaques et mécanismes sont  à peines perceptibles, en particulier sous les gants épais des combi lourdes.

Le sas est situé sur l'avant du vaisseau, dans l'axe de propulsion, juste en face du poste de pilotage. Les commandes externes sont protégés par un panneau protecteur, indiqué par les gravures profondes et marquées reprenant les glyphes standards d'urgence. Une session de découpeuse à plasma permet à Ark d'exposer un port broché et de connecer sa console qui se mets en quète d'établir quels protocoles d'urgences permettront de forcer la porte. Il la pose contre la surface du vaisseau, il y en a probablement pour quelques minutes. Et Ark compte bien mettre à profit ce temps pour installer une ombilique et connecter le Hit Me au Unknow Army par une ligne de vie.
