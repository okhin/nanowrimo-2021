title:          Nouveau_2
ID:             38
type:           md
compile:        2
charCount:      1462



L'ouverture de la cabine du Hit Me s'avère plus complexe que prévu. D'abord son assistant doit passer à travers de nombreux NDA qu'Ark ne compte pas signer, NDA qui diffusent un bruit à bande large et semble émetre des flashs stroboscopiques dans une partie du spectre lumineux auquel Ark n'est plus sensible depuis longtemps.

Mais plus que le léger désgarément provoqué par ces agents, ce qui énerve quelque peu Ark, qui commence à perdre sa lucidité, c'est le temps perdu à devoir crasher ces assistants, au lieu de pouvoir récupérer les données sur la composition atmosphérique, l'état de vie des passagers, leurs blessures éventuelles. les choses qui peuvent être relativement importante, pas ces stupides notions de protection de la propriété intellectuelle.

Et qu'est-ce qui peut bien nécessiter de tels mesures ? Certes, le Hit Me est enregistré auprès d'un label de musique, mais il y a peu de chances qu'il contiennent de la musique en cours de production, ou autre. Mais son assistant commence à lui remonter les informations extraites du système de diagnostique du Hit Me. La pression est basse — 0.3 bar — par contre l'atmosphère est composée d'oxygène pur. Et il y a bien quelqu'un à bord, constantes basses, en hibernation induite par drogues. Mais manifestement pas de combinaison dure, pas de casque. Bien, se dit Ark. Il me faut un sas temporaire. Et un cercueil. Et je ne sais quel cocktail de drogue pour réveiller la personne à l'intérieur.
