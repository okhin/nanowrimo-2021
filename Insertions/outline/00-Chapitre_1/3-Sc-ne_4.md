title:          Scène 4
ID:             43
type:           md
POV:            1
notes:          {C:1:Echo}
                {P:0:Saison de Traj'Orb'}
                {C:2:Shi}
                {C:3:Kam}
                {C:4:Yas}
                {C:5:Pierre}
                {W:6:Trasher}
                {W:14:Prigma}
label:          4
status:         2
compile:        2
charCount:      3305



La tête d'Echo s'affiche dans un entoptique surimposé au champd e vision d'Ark.
Cheveux roses, les tâches de rousseurs de son père, les yeux de son autre père et le
nez de sa mère, souriante et, d'après l'arrière plan, en train de célébrer avec
son pod quelque part dans un bar en orbite haute. Le suppresseur de bruit de
fond isole sa voix quand elle lui parle.

"Hey, je suppose que tu as vu les news, du coup j'attache ça rapidement à ton
flux. Je vais bien, j'ai récupéré et Prigma m'a payé les bouts trop cassés pour
être soignés. Du coup j'ai ajouté quelques améliorations fort, disont, agréable.
Par contre, Trasher est complètement défoncé et Prigma tire un peu la tronche,
ils veulent bien me sponsoriser, mais me racheter une nouvelle fronde à chaque
saison semble hors de leur volonté de supporter les jeunes talents.

Bref, ils se limitent à leurs obligations contractuelles. Shi et Kam ont voulus
expliquer à mon agent qu'il y a des choses que Prigma ne peut probablement pas
réparer dans l'anatomie humaine, contrairement à leur slogan. Elles étaient
vraiment furax t'aurais du voir ça. Mais du coup je ne suis pas sûre d'avoir un
sponsor.

Tu connais Yas, ma mère ne veut pas financer ce qui n'est, pour elle, qu'un jeu
futile. Et Pierre peut aider, mais il a pas assez de crédits pour payer une
fronde un peu sérieuse. D'où ce message. On se parle pas beaucoup, mais c'est
surtout parce que c'est ce que tu préfère. Mais j'aime vraiment ça. Pas
tellement le calcul de trajectoire orbitale, quoique si, mais je tiens ça de toi
je suppose, mais surtout faire la nique à ces Puiseux, et leur montrer qu'ils
feraient mieux de rester au fond de leur puits.

Je sais pas vraiment dans quel coin du système tu es encore. À priori LNR me dit
que tu traine autour d'Io en ce moment, donc on ne pourras pas en discuter de
vive voix pendant encore quelques années, mais les sélections commencent
bientôt. Et j'ai une solution à moins de deux kdV pour battre le record sur les
interlopeurs Vestiens, et tu m'as promis que si je résolvait ça, tu me donnerais
un coup de main."

Elle se détourne de l'objectif et ses doigts transitent dans le champ de vision
pour attacher une pièce jointe et suspendre l'enregistrement, avant qu'elle ne
reviennent rapidement dans le champ.

"Tu me manques quand même hein, on essaye de se voir quand tu orbite dans le
coin."

Les doigts en gros plan et, cette fois, la fin du message. Après quelques
secondes nécessaire à assimiler les informations, Ark charge la solution
orbitale dans sa ROM interne pour pouvoir l'internaliser et voir l'œuvre de sa
fille à loisir. Avec Hapse, Echo est probablement la personne qui lui manque le
plus. Les autres membres du pod aussi, mais Yas et Pierre préfèrent vivre leur
vie dans des endroits denséments peuplés, et mener une vie sociale beaucoup trop
mouvementée pour les goûts d'Ark. Et bon, Hapse est, comme lui, un de ces solos
qui passent plus de la moitié de leur vie à plusieurs UA de toutes formes de
conscience. Il aime bien le voir, généralement ils s'amarrent ensemble sur une
partie des Hoffmans Joviens qu'ils tirent régulièrement. Il aimerait cependant
passer plus de temps avec Echo. Elle a vingt cinq ans maintenant, et il n'a
probablement du passer que six mois dans une sphère d'information partagée avec
elle.
