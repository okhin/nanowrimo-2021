title:          Scène 2
ID:             41
type:           md
notes:          {C:0:Ark}
label:          4
status:         2
compile:        2
charCount:      2502



Sa rupture induite par chem-mem le préoccupe. Il ne comprend pas pourquoi son
histoire avec cette personne s'est terminée. Il ne se rappelle plus d'elle, ni
de son nom, ni de son apparence, juste qu'elle est partie un jour. Ou qu'il est
parti. Pareil. Il se rappelle la douleur, la tristesse, les larmes. Ses canaux
lacrymaux secrètent maintenant des solvants permettant de maintenir ses
plaquettes radiosensibles propre, mais il peut toujours pleurer. Il aimerait se
rappeler ce qu'il a dit ou fait qui a amené à cette situation, mais les pillules
de narcorythmes qui délivrent les chem-mem à son système nerveux central
n'embarquent pas de contexte, juste les expériences enregistrées et travaillées,
excisées, découpées et concentrées.

Ark n'aime pas cette tristesse, mais il aime se rappeler que des personnes ont
été importantes pour lui à un moment. Ou le sont encore. Même si tout cela n'est
que illusions induites par composés organiques volatiles ou autres aromatiques,
ces expériences n'en reste pas moins réelles et font partie de son flot de
conscience.

Il termine l'inspection des attaches des panneaux solaires sur le container bleu
dont les marquages ont représentés une plume blanche avant d'être poncés par les
passages réguliers dans le tore de plasma Ionien. Et contrairement aux marqueurs
publicitaires, les panneaux solaires et radiateurs, soumis au même traitement,
doivent rester fonctionnels. Et vu la lenteur de son métabolisme, Ark préfère
être sûr que les panneaux solaires pourront se rétracter dans leurs étuis
blindés avant d'atteindre la périapse de son ultime orbite.

Une vibration sur son poignet droit. Deux longue, deux courtes, une longue.
Niveaux d'iodes. Deux heures restantes. Fin du message. Il est temps de rentrer
et de cycler sa combinaison. Une longue. Une courte. Une courte. Une courte. Une
longue. Une courte. Une longue. Nouveaux messages. Six non-lus. Fin du message.
Il est aussi temps de lire ces messages qui s'accumulent dans son unité de com.
Il a beau être en plein système jovien, les systèmes de communications finissent
toujours par le trouver. La faute a ces collectivistes de Love and Rage
qui ne peuvent s'empêcher de penser qu'une société équilibrée passe par un accès
à la communication à travers tout le système solaire. Même si il n'est pas
vraiment possible de communiquer, mais juste d'envoyer des messages et d'attendre
plusieurs jours ou semaines une réponse. Ark préfère ses chem-mem pour maintenir
un sentiment de socialisation.
