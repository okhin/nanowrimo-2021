title:          Scène 3
ID:             42
type:           md
notes:          {C:0:Ark}
                {C:1:Echo}
                {C:6:Hapse}
                {W:25:Jam's}
label:          4
status:         2
compile:        2
charCount:      3941



C'est en combinaison de vol légère qu'Ark dérive vers son unité de com. Les
messages ont étés téléchargés depuis le réseau d'archives et de relais
trans-Uranien, trois habitats répartis en une orbite solaire quelque part entre
Neptune et Uranus, et qui maintiennent un bruit de fond électromagnétique sur
lequel se branchent les émetteurs et récepteurs standards. Rien à voir avec la
bande passante des systèmes planétaires, mais ça permet de quand même recevoir
des messages, y compris dans la soupe électromagnétique jovienne.

La gravité artificielle est arrêtée, le module com, l'habitat et la réserve
d'eau étant rétractés et repliés derrière l'épais bouclier ablatif en béton qui
va se charger d'ion et d'éjecta d'Io juste avant l'allumage des MET au plus
proche de la périapse pour mettre l'Unknown sur sa trajectoire d'Hoffman, vers la
ceinture d'astéroïdes. Un peu plus de trois ans de balade donc, essentiellement
passés à dériver dans le vide. Il reste six mille quatre vingts "msdv" à brûler,
plus quelques dizaines encore pour finir la sortie du champ gravitationnel d'Io.

Les premiers messages sont deux notifications de primes pour exécutions de
contrats. Les sculpteurs de macro-processeurs ont bien reçus les silicates
carbonés qu'Ark a éjecté il y a quelques mois en direction des Troyens, et les
gratifications sociales négociées, sous la forme de crédit d'eau ainsi qu'une
invitation à passer quelque jours dans le voisinage de Patroclus pour voir les
gigantesques structures d'assemblages orbitales.

Vient ensuite un message d'Hapse, qu'Ark archive immédiatement, les chem-mems ne
sont pas encore disperés, et il préfère avoir de meilleurs souvenirs avant
d'écouter ce qu'Hapse a à lui dire.

Un spam. Pour du gravity porn. Il faudrait vraiment que ses spammeurs apprennent
à cibler leur public pense Ark. Il ne peut pas vraiment fonctionner normalement
dans un champ gravitationnel, la plupart de ses fonctions somatiques ayant été
modifiées pour tolérer le vide, pas les atmosphères gazeuses ou une gravité
supérieure à quelques centi-G. Et bon, la structure osseuse terrienne, ou les
peux hyper-sensibles ne l'excite pas vraiment. Trop fragile, trop subtil pour
ses gros doigts plus habitués à manipuler des silicates que les muscles fragiles
des hardeurs pourtant bio-sculptés à l'extrême.

Une invitation pour un concert de Jam's. De la synth-pop brutale, qui rend
plutôt bien dans les systèmes audio de l'Unknown. Ça ressemble aux bruits
d'accouplements que pourraient faire des frondes surchauffées sur la ligne de
départ. Avec une lead qui a habituellement une bonne présence scénique. Bon, Ark
ne pourra jamais y être, la plupart des dates seront passées depuis un an au
moins quand Ark mettre les pieds sur 4 Vesta. Il devra se contenter des streams
et des codes de chem-mem des personnes du public qui partageront ça.

Enfin le résultat de la dernière course de traj'orb' — à propose de frondes. Ark
n'est pas un grand fan de ce sport habituellement, mais depuis deux saisons
Echo, sa gamine — plus ou moins — est passée pro et fait plutôt de bon scores.
La faute a ses séjours passés dans les sillages des tankers autour des puits de
gravités de la ceinture interne. Quiconque grandit au milieu de ces monstres
automatisés qui détourne et dépouille les comètes développe un instinct pour la
mécanique orbitale qui bat la plupart des assistants de vols qu'utilisent les
Puiseurs — les gamins issus de l'aristocratie gravitationnelle et qui font du
traj'orb' pour se distraire.

Echo a finit deuxième de sa ligue. Mais elle s'est pas vraiment faites des amis
parmi les Puiseurs et la seule raison pour laquelle elle n'a pas gagné cette
année c'est que l'un d'entre eux l'a éjecté hors traj' la forçant au forfait
suite à une collision avec un glace-téroïde mineur. Trois semaines
d'hospitalisation pour Echo, et son Trasheur est bon pour la casse. Ce qui
explique l'annexe ajoutée au flux personnaliśe. Play.
