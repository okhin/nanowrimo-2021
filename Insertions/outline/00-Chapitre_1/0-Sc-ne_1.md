title:          Scène 1
ID:             40
type:           md
notes:          {C:0:Ark}{W:3:Unknwown Artist}{W:9:Io}
                
label:          4
status:         2
compile:        2
charCount:      2732


Planant sur les souvenirs d'une mauvaise rupture sentimentale, Ark orbite en
pleine magnétosphère d'Io, s'approchant de l'apogée de son orbite excentrique.
Encore quelques heures à pouvoir flotter dans le vide spatial, engoncé dans sa
combinaison d'EVA aux couleurs de la coop.

Six mois à accumuler de l'énergie orbitale pour pouvoir ensuite s'éjecter du
système Jovien et rediriger l'Unknown Artist dans une trajectoire d'insertion
vers la ceinture intérieure, et 4 Vesta en particulier. Un retour au monde
social après une saison de culture de silicate. Pas vraiment quelque chose
qu'Ark anticipe particulièrement, mais même lui doit passer se ravitailler de
temps en temps, et l'Unknown doit être révisé régulièrement. Et le trajet
prendra tout de même quelques années.

Pas vraiment un vaisseau mais plus un assemblage ad-hoc d'éléments récupérés sur
des vaisseaux et habitats abandonnés, et connectés les uns aux autres à grands
renforts de torches à plasma et de gel de fullerène. La structure est une
succession de containers chamarrés portant les logos de sociétés disparues dans
les limbes de l'éclatement de la bulle expansionniste, quand l'espèce humaine
pensait encore pouvoir maintenir une cohérence culturelle dominante sur plus
d'une planète. Partant du centre de masse, et s'élançant vers l'arrière, un calice
de réservoirs de carburant, maintenant presque complètement vide, vient
embrasser une corolle de MET.

Le nombre de sondes, satelitte et autres vaisseau qu'il a fallu désosser pour
assembler cette corymbe de propulseurs à micro-onde, ainsi que l'écriture et le
réglage des systèmes permettant de réguler la poussée de cet ensemble hétérogène
rendrait fou n'importe lequel de ces architectes à la mode de la ceinture, mais
on ne peut pas battre l'efficacité de ces MET. Ni leur simplicité qui les
rendent facile à produire, bricoler et réparer.

Et puis il faut bien s'occuper. Et Ark n'as pas vraiment ni l'envie ni les
moyens de se payer une licence pour ces moteurs standardisés.

A l'autre bout de la hampe de conteneur, parsemés de panneaux solaires et de
radiateurs, les uns pointant vers le soleil, les autres parallèles à
l'écliptique, la maison d'Ark. Son monde, le seul endroit qu'il considère comme
étant chez lui. Quelques ridicules tonnes de blindages formant un bulbe
protégeant un petit module de survie, une centrifugeuse extensible pour fournir
quelques dixième de G, et des fermes de lichens et de mycélium, seuls parties
dans lesquels de l'énergie est gaspillée à un éclairage aux teintes fushia, le
reste du module étant habituellement baingé dans le noir. La lumière
artificielle est un gaspillage d'énergie quand, comme Ark, on utilise les
nano-ondes pour percevoir son environnement.
