title:          Nouveau
ID:             85
type:           md
compile:        2
charCount:      3973


Les deux énormes moteus chimiques se sont accrochés sur les flancs du Unknown Gravity Dog il y a deux jours, de par et d'autres du centre de masse de l'attelage. Ikomi ne tiens plus en place. lorsque le freinage démarerra elle ne sera plus qu'à quelques jours de revoir Peg. Une semaine avant son concert. Et elle essaye de réfléchir à quoi dire ce soir là, mais elle sèche. Elle sait qu'elle ne veut pas être la responsable d'une insurrection globale. Elle sait aussi, et Hapse ne rate pas une occasion de lui rappeller, qu'elle ne doit pas se donner trop d'importance, et que ce qui lui arrive n'est qu'une étincelle dans une poudrière.

Elle est dans un état de fébrilité qu'elle n'avait pas ressenti depuis longtemps. Ses jambes peinent à la porter et elle ne parviens plus vraiment à s'almenter, elle veut que ce voyage se termine et elle veut juste sortir de ce vaisseau pour aller ailleurs. Et plus elle se rapprcohe du HTT, pire ce sentiment augmente.

Ark est à côté d'elle, assis sur le banc dans la cuisine qui leur a servi d'espace de vie commun ces deux dernières semaines de freinage. Il n'est pas pressé, lui, de sortir de ce qu'il reste de sa maison, mais il comprend que d'autres veuillent partir.

"Alors, t'es prête pour … ton retour dans le monde ?" dit-il?

"Franchement ? Non. Mais est-ce que j'ai le choix ?"

"Tu l'as eu. Maintenant c'est un peu tard pur faire marche arrière. Mais t'es pas toute seule."

"Ouais, Hapse et toi vous êtes là, heureusement."

Ark prend une profonde inspiration.

"Non. Enfin, si. Pour le moment. Mais on va pas rester. Il y a un moment où on devra repartir. On est pas fait pour ça. Je pensais plus à Peg, à ma fille, à son pod. Si tu veux rester avec elles, tu sera bien entouré. Nous, on ets juste les gars qui t'avont sortis du trou. Le reste … Le reste c'est à toi de le faire."

"Ouais. Merci pour tout en tout cas." Puis, après avoir avalé une gorgée de cet alcool distillé qu'Ark s'obstine à fabriquer, elle reprend. "Tu as un plan pour un autre vaisseau ?"

"Pas vraiment. Faut que je vois la coop, mais ils sont un peu occupés. Et j'ai envie de passer un peu de temps avec ma fille et les gens qui sont importants pour elle."

"Tu te débarasses pas tout de suite de moi alors." répond Ikomi, légèrement amusée.

"Faut croire que non. Et je t'aime bien tu sais, t'es pas facile à vivre, moi non plus, mais je sais ce qui t'es arrivé. Et je sais que tu es aussi quelqu'un de bien, Perds pas ça de vue, oublies pas le sgens qui t'entoure, et tout devrais aller. Et qui sait, dans dix ans quand je reviendrait de je ne sais où, on pourra toujours se boire un verre quelque part."

Même s'il dit que ce n'est pas le cas, Ikomi trouve que cette discussion sonne comme un adieu plus que comme un "on se reveera plus tard". C'est probablement vrai. Ark doit aller voir rapidement un médecin, au moins histoire de contenir les méta-stase et histoire de subir un certains nombre de greffes de remplacement.

Elle prends Ark dans ses ras, et le serre fort. C'est ce qu'ils ont eu de plsu proche comme lien physique, mais elle ne veut pas le laisser partir sans, au moins, un câlin.

"Doucement, doucement, il nous reste deux jours tu sais."

"Et tu sais bien que pendant ces deux jours, on va manger tellement de G qu'on pourra pas vraiment marcher. Alor stu me laisses te faire un câlin, parce qu'après ça on aura plus l'occasion". La voix d'Ikomi tremble alors qu'elle laisse s'échapper une larme. Quelque part en elle, une pensée se déclenche. Au moins ils n'ont pas réussi à m'enlever ça ces connards. Et, alors qu'elle trouve enfin les mots pour commencer son concert — qui sera plus une diffusion live qu'un concert de stade dont elle a l'habitude — le compte à rebours avant freinage s'affiche sur leurs moniteurs de poignets, déclenchant une alere rouge sur le pont, les invitant à aller s'attacher dans leurs fauteuils et à pressuriser leurs combinaisons souples, pour compenser les 3G des deux prochains jours.
