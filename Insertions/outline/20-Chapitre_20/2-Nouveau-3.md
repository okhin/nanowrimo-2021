title:          Nouveau_3
ID:             87
type:           md
compile:        2
charCount:      3654



Le HTT est encore plus impressionant quand on se trouve à côté. L'avant du maxi cargo est ouvert, laissant apparaître une bouche pleines de harpons en tungstène, prêt à aggripper les astéroïdes qui se trouverait sur sa trajectoire, afin de les digérer en lingots de différents éléments chimiques stable, éléments ensuite raffinés et partiellement utilisés pour les différents systèmes de bord ou pour refaire le plein.

N'ayant plus d'astéroïdes à se mettre sous la dent, des extensions ont été connectées à certaines dents, extensions qui servent maintenant de point d'ancrage aux vaisseaux tels que le Unknown Gravity Dog qui, en dépit de sa taille respoectable de pas loin de deux cent mètre, paraît ridiculement petit à côté de cette baleine mécanique.

Ikomi a passé une partie de l'approche finale entre deux niveaux de conscience, essayant de trouver un peu de sommeil d'un côté, et l'excitation d'une gamine qui arrive pour la première fois dans un parc à thème.

Hapse et Ark l'ont laissée dans sa cabine pendant qu'ils sont partis remplir des formalités quelconque. Plus probablement commencer la tournée des potes et des bars après plusieurs années d'isolement partiellement choisit.

Alors elle profite d'etre vraiment seule depuis une éternité pour se reposer. Ça ne va pas durer, dès qu'elle aura mis le pied sur le pont du HTT, la course reprendra. Mais cette fois, ce n'est pas elle après qui ont court. C'est elle qui court vers les autres. Et les autres en ont après quelque chose qui lui a causé tellement de mal qu'elle ne peut que ressentir une forme de jouissance anticipatrice des évènements à venir.

Vibration sur son poignet. Contrairement à Ark, elle n'a pas encore appris à les déchiffrer instinctivement. Elle regarde son communicateur. Un appel. Pas un message, un appel. Quelqu'un qui est à une distance d'elle qui se compte en dizaine de milliers de kilomètres, pas en multiples d'unité astronomique. Numéro inconnu. Pas forcément étonnant en fait. Elle se demande d'ailleurs comment qui que ce soit a pu avoir ce numéro. D'une impulsion sur son poignet, elle décroche en vocal.

"Hey" elle reconnaît la voix et l'intonation de Peg au milieu de la première syllabe. "Ça va ?" Silence.

Il faut un peu de temps à Ikomi pour répondre, elle attend la fin du message.

"Oui. Je reposait mes yeux. Et il faut que j'attende encore … deux heures de balance atmosphérique". Entendre sa voix normale est étrange. Hapse et Ark se sont relayés avant elle pour évacuer l'Helium dans leur organisme et se réhabituer à un mélange d'Azote, et elle a un peu trainé des pieds pour le faire. Elle regrette un peu.

"Je peux attendre deux heures. Si c'est pour retrouver ta voix normale, ça vaudra le coup."

"Et on est plus vraiment à deux heures prêt." Ajoute Ikomi. "C'est étrange, un peu de se parler non ?"

"Un peu oui. Oh, et surtout, reste sur ton vaisseau pour le moment, je te rejoins. Dans deux heures. On pourra passer un peu de temps juste toutes les deux. Après, ça va être compliqué."

"J'imagines." Ikomi se prépare à raccrocher avant d'ajouter "Tu m'as manqué. Beaucoup."

"Toi aussi. Allez, à tout de suite. Faut que je me fasse belle, j'ai un rencard avec une meuf trop bien." Peg raccroche avant qu'Ikomi ne réponde.

Son bracelet de communication se mets à vibrer de nouveaux. De nombreux fournisseurs de services font leur pub. Les souscriptions aux services d'urgences. Quelques règles et régulation. Et probablement quelques menaces. Son numéro turne beaucoup trop, et il lui faudra quelque chose de plus perso rapidement.

Et trouver quoi faire pendant les sept milles cent trente cinq secondes qui la sépare de Peg.
