title:          Nouveau_4
ID:             88
type:           md
compile:        2
charCount:      2566



"C'est quand même mieux quand on peut se toucher non ?" dit Peg, lovée dans les bras d'Ikomi, leurs peaux contrastées collée par la respiration, leur baudriers verrouillé l'un à l'autre flottant dans la passerelle maintenant déserté de l'Unknown Gravity Dog.

"Mmm. Mmm." Ikomi hoche la tête d'approbation. Fatiguée par les deux jours de freinage, la fatigure nerveuse de se retrouver elle-même et de commencer à recoller les morceaux de sa personnalité, mais aussi fatiguée par ce que Peg lui a fait ces deux derniers jours. Elles n'ont pas vraiment quitté la cabine, ni vraiment enfilé de fringue, et elles ont décidées que, au moins pour le moment, le monde extérieur pouvait cesser d'exister.

Encore quelques minutes pense Ikomi. Juste quelques minutes de paix, avant de repartir. Les notifications sur leurs téléphones n'ont cessées de s'empiler. Rien d'urgent, les personnes qui comptent savent qu'elles sont occupées et ont ce qu'il faut d'information pour prendre les décisions. Mais le concert est pour dans trois jours, et elles ne vont plus vraiment pouvoir rester seule entre elles plus longtemps. Et sutout, Ikomi veut manger autre chose que du kale aux haricots. Mais ça peut attendre deux minutes.

"Bon. Pas que j'aime pas être là avec juste toi. Mais on a du boulot à faire. Et Echo va finir par découper le sas à la torche plasma si on continue de ne pas répondre."

"Ouais. Je sais …" Ikomi n'est pas étouffée par l'enthousiasme. Non pas à l'idée de ne pas être avec Peg, ni à celle qu'elle ai développé une relation avec Echo — quoique, mais ce concert lui semble d'un coup une mauvaise idée.

"Allez, enfile quelque chose. Je t'amène à la meilleure taqueria du HTT. Bon, ok, la seule, mais c'est super bon. Et après … shopping je suppose " dis Peg contemplant la combinaison légère d'Ikomi qu'elle a utilisé comme seul vêtemet ces six derniers mois. Les systèmes auto-nettoyants fonctionnent, mais la combi n'ets pas adaptée à sa morphologie, et Peg n'aime pas la couleur.

"Ok, ok. Si tu m'attaques avec des tacos, j'ai pas le choix. Mais je te prends ça." Elle étend le bras et choppe la combinaison légère de Peg, ornée d'un portrait d'Ikomi, celui qui aurait du illustrer la couverture de son album non produit. À une époque où elle allait mieux. Elle traite le souvenir, et se cocnentre sur l'odeur de Peg qui émane de la combi. Et de sa propre peau. Elles devraient prendre une douche, mais elles n'ont pas envie de perdre ce lien olfactif. Et tant pis si tout le monde sait qu'elles ont baisé. Tout le monde le sait déjà de toutes façon.