title:          Nouveau_2
ID:             86
type:           md
compile:        2
charCount:      2549



Même si elle ne l'as vu que l'espace de quelques instants, avec une vélocité relative bien trop importante pour réllement apréhender la taille massive du Heavier Than Thou, il n'en reste que l'impression que ce gigantesque témoignage d'un autre âge a laissé dans l'esprit d'Ikomi stimule son imagination. Rien que la taille de l'équipage nécessaire à manœuvrer ce géant de l'espace est équivalent à celui d'une station orbitale de taille conséquente, comme HBI. Les moteurs à fission à cycle ouvert impressionent aussi. Autant par la taille de leurs buses d'échappement que par la quantité de siverts dont ils devaient asperger leur voisinnage.

Ikomi en a le souffle coupé. Ce qui n'est pas forcément difficile, étant donné que sa cage thoracique pèse trois fois le poids qu'elle pèserait sur terre, et probablement plus de six fois celui auquel elle est habituée. Elle et Hapse sont intubés et respirent grâce à un compresseur qui leur permet de recevoir suffisament d'oxygène pour compenser leurs difficultés respiratoires. Ark lui est asis à son fauteuil, sans nécessiter l'incomfortable combinaison compressive ou les tubes. Une ossature adaptive lui permet de supporter ce genre d'accélération en maintenant une oxygénation correcte du cerveau.

La coque du Unknown Gravity Dog vibre avec les énormes moteurs des deux remorqueurs qui freinent de toute la vitesse de leur panache de fumée, générant un bruit d'enfer dans le vaisseau qui fut relativement silencieux ces derniers mois. Une façon particulière de souhaiter la bienvenue dans l'influence gravitationnelle d'un planétoïde se dit Ikomi, inspirant le mélange riche en oxygène que lui injecte son respirateur.

Encore quelques minutes de cet enfer, et les deux remorqueurs s'arrêtent. Ikomi est projettée contre les sangles qui la maintiennent sur la couche gravitationnelle et ses oreilles se mettent à bourdonner sous le silence soudain. Trente sept heures de rafut, d'enfer et de vibration, mais ils ont une orbite autour de 4-Vesta. La fin du voyage, le début d'un autre, pense Ikomi.

Le vaisseau n'est cependant pas encore arrivé. Les remorquers sont à secs, et les réserves d'eau de l'Unknown Gravity Dog aussi, transformant le moteur à fusion en un simple réacteur alimentant en électricité une structure relativement inerte. Mais à leur apogée, ils seront accosté par un réservoir de carburant, installé là il y a quelques semaines, et ils pourront circulariser leur orbite sur une trajectoire un peu plus pratique pour le ravitaillement et l'accostage de différentes navettes.
