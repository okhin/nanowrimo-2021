title:          Nouveau_5
ID:             82
type:           md
compile:        2
charCount:      3290



L'avantage des freinages long, c'est de pouvoir prendre des douches sans risquer de se noyer se dit Peg, profitant de l'eau chaude juste à côté du neuf cube d'Echo qui s'est effondrée dans le sommeil. Peg l'a attachée dans le hamac, par habitude plus que par nécessité, entre la micro gravité de coriollis et le freinage des Vasimr, il y a peu de chance que quoi que ce soit se retrouve à dériver, mais cest un réflexe ancré dans leur habitude. Comme celui de ne jamais vraiment quitter harnais ou baudrier. Ou de se laver avec une bulle d'eau au lieu d'une douche.

S'ajuster à la vie sur le dernier jardin de Shi s'est avéré un peu plus lent que ce dont elle avait l'habitude. Pas tellement que Peg ait fait de nombreux voyages inter planétaires, mais pour le peu qu'elle en a fait, elle s'était habituée à une vie plus proche de celle des croisiéristes et passagers que de membres d'équipages. Mais ce temps passé à trouver un rythme et des marques lui permet de se sentir chez elle, et non juste comme une passagère temporaire qui devrai quitter sa maison bientôt.

Elle se dirige, justement, vers son neuf cube. Peu importe qui couche avec qui, chacun dort chez soi. Habitude de spaceux, son cube est son pod de survie, et les neufs cubes ne permettent pas de maintenir plus d'une personne en vie. Et ça permet aussi de maintenir une distance émotionnelle si nécessaire, ce qui convient parfaitement à Peg.

Ikomi n'est vraiment plus très loin maintenant, et elle a hâte de l'entendre sans sa voix déformée par l'Helium, de la toucher, de s'enfermer seule avec elle pendant le temps qu'il leur faudra. Sans que personne ne se soucie particulièrement d'elles ou n'attende un truc de leur part. Et en attendant, elle a Echo. Quand elles arrivent à se croiser comme tout à l'heure du moins.

Peg sait que leur relations, née de la détresse émotionnelle d'Echo, n'est pas faites pour durer. Mais ça lui va, à peu près. Elle aimerai réussir à en parler avec elle, mais Echo ne fait qu'esquiver la conversation. Peg a donc décidé qu'elles étaient potes, que le sexe avec elle est en général cool, mais qu'Echo n'est pas capable en ce moment de s'investir plus dans une relation. Peg est OK avec ça. Elle pense. Peut-être qu'elle voudrait plus, mais la Capitaine n'est pas en mesure de lui donner, et elle ne veut pas lui mettre ça sur les épaules. Peut-être une fois que ce bordel sera terminé, et qu'elles pourront se poser pour parler au lieu de juste baiser.

Reste que Peg a hâte d'arriver vers 4-Vesta. Même si ça risque d'être un joyeux bordel une fois là-bas. Même si ça risque d'être douloureux. Au moins elle ne sera pas seule. Elle l'a beaucoup trop été, même quand elle était avec Ikomi, elles étaient seules. Là, elle a un pack sur lequel elle peut compter. Avec leurs faiblesses, leur force, mais aussi leur volonté de se soutenir mutuellement, peu importe ce qu'il se passe, peu importe leurs désaccords.

Et il y a du monde autour de ce pack aussi. Le dojo de Abdoul Xe est pas très loin dans leur sillage. Et une demi douzaine d'équipages divers font aussi route vers le Heavier Than Thou, formant une étrange caravanes convergeant vers un point pour se retrouver autour d'une oasis commune, échanger, partager un moment et résister à ceux qui voudraient leur poser problème.
