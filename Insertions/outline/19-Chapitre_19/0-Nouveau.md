title:          Nouveau
ID:             80
type:           md
compile:        2
charCount:      1361


Le Unknown Gravity Dog est entré il y a quelques jours dans la famille des Vesta. Il reste à l'équipage un peu moins de 0.3 unité astronomique avant d'arriver à leur destination et de finaliser l'insertion autour du corps céleste quasiment dépourvu de gravité de 4-Vesta. De nombreuses combustions à plusieurs G ralentissent le vaisseau, plusieurs fois par jour, rendant la vie à bord complexe, en particulier pour Ark qui est plus habitué aux lentes combustions des moteurs à propulsion électrique qu'il tend à favoriser.

D'autant que l'ambiance à l'arrivé va être tendue. Au lieu de pouvoir s'arrimer dans le voisinage de la coop et de pouvoir passer du temps avec Hapse, Echo et les quelques autres personne dont il apprécie généralement la présence, les voilà qui vont devoir gérer un tas de merde plus gros que la cloche du Heavier Than Thou, le maxi cargo géant que la coop a amené en orbite autour de 4-Vesta et qui leur sert de base d'opération depuis.

Pour la première en cinquante ans qu'il fait ce genre de trajet, Ark n'est pas vraiment impatient de revenir. L'excitation d'Ikomi et le stress qu'elle exsude n'aide probablement pas non plus, et, même si Hapse passe beaucoup de temps avec lui, il ne peut s'empêcher de se demander si il va repartir pour une autre orbite, ou si il va rester coincé dans le HTT en espérant un jour pouvoir repartir.
