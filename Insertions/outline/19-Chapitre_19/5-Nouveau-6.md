title:          Nouveau_6
ID:             83
type:           md
compile:        2
charCount:      3392



Nyasid se promène dans les jardins au cœur du vaisseau. Les idées de Shi n'ont pas toutes été simple à mettre en œuvre — développer une faune d'invertébrés et d'hyménoptères en particulier, mais les résultats sont là. Il passe beaucoup de son temps à regarder fleurire les plantes. Parce que c'est apaisant d'une part, mais aussi parce que c'est un stade critique de la cascade environnementale. Si certaines espèces ne fleurissent pas, alors un effet domino peut amener à un effondrement partiel de l'écosystème.

Et il n'as pas pu installer autant de diversité qu'il aurait voulu pour être comfortable. Alors il passe une bonne partie de son temps dans les jardins, à regarder, observer et prendre des notes, à essayer de deviner ce qu'il se passe dans le substrats à partir de l'état de développement de ses plantes indicatrices.

Effectivement, il ne passe pas beaucoup de temps avec les autres. Mais il préfère la solitude à une socialisation permanente, pas forcément choisie et — même si elle est intéressante — épuisante. Il préfère croiser Peg, Pak, Kam ou Echo de temps en temps. Ou les observer légèrement en retrait, en essayant de comprendre les dynamiques sociales cachées en se basant sur l'état des personnes.

Il va bien, comme rarement il l'a été. Ça lui convient d'être à l'écart. Il sait aussi que, lorsqu'il veut discuter, parler ou autre, quelqu'un sera disponible pour lui. Ou pas, mais elles sauront lui dire, plutôt que de subir une interactions non voulue.

Ses déambulations l'amène vers la proue, là où Pak prépare la plateforme d'accueil. là oú la baie vitrée et la coupole permette de contempler le vide autour d'eux. Ils croisent, de temps en temps, des astéroïdes, surtout depuis qu'ils sont entrée dans la famille des Vesta, mais c'est quelque chose de relativement rare. Même si cela implique, systématiquement, de réveiller Echo pour les manœuvres d'esquives.

Juste avant de rentrer dans ce qui sera un bar, il jette un œil aux productions de céréales. Les poacées séclectionnées poussent bien. Leurs épis sont chargés et leur tiges fine et courtes ne plient pas sous le poids. Pas tant que la gravité reste sous les 0.5G normalement. Cela permet de très bon rendement, et de ne pas être envahi de paille. Les épis sont encore verts, loin d'être récoltables. Certainement pas dans les temps pour démarrer le projet de Pak d'essayer de faire une bière à bord. D'autant que les houblons sont encore vraiment un peu jeune pour ça. Pak devra se contenter de distiller choux et racines pendant encore quelque mois.

"Salut Pak, dit Nyasid en rentrant dans le module de proue, besoin d'un coup de main ?"

"Hey Nyasid, ouais, je suis en train d'installer les faisceaux pour les captures. LNT m'a fournit des schémas et des spécificatiosn mais franchement, on sera mieux à deux pour le faire." Pak sait que non, ils vont perdre du temps à le faire à deux, mais elle apprécie la présence du jardinier, qui a toujour sl'air un peu perdu, et au moins elle peut parler à quelqu'un pendant qu'ils déroulent les faisceaux de fibre optique fraichement imprimés.

Et il y a pas mal de boulot à faire dans les derniers jours de gravité leur permettant de déplacer les facilement les meubles récupérés du Xenon Girl. Bientôt Echo les clouera au sol dans un freinage de capture, et ils devront subir quelques jours à plus de 1G, rendant impossible le travail d'installation.