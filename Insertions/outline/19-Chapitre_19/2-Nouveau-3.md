title:          Nouveau_3
ID:             80
type:           md
compile:        2
charCount:      1913



Hapse n'aime vraiment pas les insertions rétrograde. Il faut compenser énormément de delta-V, le destination ajoutant sa vélocité aux quantité astronomiques — littéralement — de masse à éjecter pour ralentir suffisamment et inverser sa propre vélocité pour rattraper une trajectoire orbitale. Et c'est pas 4-Vesta, avec sa masse ridicule, qui va aider à ralentir.

Et cela les oblige a rester clouer sur une surface plusieurs heures par jour et à lutter contre le poids de leurs propres corps. Il est fait pour les volums, pas les surfaces. Il préfère se déplacer affranchit du poid. L'Unknown Gravity Dog est conçu autour de ce principe, le seul module rotatif est celui de l'infirmerie de l'Unknown Artist.

Au moins la cohabitation se passe bien. Mieux que ce qu'il a imaginé lorsqu'Ark lui avait envoyé le message lui disant qu'il allait secourir une fronde en détresse. Et ils seront bientôt pris en charge par les remorqueurs détachś par la coop pour finaliser leur approche, Hapse n'ayant définitivement pas suffisament de carburant pour freiner suffisament et se faire capturer par 4-Vesta. Le port d'HTT à donc dépécher une paire de remorqueurs, esseniellement des gros boosters chimiques qui vont cramer plusieurs centaines de tonnes de carburants en quelques heures avant de devenir des masses inertes, afin de mettre l'attelage sur une orbite eccentrique, au moins le temps de se ravitailler et de pouvoir ensuite aller parquer l'Unknown Gravity Dog sur une orbite prograde.

Toutes ces manœuvres vont encore coûter cher en crédit d'eau et en eks. LNR en prend en charge une partie, et la coop aussi puisque ces surcoûts sont liès à un sauvetage, mais il va rester une note salée à payer d'une manière ou d'une autre. Enfin, si tout se passe bien, que le concert à lieu, que les copycops restent à l'écart et que Hapse survit aux heures de poussées rétrograde encore nécessaire pour finir leur trajet.
