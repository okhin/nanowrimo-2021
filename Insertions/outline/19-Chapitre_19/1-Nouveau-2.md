title:          Nouveau_2
ID:             79
type:           md
compile:        2
charCount:      2264



Deux minutes trentes de lag avec le HTT et les services de la coop. Pas encore assez court pour des appels directs, mais ça devient un peu mieux pour des quasi-conversations. Cinq minutes entre l'envoi et la réception. C'est aussi, paradoxalement, plus stressant. Quand il faut plusieurs heures à un mssage pour nous joindre, on ne l'attend pas devant un écran en espérant avoir de bonnes nouvelles. En revanche, moins de cinq minutes ? Ikomi reste plantée devant le moniteur à attendre les réponses aux messages qu'elle envoie à HT-3, un des opérateurs de Love And Rage qui fait la liaison avec le HTT et la coop.

Un quart d'heure de lag avec le dernier jardin de Shi. Une demi heure avant une réponse. Ça fait plus de six mois qu'elle n'a pas été aussi proche de Peg, et pourtant cette dernière semble être encore beaucoup trop loin. Elle l'imagine, chevauchant les éjectats d'Argon ionisés, droit vers Ikomi, suivant une trajectoire d'impact pour la retrouver plus vite.

"C'est compliqué pour le HTT de prendre officiellement parti pour Ikomi. Ils dépendent encore un peu trop des corpos. Du moins, ils pensent que c'est le cas et, dans le temps que nous avons, je ne peux pas leur faire changer d'avis." Le message d'HT-3 est arrivé.

"En revanche, vous avez du soutien et ils peuvent fournir les mêmes services qu'aux autres. Moi je lis ça comme une invitation à venir vous arrimer, et à utiliser de leurs systèmes de diffusion. Par contre, vous ne pourrez pas faire le concert depuis l'enceinte du HTT.

"Oh, et ils m'ont dit d'ajouter que la dernière fois qu'un copycop a essayé de saisir quelque chose, il a eu un accident bête et que le HTT est un environnement dangereux, il pourrait y avoir d'autres accidents.

"Bref, désolé de pas pouvoir te fournir le truc que tu voulais, mais si vous avez un endroit depuis lequel faire ton concert, on pourra le diffuser dans le HTT et ailleurs. Juste, ça peut pas être ici. Bon, faut que je te laisse, deux milles trucs à gérer ici."

Pas la même voix que d'habitude note Ikomi, mais HT-3 pourrait très bien être plusieurs personne. Elle se doutait de la réponse de la coop, Ark l'avait prévenue, mais elle a un plan de secours. Et au moins, si elle a bien suivi, elle sera relativement en sécurité dans le HTT.
