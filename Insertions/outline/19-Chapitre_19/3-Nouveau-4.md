title:          Nouveau_4
ID:             81
type:           md
compile:        2
charCount:      2732



La configuration Sandy se comporte parfaitement. Le vecteur de poussée s'aligne parfaitement avec le centre de masse, centre de masse qui est relativement central, en particulier par rapport aux vaisseaux à radiations, permet une agilité particulière lors des manœuvres de retournement et les Vasimr jumelés sont parfaitement équilibré. L'un dans l'autre, le premier voyage du Dernier jardin de Shi se passe extrêmement bien note Echo, en prenant son café dans le module commun à la poupe du vaisseau.

Les fleurs plantées par le discret Nyasid sont en train d'arriver à maturité. Les iris plongent leur racnes filtrantes dans les réservoirs d'eau grise, les soucis calendulaires tournent leur capitule en direction des lampes cycliques fournissant en ce moment un spectre plutôt froid censé illustrer le matin quelque part sous une atmosphère azotée. Des faux lilas rampent sur les cloisons, libérants leurs inflorescence mauve de manière désordonnées, grimpant avec les jasmins et les vignes sur une structure délimitant les volumes, créant de multiples alvéoles parfumés et colorées, cassant l'impression de vide que ces modules peuvent parfois fournir.

Kam est assise en face d'elle, lui tenant la main, buvant une tisane. Elle a finit son quart et va aller se coucher alors qu'Echo démarre le sien. Le décallage des heures de garde les tiens un peu à l'écart, mais elles profitent chaque jour d'une heure ensemble, rien que toutes les deux, souvent en silence, et généralement dans ce jardin. Elles commencent à retrouver un équilibre émotionnel et se tenir un peu à distance l'une de l'autre y contribue fortement, même si Echo préfèrerais passer plus de temps avec Kam.

Mais ça veut aussi dire qu'elle a du temps pour réfléchir, pour s'approprier son vaisseau et son équipage et prendre le temps de connaître un peu plus Peg. Ou d'essayer de faire sortir Nyasid de son trou. Ce dernier passe son temps à la rassurer qu'il va bien et qu'il est content d'être seul et de pouvoir passer du temps à s'occuper des plantes, elle a du mal à gérer cette distance.

"Hey, dis moi, ça se passe bien avec Nyasid ?" Finit par demander Echo.

"Oui. Très bien. Adorable, fait sa part du taff. Il semble passer pas mal de temps seul avc lui même et je l'ai vu parler aux quelques insectes qui trainent dans le vaisseau. Rien d'anormal. Pourquoi ?" Répond Kam, connaissant déjà la réponse d'Echo.

"Je sais pas. Il parle pas. Il partage peu de moment avec nous. Je m'inquiètes un peu."

"Il tiens probablement plus du distant que du spaceux. Mais il passe du temps avec nous, à écouter. Il rit aux blagues de Peg aussi. Et il traine vraiment beaucoup dans la serre, mais il va bien. Et oui, je vais garder un œil sur lui. Après ma sieste."
