title:          Nouveau_2
ID:             51
type:           md
compile:        2
charCount:      11387



"Alors, finalement, tu t'es décidée à faire demi-tour ?" La voix de l'assistant d'Ikomi est devenue un afficheur de texte. Ark a bricolé un semblant d'interface pour essayer de récupéré des informations qui, potentiellement, pourrais avoir une quelconque valeur. Sa recherche n'as pas été conclue, mais Ikomi peut parler avec son assistant maintenant.

"Pas vraiment. Je … je sais même pas pourquoi je t'ai redémarrée."

"Tu as une décision à prendre, et comme tu es incapable de le faire, tu attends que les scripts te diesnt quoi faire." Après une pause, le curseur continue d'afficher du texte. "Attends … je peux appeler personne. Et j'ai pas accès à ta navigation, ou à tes constantes. Il se passe quoi ?"

Ikomi éteint la console bricolée. L'assistant a raison, elle ne sait plus comment prendre de décision. On lui a juste appris à se rebeller contre une partie des scripts, mais même ça n'existe que pour alimenter le scandale et la machine à ek de Shiitake. Elle ne sait plus ce dont elle a envie, si elle l'a jamais su. Elle sait que, quelque part, il y a cette jeune fille un peu perdue qui voulait être une star, et qui déambulait dans les rues de Tenochtitlan. Ou de Valles-New Beijin. Ou d'une autre des mégapoles des puits.

Elle se rappelle de flash, de souvenirs. Des soirées, la drogues. Les copines — qui faisaient toujours plus de vues — et les copains — qui étaient toujours plus abusifs. Les sex-clips qui sont devenus campagne commerciale avec placement produit. Les contrats. Assez étonnament, assez peu de concert et de souvenirs de tournées. Probablement pour le mieux. Et Peg, quelque part dans tout ce merdier. Elle ne se rappelle plus comment elle l'a rencontrée, mais elle sait qu'elle veut la revoir. Et … Elle ne sait pas. Juste la revoir déjà.

"Hey, tu vois, je sais ce que je veux. Dès fois." dit-elle, à l'adresse de l'assistant impassible. "Et Hapse a raison, il va bien falloir faire quelque chose, j'en suis réduite à parler aux objets.".

"C'est pour ça que je désactive la voix en général." la voix au timbre métallique la fait sursauter. Elle se retourne sur sa couchette pour le voir apparaître dans l'ambrasure de la porte. "Hapse a dit que ça nous ferait du bien de parler. Et il a raison en général. Donc voilà." Il sort les mains de derrière son dos et pose deux timballes cabossées en aluminuim, sur lesquelles on peut encore deviner le logo de corporation de fret, et une gourde pressurisée.

"Distillat de kale, et sirop de fraise. Fait maison. Ça sert aussi de dégripant, sans la fraise du moins." Il sert deux verres, en pose un dans les mains d'Ikomi et avale le contenu du sien quasiment d'une traite. Les vapeurs d'éthanol saisisse Ikomi à la gorge, et il lui faut bien une trentaine de secondes avant de distinguer le subtil parfum de fraise. Hors de question qu'elle boive ça.

"Ouais, c'est peut-être un peu fort. Je me rends pas bien compte, les radiatiosn ont foutus en l'air pas mal de mes systèmes de perception du goût. Mais il paraît que c'est comme ça qu'on fait. On se met autour d'une table, on se bourre la gueule, et on boit."

"Je sais pas trop. Moi ça implique plutôt d'être beaucoup moins habillés. Et, si j'en crois mes plus hautes vues, plus je morphle mieux c'est. Mais après on peut parler. Quand plus personne ne regarde ou n'écoute ce qu'on a as dire."

"Bon, ne faisont donc aucune de ces choses alors. Et puisuqe c'est moi qui amènes la conversation, tu commences. Pose-moi une question, ou je sais pas, dis-un truc. Je t'écoute." Ark s'est maintenant assis sur une étagère. Les jambes croisées, il bat la mesure de sa main droite de manière incontrôlée. Sa respiration est irrégulière et, en dépit d'une certaine décontraction affichée, tout laisse à penser qu'il ne l'est pas.

"Est-ce que je t'emmerde ?" Répond-elle quasiment sans réfléchir.

"Oui." Répond Ark au bout de quelques secondes. "Comme tout le monde. Oui, même comme Hapse. C'est mon problème, pas le tien, et c'est pour ça que j'ai passé la plus grande partie de mon temps en isolation."

Ikomi attends qu'Ark ajoute quelque chose. Qu'il développe, mais rapidement elle comprend qu'elle n'en saura pas plus. Que personne n'en sait probablement plus, et que ce n'est pas ce qui est important pour lui.

"J'avour", finit-il par reprendre, "j'ai été curieux. Et j'ai regardé ce qui t'es arrivé. Sur KPI et ailleurs, mais essentiellement cette poursuite final, ce dernier script. Ce qui a démarré une réaction en chaîne d'évènements qui ont amené à la rupture de l'axe central. Entre autres choses. Bref, je me suis posé la question. Tu t'en senti comment au moment où tu as fait demi-tour pour aller percuter ces personnes qui rendaient ta vie misérables ?"

Il faut quelques instants à Ikomi pour processer l'ascensseur émotionnel. Elle ne parvient pas à savoir si il la tiens responsable de la rupture de l'axe centrale, ou si il est réellement curieux.

"Tu m'en veux pour ça ? Tu penses que c'est ma faute ?" demande-t-elle, sur la défensive.

"C'est important ce que je pense ?"

"C'est important ce que pense les autres de moi. Surtout si on est coincés ensemble."

"Alors je ne pense pas grand chose de toi. À part que tu n'as peut-être pas eu beaucoup de choix dans ce qui t'es arrivé. Et que peut-être que oui, tu as une part de responsabilité, mais je ne pense pas qu'une personne seule puisse causer autant de destruction. Il faut un ensemble de conditions pour ça, une tempête parfaite ou un truc du genre."

Responsable donc. Pas coupable, aps de ressentiment, mais responsable. Ça fait un peu mal de l'entendre de manière posée, pas venant de centaines de millions de personnes qu'elle ignore et qui lui demande sa tête, son cul sa chatte ou n'importe quelle autre partie de son anatomie. Mais Ark semble soit être déconnecté de toutes empathie, soit considérer cette problématique de responsabilité de l'ordre du détail.

"Pour répondre à ta question, je sais pas vraiment." commence-t-elle après une paire de profondes inspirations "C'est confus. J'étais en pleur, je me souviens. Je ne sais même plus pourquoi la horde me courrait après, je voulais juste que ça s'arrête, que je puisse pleurer seule dans ma fronde sans que ça ne soit repartagé des millions de fois, sans que ce ne soit exploité par mon contrat maquillage qui peut prouver que leur mascara est vraiment à l'épreuve de la vie, sans me soucier du respect des scripts. Et en colère aussi. Contre eux, mais aussi contre le reste du monde. Une envie d'en finir, quelque part. De détruire ce persona, ce produit publicitaire qu'est devenue ma personnalité. Et de leur faire mal. Profondément mal. D'éclater leur os, de voir leur sang gicler dans le vide alors que leurs combinaisons se dépressurisent. D'imaginer leurs yeus sortir de leurs orbites alors que l'air dans leurs poumons se gèle. De voir leurs regards d'agonie me fixer, probablement en train de se demander comment en profiter. Ou peut-être de réaliser ce à quoi ils ont participés."

C'est la première fois qu'elle le formule à voix haute. Elle ne se sent pas vraiment bien, mais le dire, l'externaliser, repasser à travers les évènements dans sa tête, semble lui enlever quelque chose, lui donner du recul. Elle remarque qu'Ark s'est resservi un verre de son alcool de kale.

"T'en veux ?" Demande-t-il après aboir remarqué sa fixation.

"Non, toujours pas. Je sais même pas comment tu fais pour boire ce truc." Ils restent là quelques instants. En silence, seul les vibrations des compresseurs qui alimentent les découpeuse à plasma se font entendre, Hapse est dehors, à tailler le Unknow Army pendant qu'ils gèrent ce qu'ils ont à gérer.

"Pourquoi cette question tiens ? Pourquoi ce moment précis  et pas un autre ?"

"Il faut un point de départ à une histoire non ?" Répond-il, avant de se rendre compte que ce n'est pas vraiment ça la réponse. "Je … Tu te rappelle quand je t'ai récupérée ? Je suis pas sûr que j'étais la même personne à ce moment. Tu t'en rappelles toi ?"

"Oui." Pas besoin de détails, ça reste quelque chose d'étrange comme premier contact.

"J'étais en pleine crise d'idnetité. Un truc con. Pour me souvenir, pour essayer de comrpendre ce qui donne envie aux gens de revenir régulièrement voir leurs congénères, je prend des narco. Chem-mem. Rien de ouf, plutôt des expériences désagréables, mais c'est aprce que c'est probablement les trucs les plus authentiques, les moins joués. Et c'est ceux qui me font avancer. Bref. Donc. Je pense que j'ai récupéré quelques uns de tes souvenirs. Plsu que quelques uns manifestement. Ou des autres versions de toi, je sais pas combien d'Ikomi vous avez été. Mais globalement, il ne vous arrive pas des trucs cools, mais vous avez toute cette … proprioception ?" Non, pas le bon mot.

Ark ne trouve pas vraiment de mots pour décrire cette sensation d'habiter un corps, à quel point ça peut donner une sensation apaisante de savoir quelle volume on occupe, quelle forme son corps a, de pouvoir se reconnaître dans un miroir, de ne pas se dire qu'il va devoir falloir changer plus de la moitié des masses graisseuses, des muscles, des tissus et des os de son propre vaisseau corporel lors de son prochain arrêt dans une station.

"Bref, proprioception. On peut savoir quand un souvenirs est un syncrétisme de plusieurs expérience. Il y a quelque chose de perturbant. Mais dans ton cas, ou votre cas, on sent que vous aveez une forte conscience de votre corps, dans les moindres détails. Et donc, j'étais en train de tripper sur des crises suicidaires, avant de venir te sortir de là et, lorsque je t'ai vu, je me suis vu, et je suppose que le machin qui me sert de cerveau a relativement mal vécu la chose, il a du penser qu'il était pas dans le bon corps.

"Pas l'expérience la plus agréable. Et j'imagine que ça a pas du être fantastique pour toi non plus. Je suis désolé qu'on ai commencé comme ça."

"Ben, tu n'as pas essayé de me sauter dessus, tu m'as pas demandé quoi que ce soit. Alors certes, c'était étrange, et je comprends un peu mieux certains trucs, et il me faudra du temps … Mais en fait ça va. Enfin, ça, ce bout là. Ça va." Répond Ikomi.

Elle réfléchit. Elle réfléchit à ce qu'elle veut faire, à ce qu'elle a envie de faire ­— revoir Beth, mais elle sent qu'elle va devoir faire plus que ça. Pas tellement qu'elle doive se faire pardonner des trucs, mais peut-être qu'elle peut expliquer. Ou donner sa version des choses. telles qu'elle s'en souvient.

"Dis, rien à voir. Mais, si je veux faire un enregistrement et le diffuser, je fais comment ?"

"Vois avec Hapse. Il a des contacts avec Love and Rage et moi, tu sais, les gens...."

"Les gens t'emmerdent. J'ai bien noté. Bon, je vais attendre qu'il rentre donc, et en attendant je vais prendre une douche." Elle se lève et, voyant qu'Ark n'as pas vraiment bougé. "Tu reste si tu veux, mais c'est pas une invitation à me rejoindre ou autre." Au moment de sortir de la pièce, elle repasse la tête dans l'embrasure de la porte. "Et merci de la discusssion. Tu diras à Hapse qu'il avait raison."

"J'en décuit d'on est OK ?"

"Oui, on est OK."

Ark s'allonge sur la couchette libérée par Ikomi, essentiellement parce qu'il ne comprends pas vraiment la nécessité de garder des couchettes séparés, et se verse un verre de son immonde alcool de kale. Il a menti, il sent parfaitement la sensation de brûlure alors qu'il avale le liquide. Mais c'est bien ça qu'il cherche.