title:          Nouveau
ID:             50
type:           md
compile:        2
charCount:      5156


Ikomi avance prudemment sur les tirants de l'Unknown Artist. Ça va faire une semaine, ou deux — le temps semble passer étrangement au sein de cet équipage — et elle commence un peu à prendre ses marques. Après l'impulsion initiale, et avant de consommer trop de carburant, ils doivent procéder au démontage de ce qui n'est pas suffisament efficace, c'est à dire tout ce qui se situent sous les caissons d'hydroponie du Unknown Artist.

Et elle fait sa part. Ça lui permet de garder la tête hors de l'eau, loin de sa vie précédente. Elle essaie de ne pas réfléchir à ce qu'il s'est passé sur RKI, car elle ne peut pas s'empêhcer de pensée que c'est de sa faute. Ni au fait qu'Ikomi est annoncée sa séparation avec les Jam's et qu'elle lance une grande tournée des planètes interne pour annoncer son nouveau projet solo.

"Tout va bien ?" Demande Hapse, ayant remarqué le changement de rythme dans la progression d'Ikomi.

"Ça ira mieux quand tu arrêtera de poser cette question." Ikomi n'as pas vraiment envie de parler. Certainement pas là, dans le vide. Elle serait plus à l'aise sdans un lit, assise sur son partenaire. Mais elle essaye de savoir quelle part d'elle-même ramène toute interaction sociale au sexe, et quelle part vient du processus de conditionnement. Et Hapse n'aide pas.

Elle trouve plus simple de parler à Ark. Il n'essaye pas de la réparer ou de la faire aller mieux. Elle n'arrive pas à savoir si il lui accorde de l'importance. Mais cela rend plus facile pour elle le fait de lui parler, de lui dire ses pensées noires, ce qu'il lui est arrivé, ce dont elle se souvient, mais surtout des trous de mémoire, des journées ou semaine qu'il lui manque. Ça ne résoud rien, ça ne lui fait pas du bien d'en parler, mais c'est la seule chose qu'elle arrive à faire pour le moment.

Ça et démonter les systèmes de propulsions du Unknown Artist. Elle a une compréhension instinctive du système complexe de propulsion et d'équilibrage du vaisseau, et elle aime vraiment se servir de ses mains. Ça et le mélange atmosphérique de sa combinaison chargé au protoxyde d'azote, lui donnant une sensation de légère euphorie.

C'est pas la chose la plus sûre au monde, mais c'est une solution temporaire. Il faut sortir et se relayer pour démonter le vaisseau d'Ark. Et la seule façon que Hapse à trouvé pour la faire sortir c'est ça. Des sutures temporaires sur une fracture ouverte est l'expression qu'l a utilisé. Ça arrête les saignements, et ça permet de stabiliser, mais ça va faire encore plus mal quand il faudra rouvrir et corrigé la fracture.

Et Ikomi ne peut s'empêcher de penser qu'il faudra agir avant que les os ne se resosudent de travers. Alors elle gratte la plaie régulièrement, elle se plonge dans des introspections destructrices lors de ses pauses. Contemplant la facilité avec laquelle elle pourrait juste sortir et passer par le sas. Elle semble se rappeler d'avoir déjà fait ça, mais c'est flou, mal défini, comme grossi par lentille logicielle.

Elle finit par se glisser dans le sas d'entretien, et, une fois que Hapse l'y a rejoint, ils traversent les containers maintenant dépressurisés, et ils se dirigent vers la partie pressurisés de ce qu'ils appellent maintenant le Unknow Graivty Dog. Les ronds de lumières blanches projettés par les épaules montés sur leurs combinaison sont les seules sources d'éclairage, par net contraste avec l'extérieur, et les rayonnements solaires qui, même à cette distance, commencent à chauffer la coque du vaisseau.

Une fois pressurisé, au milieu des aloés vera, Hapse attrape Ikomi par l'épaule.

"Écoute. Il vaut mieux que ce soit en tes termes, mais il va falloir parler de toi, de ton état, de RKI et du reste. Pas maintenant, mais je préfère qu'on évite trop de crises pendant le trajet, et même pour toi, ça sera mieux."

Ikomi se dégage d'un coup d'épaule, oubliant temporairement l'abesne de gravité, et elle se cogne contre un des panneaux en verre abritant des pousses de kale mauve.

"On dirait mon assistante. Tu trouveras ce dont tu as besoin dans ses tripes. Et je gère. Je vais pas bien, mais je gère. Et Ark, tu t'occupe aussi de ses problèmes, ou lui il a droit de ne pas en parler et de rester dans son coin à digérer le fait qu'on l'ampute de son vaisseau ?"

"Ok, ok, on la joue comme tu veux. Tu sais ce que j'en pense, mais c'est toi qui décide." Hapse la dépasse, bouillonant intérieurement. Puis, marquant une hésitation, il se retourne. "Et Ark n'as pas besoin d'aide. Ou, au moins, il a pas besoin de ga euphorique pour sortir de son trou et il ne nous mets pas en danger en risquant de s'effondrer au pire moment possible. Toi si. Je peux pas te forcer. Je veux pas te forcer. Mais il va falloir que tu acceptes que, à défaut d'être là pour t'aider, la survie dans ce genre de situation se repose sur le fait qu'on puisse compter sur les autres dans des situations de stress intense. Et c'est pas ton cas. Et tu le sais."

Il ne lui laisse pas le temps de répondre et rentre dans le module central du vaisseau, autour duquel le bras assymétrique du Unknown tourne lentement, fournissant une micro-gravité à à ce qui est devenu le quartier d'habitation d'Ikomi.
