title:          Nouveau_3
ID:             70
type:           md
compile:        2
charCount:      3162



Ark dévie de sa trajectoire mémorielle habituelle. Elle essaye de se créer des souvenirs à lui. Et elle se rappelle qu'il n'aime pas vraiment ça, c'est ce qu'elle fait lorsqu'il est, normalement, accosté à une station pour refaire le plein. Mais elle n'est pas sensée lui créer des souvenirs pendant les longues traversées du vide spatial, elle absorbe les souvenirs des autres pour, justement, ne pas se souvenirs des longues heures seuls.

Mais là, il n'est pas seul. Elle est avec elle aussi. Enfin, avec Ikomi. Les souvenirs se décante un peu et il se rappelle les abus qu'elle a subit. Ikomi lui a laissé un disque avec certains de ses souvenirs à elle, ceux dont elle ne veut plus. Ark a essayé de lui expliquer que ce n'est pas comme ça que ça fonctionne, qu'on ne peut pas vraiment s'enlever des souvenirs ou ds expériences, elle a insisté. Et Ark a commencé à intégrer tout ça.

Elle se rappelle les galères dans les quartiers néons de Valles New Beijing, après avoir fuit sa famille de fermiers. Elle se rappelle les soirées et les nuits d'abus subit et infligés. Elle se rappelle la fascination pour les idoles qui alimentaient ses rêves chimiques. Elle se rappelle grandir vite, très vite. Trop vite. Et finalement rencontrer celui qui deviendra son agent, qui lui fera plein de promesse, qu'elle s'empressera de signer. Elle se rappelle avoir compris, trop tard, qu'elle ne s'appartiens plus, qu'elle n'est plus qu'un objet mémétique, copyrighté, duquel on a supprimé toute capacité d'agir. Elle se souvient des thérapies supposées, essentiellemet du conditionnement mental, lorganisation de sa dépendance à différente substance, ses refus timides non entendus, et de plus en plus de substances, perdant tout contact avec un sens de soi.

Mais elle se souvient aussi être sur scène, et aimer ça. Sentir l'énergie chimique et électrique d'une foule la porter. Elle se souvient se demander si ce plaisir pur, ce bonheur, tout ça, vallait le coût qu'elle paye. Et elle se souvient des afters qvec les différents groupes. Organisés, scriptés, cadrés comme des clips publicitaires — ce qu'ils sont — comme pour lui rappeler que ses seuls moments de liberté sont sur la scène des complexes culturels, pour lui donner envie d'y retourner de plus en plus, de subir de plus en plus.

Elle se rappelle aussi de Peg. Une rencontre improbable avec une roadie alors qu'elle s'était négociée une pause seule avant un concert. Elle se rappelle que, pour la première fois de sa vie, elle avait été considérée comme une personne. Cassée, peut-être, mais une personne. Elle se rappelle avoir eu envie de la revoir, avoir essayé de l'embrasser et qu'elle lui avait répondu non, tout en lui donnant son numéro, pour l'appeler si elle était vraiment sérieuse.

Ark se rappelle maintenant de tout ça. Avec la clarté que peuvent avoir les souvenirs chimqiues, avant qu'ils ne soient internalisés et intégrés à l'ensemble de ses expériences. Une remémoration vivide, intense. Stressante comme une redescente de MD, mais éclairante comme une thérapie.

Elle sait qu'il n'est pas Ikomi, elle se rappelle d'autres vies. Mais elle sait qu'elle n'est plus vraiment Ark non plus.