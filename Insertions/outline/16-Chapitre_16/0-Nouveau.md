title:          Nouveau
ID:             68
type:           md
compile:        2
charCount:      5271


Le plus dur dans les anniversaires, ce sont les personnes qui ne sont pas là. Quelques soient les raisons. Les personnes que l'on invite, que l'on a envie de voir, et qui ne viennent pas. C'est pour ça qu'Ark a arrêté de fêter les siens il y a longtemps. Elle se souvient du sentiment d'abandon, de trahison, quand — comment elle s'appelait déjà ? Quand elle n'avait pas pu venir au sien. Une des gouttes d'eaux qui l'a amenée à, finalement, se barrer, quitter l'orbite de ces fermes de poussières et à se planter là, au port, à attendre que le vqisseau qui doit la conduire loin d'ici accoste.

Trahie par tous, surtout par celle qu'elle aime. Ou qu'elle croit aimer, après tout, elle ne se rappelle pas vraiment avoir jamais aimé. Du moins, pas maintenant, pas dans les zones actives de ses souvenirs, pas dans les expériences dont elle arrive à se rappeller.

Peut-être qu'elle apprendra plus tard qe ce sont ses parents, au final, qui ont empêché — Ark ne se rappelle pas de son prénom — de venir. Pour des raisons religieuses, ou idéologiques, ou quelconque. ou peut-être qu'il lui est arrivé quelque chose de grave. Mais elle ne se rappelle pas, elle ne se remémorre pas. Elle se rappelle juste la douleur et l'abandon.

Ces derniers jours en poussée gravitationnelle n'ont pas été simple. Elle pensait, originellement, pouvoir passer du temps avec Hapse, mais il semble plus occupée avec Ikomi qu'à s'occuper d'elle. Elle est censée être OK avec cette situation, et elle l'est, mais elle ne peut s'empêcher de penser que ces jours-ci ne sont qu'un anniversaire perpétuel.

L'amputation de l'Unknown Artist n'as pas été simple non plus. Elle se rappelle qu'il a passé énormément de temps à le bricoler, l'adapter pour survivre aux affres de la magnétosphère Jovienne et aux plasmas Ioniens. Il se rappelle aussi la beauté des aurorres boréales sur Jupiter. Il se rappelle aussi voir Neptune se lever depuis l'orbite de Thalassa, alors qu'il essayait de maintenir un avant-poste sur la lune en forme de disque, aux confins du système solaire. Il se rappelle les courses effrénés dans les anneaux de Saturne, essayant d'impressioner — encore un nom manquant aux souvenirs d'Ark — et avec qui elle espère coucher.

Tout ces souvenirs. Elle ne sait plus, si elle n'a jamais su, lesquels sont les siens, et lesquels elle a ingéré. Ça n'as jamais été vriament important pour elle, sinon elle n'aurait pas commencé et, de manière étrange, elle ne se souveint pas avoir commencé. Elle se souvient avoir toujours été comme ça. Ou avoir vécu une vie décousue, pleines de vide et de manque de continuité. Pleins de douleur et de déception Pleine d'expérience et de choses apprises aussi. De souvenirs uniques. Mais elle ne sait pas quels sont les siens.

Et elle ne sait pas si elle est en colère — ou triste peut-être —  à cause d'une de cesexpériences, ou si elle a de vraies bonnes raisons de l'être. C'est elle qui a du sacrifier sa maison, et les expériences associés qu'elle savait réelle. Mais maintenant, elle ne peut plsu faire la différence. Est-ce que l'Unknown Artist a réellement existé ou est-ce que ce n'est qu'un souvenir de quelqu'un d'autre ?

Et au final, peu importe, l'expérience de cette perte est rélle. La douleur est réelle. Et elle n'est pas d'accord avec le temps que Hapse passe avec Ikomi. Elle veut passer plus de temps avec lui. Et pas assise là, à se regarder tourner dans un fauteuil, un casque sur les oreilles, à probablement bricoler de la musique.

"T'as pas l'air d'aller bien ?" Se dit-elle à lle-même, arrêtant de tourner sur la chaise.

"Si si, ça va." Non, ça ne va pas et elle le sait. Pourquoi elle se pose la question ? "Enfin, à part que je suis là, assis dans ce fauteuil, et aussi assise en fce de moi. Mais probablement que je m'imagine moi même, me parlant à moi-même. Ou alors c'est encore les souvenirs de quelqu'un d'autre dans lesquels je suis un peu trop tombé. Un narcorythme plutôt balèse. Mais ça va, à part ça."

Ikomi observe Ark. Elle ne va manifestement pas bien, mais la façon qu'elle a de ne pas réussir à se distinguer d'Ikomi inquiète cette dernière. C'est déjà arrivé. Depuis le prenier jour où elles se sont rencontrées, Ark a déjà eu ce genre de problème, mais l'amplitude de cette crise est inédite, du moins pour Ikomi.

"Hapse, dit-elle dans un canal de com chiffré, je crois que Ark fait un genre de … d'overdose, ou quelque chose de ce genre ?"

"Il, ou elle, se prend encore pour toi ?"

"Non, tu sais bien que ce n'est pas ça. Je pense. Mais là c'est … Ça fait vingt minutes qu'Ark ne fait que déblatérer des choses à propos d'anniversaire, d'être trahi ou autre. Je ne suis pas certaine qu'il se soit rendu compte qu'il parle à voix haute. Et là il me parle comme si j'étais un genre d'ami imaginaire. Un truc du genre."

"Merde." Un silence. Hapse repose le filtre à air qu'il est en train de nettoyer. Ark ne fait pas souvent ce genre de choses, à peu près une fois par an. Pour son anniversaire. Hapse n'a jamais réussi à savoir si c'était un souvenir implanté ou quelque chose qui était réellement arrivé, ou si Ark s'était réécris des souvenirs pour oublier quelque chose.

"Sort de la pièce", reprend Hapse. Dans cet état, Ikomi ets probablement la pire personne pour aider Ark. "Et j'arrive."
