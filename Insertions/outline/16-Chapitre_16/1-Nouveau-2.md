title:          Nouveau_2
ID:             69
type:           md
compile:        2
charCount:      3622



"T'es venue finalement", dit Ark quand il voit arriver Hapse, une grande tristesse l'envahissant, mais le soulageant du sentiment d'abandon.

"Oui." Hapse s'est assis à côté d'Ark, sur l'une des dessertes du pont de commandement. "Est-ce que tu sais comment je m'appelle ?" Une quetsion importante pour savoir où en est Ark. Le système cérébral a du mal avec les références trop précises, et donc les noms de personne, de lieux, tombent des souvenirs ingérés. Et Hapse a besoin de savoir dans quel état de lucidité se trouve Ark.

"Ouais. Tu es Hapse. Je sais qui tu es. Je ne sais pas trop qui je suis, mais je sais ce que j'ai vécu. Et je sais aussi que je suis en colère. Contre toi je pense." Au moins Ark a encore un sens de la réalité et n'est pas parti trop loin. "Contre moi aussi. Ou la partie de moi qui se ballade dans le vaisseau quand je ne suis pas là."

"Tu sais que c'est l'inverse ? Que tu as probablement pris trop de chem-mem inspirée de sa vie. Probablement des souvenirs d'une fiction." Hapse évites avec soin de parler de la colère d'Ark, il n'as pas envie pour le moment de se confronter au fait que, oui, Ark a des raisons légitimes d'être en colère, et de demander un petit peu plus que l'absence d'attention que lui a accordé Hapse.

"Je sais. Quelque part, je le sais. Mais c'est pas simple, je perds mes repères. J'ai jamais été très bon à trouver des repères, mais ces derniers temps, il y avait le Unknown Artist. Et toi. J'ai perdu le premier suite à un accident. Et c'est pas juste mais c'est comme ça. Et j'ai l'impression qu'on se perd. Pas le souvenir que l'on se soit perdu de vue, ça c'est déjà arrivé. Et c'était pas avec toi. Mais avec machine. Bref. Pas un souveni, pas un vécu. Mais une sensation."

"Oui, désolé. J'ai … "

"Tu essayes de la sauver. Je sais. Un jour peut-être, tu pourra lui expliquer pourquoi tu tiens temps à sauver les gens. Moi je ne me souviens pas que tu m'ai expliqué. Mais peut-être que tu l'as fait et que je ne m'en souviens plus. Ou peut-être que je me souviens que tu l'ai fait, mais que je ne peux m'en rappeler."

"Tu veux savoir ?"

"Non. Mais est-ce que toi tu le sais ?" Un point pour Ark. Hapse n'as jamais vraiment voulu se confronter à ça. C'est plus facile d'aider les autres, même si les autres ne veulent pas être sauver. Ou que la situation dégénère. Au moins Hapse a une raison de s'en vouloir dans ces cas là, il a pas assez essayé, pas assez fort, pas assez longtemps, pas de la bonne façon. Bref, des raisons concrètes d'être en colère contre lui-même.

"Ça va aller tu sais ?" lui dit Ark. "Ça m'est déjà arrivé hein ? Et je suis toujours là. Ça va aller."

"Oui. Ça va aller. Et je suis là tu sais. Je ne vais nulle part. Et je suis désolé pour le Unknown Artist. Et pour ce que tu subit en ce moment. C'est ma faute."

"Non. Mon état émotionnel n'est pas ta faute. Ce qui est ta faute, c'est de ne pas t'être assuré de comment j'allais. D'avoir préféré passer du temps avec … Ikomi" — son prénom vient de loin, de sous les souvenirs algorythmiques d'Ark, quelque part dans une partie de son cerveau qui n'est pas en train de lui exploser la boîte cranienne — " qu'avec moi. De chercher à la réparer, alors qu'on est probablement autant pété tous les deux. Tu peux t'excuser de ça. Pas du fait que je soit en colère et triste et un peu déçu."

"Je …" commence Hapse. Qui n'as pas l'habitude de devoir faire face à un Ark aussi affirmé dans sa colère.

"Laisses moi maintenant. J'ai envie de profiter de ça un peu. De me faire, pour une fois, des souvenirs à moi. Je crois. On se reparle plus tard. Promis. Juste je veux être seul là."
