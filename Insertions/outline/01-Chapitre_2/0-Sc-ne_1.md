title:          Scène 1
ID:             2
type:           md
notes:          {W:27:WatUp Gravity Dog}
                {C:6:Hapse}
                {C:0:Ark}
compile:        2
charCount:      5359


Sous la poussée de ses 4 Arcjet nucléaires, brûlant ses stocks de deuterium, le vieillissant WatUp Gravity Dog accélère, laissant derrière lui les Grecs desquels il a extrait nombres d'épaves de colonies minières. Sa trajectoire l'amène vers les mondes peuplés de la ceinture d'astéroïdes mais, surtout, sur une tangente au Unknown Army, ce qui intéresse particulièrement Hapse. Ça va faire plusieurs mois que leurs Insertions orbitales successve se ratent et cela fait trop longtemps qu'Ark est resté seul à faire pousser ses splendides silicates dans le tore plasmique d'Io.

Ark a besoin de distance et apprécie sa solitude, mais même lui doit avoir des contacts réguliers avec quelqu'un une fois de temps en temps. Un peu plus d'une fois tous les six cent jours en tout cas. Même selon les standards de Love and Rage, ces périodes d'isolation sont mauvaises pour la santé mentale. Ark rondrait probablement qu'il ne correspond pas vraiment aux standards de Love and Rage, et qu'il a suffisament d'interactions sociales avec ses chem-mem, mais rien ne peut empêcher Hapse de tout de même se faire du souci pour lui.

Et puis Hapse a envie de le voir. De partager un peu d'espace, de respirer le même mélange gazeux, même si cela nécessite quelques heures de gym a basse pression pour s'acclimater à l'atmosphère un peu étrange qu'apprécie le solo lors de ses escapades jovienne. Il a envie de sentir la peau rugueuse et blindée de sodium d'Ark frotter contre la sienne. Il a envie de pouvoir enregistrer de nouveaux souvenirs rien que d'eux deux pour remplacer ceux qu'il a accumulé lors de sa dernières sortie, avant qu'il ne dégage d'Hector.

C'est le problème dans cette branche. On ouvre des habitats fantômes ou abandonnés pour en récupérer les composants récupérables. Extraire les données, désactiver les cœurs de réacteurs et le sembarquer une fois qu'ils ont refroidis. Une partie de tout ce matos est fourguable aux assurances qui payent relativement bien la récup, une partie sert à alimenter les différents spatioports en pièces de récup — Phobos Independent Space Station en particulier apprécie les cœurs de réacteurs et tout ce qui concerne la production d'énergie — et ls systèmes de com sont rassemblés, équipé d'un booster chimique et balancé sur une orbite vers les relais LNR.

Mais pas cette fois. Cette fois l'habitat n'avait pas été abandonné. Ça n;arrive aps souvent, mais une cascade d'erreur a tués ses occupants. Panne du système de décarbonation. Échec des hydroponiques. Vent solaires au mauvais moment. Micro météorite. Généralement un mélange de tout ça ou des nombreuses choses quipeuvent mal se passer. Théoriquement, les colonistes disposent d'un système d'évacuation. Théoriquement, ils sont aussi équipés de système multi-redondant parce qu'ils ne peuvent en général compter que sur eux-même pour les quelques dizaines d'années qu'ils vont passer sur ces cailloux.

Mais en pratique les clauses contractuelles de responsabilité et le fait de pouvoir être attqué en justice a souvent forcé les colons à prendre plus de risque que nécessaire. Et des années plus tard, après que les projets de colonisations ont été abandonnés dans le système Jovien, c'est Hapse et les quelques autres recycleurs qui passent récupérer ces ressources.

Et ramasser les corps cette fois-ci. Les logs de l'habitats ont été vidés, ce qui implique une visite précédente, ou un retrait de support de la corporation spatiale. Probablement les deux. Mais ce n'est pas ce à quoi Haspe s'était préparé en allant recycler cet habitat. Il a bâclé le travail, et à décampé au bout de trois jours, ne supportant plus de devoir se cotoyer aux cadavres flottant dans le vide atmoshpérique de ce qui devait être le dortoir. C'est le plus étrange au final, l'ensemble des colons étaient dans leur lit, dans le dortoir. Ce n'est pas censé arriver, il doit toujours y avoir une équipe active pour, justement, lancer l'alerte en cas de panne majeure. En plus cela permet de gérer les tensions qui finissent fatalement par se développer, en décalant les horaires de personnes ayant du mal à travailler ensemble. Du moins temporairement.

Hapse s'est donc remis en route après avoir harnaché autour du WatUp ce qui pouvait être récupéré facilement, vidant les soutes de leur atmosphère controllées afin de pouvoir bénéficier des systèmes d'arrimage de celle-ci ainsi que des répartiteurs de charge utile, comme si une gigantesque poche stomacale surgissait du ventre du Watup et avait fini par l'entourer complètement de débris en cours de digestion.

A l'opposé des moteurs et réservoirs, de l'autre côté de la soute ouverte, connecté au reste du vaisseau par un faisceau de superconducteurs, le module de vie / passerelle de commande tourne doucement autour d'un sas de connection et système d'instrumentations, et c'est là que Haspe va passer son temps. Dans ses quelques dizaines de mètres cubes d'espaces de vie, accroché sur un anneau concentrant le reste des systèmes de survie. Au centre de cet anneau, une baie d'amarrage à laquelle est connectée le Silly Boat, sa navette exploratoire marquée du poing levé noir sur un cœur stylisé mauve de Love and Rage.

Le fabeur de bord lui prépare une ration de nutriment qui aura la forme et le goût de ces donut qu'il apprécie particulièrement, ceux glacés au sucre rose alors qu'il démarre un message pour Ark.
