title:          Scène 2
ID:             4
type:           md
notes:          {C:0:Ark}
                {C:6:Hapse}
                {C:1:Echo}
compile:        2
charCount:      3713



"Hey toi. Vu ton transpondeur tu devrais être en train de quitter Jup quand tu recevras ce message. le WatUp est en route vers l'intérieur et je me disait qu'on pourrait s'injecter l'un avec l'autre au moins une partie du trajet.

Oh, et je pense qu'Echo a du t'envoyer un message. Si tu veux en parler je suis OK, mais je pense que sur ce coup Yas a raison, elle va finir par avoir vraiment un gros problème si … Stop".

Ark n'as pas besoin de cette intervention, se dit Hapse. La situation avec Yas et Echo est suffisament bordélique comme ça, pas besoin qu'il y ajoute son avis. D'autant que Echo est parfaitement capable de faire ses choix et de les assumer, même si ça veut dire prendre le risque de finir par le vide juste pour prouver qu'elle a raison.

"Effacer le message. Composer nouveau message."

"Hey Ark, juste un petit message pour te donner des nouvelles. Ton transpondeur est enfin sorti quelques instants du brouillard magnétique de Jupiter, tu es donc en train de rentrer vers Vesta".

Ark n'appréciera pas de se savoir surveillé comme ça.

"Et je me disait que ça serait cool de se voir un peu, j'ai un vecteur d'approche tangent au tiens, et je suis sûr que l'Unknown pourra bénéficier d'un peu plus de TWR, te connaissant tu as encore pris une trajectoire conservatrice et je pense qu'il y a des gens sur Vesta qui en ont marre de t'attendre."

Ouvrant sa combinaison pour laisser sa poitrine apparaître dans le champ, et faisant glisser cette seconde peau, s'exposant au champ de la caméra.

"Comme moi par exemple. Et je suis sûr que tu as envie de sentir tout ça autrement que par narcorythmes. Ou de me plaquer contre l'ablatif surchauffé de l'Unknown."

Refermant sa combinaison d'une main, laissant sa nuque libre et les cicatrices laissées par leur dernière rencontres visible.

"Ou tout ce que tu pourrais avoir envie de me faire. Et inversement. Et oui, j'ai suffisament de dv pour te rejoindre. Et suffisament de métabolisme régénératif pour baiser. Je te mets mes coordonnés orbitales, mais j'ai déjà des solutions.

"Oh, et ne t'avise pas de décliner Ark. Je suis sérieux. On peut juste se voir si tu préfère, mais il faut qu'on se voit. Pause."

Avec ça, Ark aura de quoi se faire un peu de chem-mem un peu plus sympa que son régime habituel. Hapse balance sur sa console un navigateur local et parcourt les XP qu'il enregistre régulièrement et remixe avant de les charger en narcorythmes pour ses propres besoins d'auto-actualisation. Il trouve ce qu'il cherche assez rapidement, sous un tag "Ark_porn". Des sessions de baise, parfois sous rétroaction parfois juste avec lui même et génère une boucle intéressante que Haspe joint au message.

"Reprise. Oh, et j'ai eu des news d'Echo. Prend une décision à son sujet, et essaye de lui dire un peu plus que « OK » pour une fois d'accord? Moi je soutiens ta décision quelle qu'elle soit. On s'en fout des autres. Allez, à bientôt. Pause. Fin d'enregistrement. Envoyer à Ark."

La console de communication émet un signal lumineux vert pour acquitter les commandes et l'envoi du message dans la porteuse de LNR, il faudra une dizaine d'heure pour qu'Ark reçoive le message et Haspe n'espère pas vraiment une réponse avant quelques jours, lorsqu'Ark aura terminé son insertion trans Jovienne et trouvé quoi répondre.

Ses meilleurs estimations donnent une interception dans trois mois, mais avec plus de 10 kdv de déccélération. Il aime bien Ark, mais cramer plusieurs mẽtres cubes de deuterium est un poil excessif pour un peu de baise. Ou même pour beaucoup de baise. Il faudra donc attendre quelque chose comme six mois pour ne pas cramer tout son carburant et toujours réaliser un petit profit. Mais c'est bien plus raisonnable.
