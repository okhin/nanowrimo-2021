title:          Scène 3
ID:             3
type:           md
notes:          {W:27:WatUp Gravity Dog}
                {W:5:4 Vesta}
                {C:6:Hapse}
                {W:25:Jam's}
compile:        2
charCount:      4811



Au bout de quelques minutes, les moteurs du WatUp s'arrêtent, le vecteur d'insertion vers 4 Vesta complété. Enfin libéré de la gravité de poussée, Haspe se aisse flotter à travers la passerelle de commande, juste au-dessus de ses quartiers, à peine séparé par une plaque métallique alvéolée, permettant de pouvoir garder un œil sur les systèmes de vol depuis n'importe quel endroit du module d'habitation.

"Bascule en mode espace profond. 0.1g"

"Bien reçu Hapse, content d'être de retour ?" Répond cette fois la console. La voix du WatUp est un poil trop élevée, dissimulant mal l'excitation du système psy de pouvoir parler. Il y a que les personne complètement cassées comme Ark qui peuvent faire l'économie d'une assistance psy pendant les vols de plusieurs mois. Mais Haspe préfère la garder inactive pendant les manœuvres, moments critiques et stressant s'il en est, et qui amène parfois le système à s'introduire dans son flot de pensée.

"Je ne sais pas. Mais on va essayer de voir Ark sur la route. Et on devrait aussi pouvoir profiter du prochain concert de Jam's, au moins en version laguée." Répondre et détourner la conversation. WatUp devrait comprendre qu'il n'a pas envie d'élaborer sur le sujet, du moins pour le moment.

Le retour à 4 Vesta n'est jamais facile, surtout pour les distants comme lui et Ark. Ou toutes celles qui vivent hors des puits en fait. Faire culture et société commune quand son voisin le plus proche est à plus de dix secondes de com-lag rend les choses parfois tendues lorsque l'on se regroupe. C'est souvent intense. Phsyiquement, émotionnellement, économiquement et socialement. Mais cela permet aussi d'apprécier la distance physique, et de créer une attraction vers un lieu distant de manière plus effiace qu'un puit gravitationnel.

C'est parcequ'ils peuvent quitter facilement l'influence orbitale et sociale de leurs congénères que les distants apprécient de revenir, orbitant autour de leurs proches sur des trajectoires fortement excentriques. Appréciant les instants de solitudes, mais voulant revenir dans la zone d'intimité des autres. Certains orbitants autour d'un corps massif, un objet culturel ou d'attention suffisament fort qu'il peut parfois, à l'instar des trous noirs, générer un horizon d'évènements, une zone à partir de laquelle il n'est plus possible de s'échapper, les distants finissants par s'agglomérer autour de ce point culturel jusqu'à ce que la radiance de celui-ci finisse par l'évaporer et laisse les autres repartir.

D'autres orbites les uns autour des autres, gravitant l'un vers l'autre dans des spirales d'énergie de plus en plus intense avant d'entrer en collision et de s'écarter les uns des autres à grande vitesse, avant de reprendre leurs lentes spirales les uns vers les autres.

D'autres enfin, défiant les lois de la physique sociale et sentimentales, louvoient entre les influences culturelles des différents groupes, s'approchant suffisament près pour s'intéresser aux autres avant d'entrer dans une autre zone de gravité dominante, suivant des trajctoires apparemment erratiques.

Perdu dans ses pensées, Haspe dérive doucement vers le blindage extérieur de l'habitat, entraîné par la force de Coriolis, à mesure que la vitesse angulaire du module augmente.

" … sera malheureusement annulé." Entend-il au moment où il reprend conscience de la conversation qu'il est en train d'avoir.

"Qu'est-ce qui sera annulé?" Demande-t-il, se mordant la lèvre car il sait que WatUp va noter mentalement son absence.

"Le concert des Jam's. D'après un communiqué de leur agent. Un incident impliquant Ikomi. Elle a percuté des paparazzis et s'est réfugiée dans un salon de coiffure pour se raser la tête à priori. Les réseaux pensent que c'est un script." La formulation utilisé par WatUp laisse à penser qu'il a une autre hypothèse en tête.

"Tu n'es pas d'accord avec ça ?"

"Non. Son contrat d'image défini assez précisément ce à quoi elle doit ressembler. Et je doute que sa nouvelle coupe soit validée par ses conseillers. pareil pour les dommages infligés au paparazzis. C'est plus que ce qui est générallement toléré dans les clauses scandales. Et … "

"Stop. Ok. Bon, ben pas de concert des Jam's je suppose".

"Bien. Et si tu as un peu de temps, je pense qu'il faut faire la maintenance préventive des hydroponiques."

C'est la façon de WatUp de bouder, plus ou moins. Du moins de montrer sa frustration quand il est trop souvent interrompus dans les discussions qui l'intéresse. Mais WatUp est un peu trop interessé par Ikomi. Il ne peut pas poster sur les forums, mais Haspe sait que si c'était le cas, il passerait beaucoup trop de temps sur le tag LeaveIkomiAlone.

Mais WatUp a raison, il faut s'occuper des hrydroponique. Et ça permettra aussi d'avoir quelque chose à manger quine soit pas juste un donut imprimé.