title:          Nouveau
ID:             90
type:           md
compile:        2
charCount:      9646


"Ça va ?" Echo n'entend pas vraiment la question. Elle a réussi à s'isoler quelques minutes pour voir son père, et ils sont assis face à face dans les fauteuils sphériques du Tao Bao. Les derniers jours ont été éprouvants avec l'installation d'Ikomi, la préparation du concert, les incessantes questions paranoïaque d'Abdul Xe, les demandes d'interview de groupe média dans lesquels, elle suppose, que Van Xetopical ou quelque soit son nom ait une participation, et qu'elle passe ses journées à décliner, les demandes d'interviews de casteurs indépendants, plus ou moins affiliés à la nébuleuse de collectifs de Love And Rage, et qu'elle passe ses nuits à décliner, toute la merde autour de ce concert pour lequel tout le monde semble penser qu'il y a un plan d'action, une planification, quelque chose d'autre que Ikomi qui veut répondre qux milliards de questions qui lui sont qadressées, et s'éclater sur scène. Tout à l'heure. Dans moins de trois heures. Dans son vaisseau. Le dernier jardin de Shi. Et elle se sent un tout petit peu dépossédée de la situation. Alors répondre à la question ça va, n'est pas vraiment non plus sa priorité.

"Ça ira. Il faut bien." Répond-elle. Elle pourrais demander comment va Ark, mais il lui fera une réponse aussi laconique qu'elle. Ils ne se voient pas assez pour avoir réellement déveoppé un lien de filliation, ou d'amitié. Et, de toutes façon, ils préfèrent leur silences.

"Dis moi", demande Echo, une question lui brûlant soudainement les lèvres, "Pourquoi tu t'es mis à faire ces trajets ? À prendre temps de recul par rapport aux autres ?".

Ark enfourne une boulette de mycellium parfumée au sésame dans sa bouche, pour se donner le temps de réfléchir aux sens cachés des questions de sa fille.

"Parce que je n'ai jamais vraiment compris comment les choses fonctionnent ici. Ni l'importance que ces choses", et Ark, d'un geste du bras, couvre l'espace autour d'eux, désignant ces choses — les personnes attablées autour d'eux à discuter, les techniciens qui permettent au mastodonte HTT de fonctionner, les touristes, les fans d'Ikomi — dont il parle, "peuvent revêtir pour moi. Du coup je voulais aller loin de l'agitation. Et une fois parti, que je ne pouvais plus vraiment revenir, j'ai compris à quel point ça pouvait me manquer ou m'être nécessaire."

"Rien à voir avec Yas donc. Ou moi."

"Pourquoi ça aurais quelque chose à voir avec toi ?"

"Je sais pas. Je me suis toujours demandée pourquoi tu avais un gosse en fait. Pas que je t'en veuille. Je suis curieuse, on se voit quoi tous les cinq ans, au mieux, tu as prêté tes gênes à Yas et à Pierre et tu ne cherches pas plus que ça à me voir." Étrange que ces quetsions viennent maintenant, mais Echo a passé beaucoup de temps à se poser tout un tas de questions beaucou plus bizarre et qui lui causent beaucoup plus d'angoisse que celle-ci, alors elle peut bien poser ces questions là, et emettre à plus tard les questions telles que 'est-ce moi que l'on aime, ou est-ce l'image que je projette ?' ou 'Pourquoi est-ce que j'ai toujours autant de mal à laisser partir Shi ?' ou bien encore 'est-ce que ce concert n'est pas la plus grosse connerie jamais faites ?'

"Pour rendre service à la base. Yas est une bonne copine, que je croisait réguièrement dans mes orbites diverses. D'avant que j'aille miner des sillicates ioniens, et que je faisait surtout du fret pour prendre le relais des corpos qui se désengageaient. Et elle m'a demandé ce service. Une histoire de gènétique en paesanteur, de mutations, de choses de ce genre. Le deal c'est que je ne pouvais pas être présent dans ta vie quotidienne. Plutôt que je ne le voulais pas. Pour tout un tas de raisons, plus ou moins fumeuse. Mais Yas et Pierre ont finit par me convaincre, je leur ai filé une séquence d'ADN et quelques années plus tard je te voyais pour la première fois."

De nouveau le silence. Il a répondu aux questions, du mieux qu'il pouvait. Avec ce qu'il pense etre une forme de franchise. La forme de franchise qui peut parfois mettre les gens mal à l'aise par son absence de subtilité ou de tact, mais ça convient à Echo, elle n'est pas vraiment à la recherche de quelqu'un qui la brosse dans le sens du poil.

Elle termine son plat d'excellente boulettes, avant de se détacher et de flotter en direction de la sortie.

"Tu restes un peu, demande-t-elle ?"

"Ouais. Faut que je retape ma carcasse."

"Laquelle ?" demande Echo, faisant allusion à la fois à l'Unknown Artist et au mélanomes qui tachète maintenant la peau d'Ark.

"Les deux je suppose. Donc ouais, je suis cloué au sol un peu de temps. Va falloir que je m'habitue à ça", dit-il, souriant un peu.

"Je suppose. Passe au concert. Ça sera bien. Et je pourrais te présenter au pod. Et revoir Ikomi."

"Je l'ai beaucoup vu tu sais. Ça va, mais ça n'as pas été simple tous les jours."

"Elle m'a dit ouais. Elle m'a dit de te dire merci aussi. Allez, faut que je trace, on a encore tellement de trucs à faire."

---

Le Xenon Girl — puisque c'est qinsi que tout le monde appelle ce petit module de proue qui peut à peine héberger dix personnes — est occupé par l'équipage du Dernier Jardin de Shi au complet, MT-33 qui est là pour faire la liaison technique avec le Heavier Than Thou, et quelques habitués.

Le plus gros du travail est fait, le vaisseau étant connecté par une ombilique au Heavier Than Thou, qui servira de plus grosse salle de concert et de point de diffusion. Des courriers corpos ont été identifié en approche dans les prochaines heures, et mis sur des orbites d'attente, l'idée générale étant qu'ils pourront bien attendre quelques heures. Ou jours. Ou peut-être semaine. Bref, personne ne leur prête vraiment attention.

"Finalement, on a pas eu des nouvelles des Sigma, "dit Echo à une Pak qui essaye de trouver un arrangement de ses boissons piratées satisfaisant.

"T'aurais voulu ?"

"Je sais pas. Ça me donnerait l'impression qu'au moins c'était quelque chose d'importants pour eux, non ?"

"Bah, je viens biens sans des mercenaires pour défoncer notre espace de vie tu sais. Et à priori, ils ont tellement de merde à gérer chez eux que, peut-être, ils ne peuvent plus se permettre d'essayer d'empêcher le concert."

"Ouais. Possible." Echo est un peu déçue. Mais au moins elle gardera son vaisseau en un seul morceau. Et un équipage en un seul morceau aussi.

"Dis, tu as prévu quoi après ?" demande-t-ell à Pak.

"Après, genre … comme after ? J'avais prévu de danser. Peut-être avec Kam. Viens qui veux."

"Non, je pensais plsu à après. Genre OK, on a fait ça, et maintenant, on fais quoi?"

"Oh ça ? Je réflhit pas jusque là moi. J'essaye de faie en sorte que vous, les gens un peu fous  — comme toi ou Ikomi — qui ont des idées de changer les choses aient l'énergie émotionnelle de le faire. Mais sorti de là, moi je comptais rester dans mon bar. Qui ira probablement là oú vous décidez d'aller. Du moment que je peux passer une semaine de temps en temps sur un habitat plus grand, ça me va."

Encore un vote pour la proposition "on te fais confiance, on va oú tu veux". Bon sauf Peg. Qui va là oú Ikomi va et Echo n'as pas envie de la faire choisir. Et Ikomi ne sait pas trop ce qu'elles veulent faire. Sauf qu'Echo a juste envie de pas prendre cette décision. Mais le consensus risque de s'établir autour de faire du lien dans la ceinture. Faire une tournée de Fuck my Lives au moins.

---

Plusieurs milliers de connectés. Probablement des centaines de fois plus avec le lag. Le concert sera terminé depuis longtemps lorsque le signal électromagnétique atteindra la magnétoshpère Jovienne. Ou les fabriques à macro-processeurs des troyens. Ou la sphère temps réel des puits. Mais peu importe. Ils seront live dans quelques minutes. Ou secondes. Et peut-être que rien ne changera, mais au moins Ikomi aura dit ce qu'elle a à dire. Et Echo a décidé que s'il ne se passait rien de plus que ça, ce serait pas mal.

Elle sait que Kam est avec Pak à régler les derniers détails techniques. Elle préfèrerait qu'elle soit là, mais Echo sait qu'elle est insupportable dans ces moments de pression. Pourtant Peg est là elle, dans ses bras, et ceux d'Ikomi. Toutes les trois ensemble. Un peu étrange vu l'admiration qu'Echo porte à Ikomi et sa musique — elle essaye très très fort de ne pas être une fan, mais ce n'est pas facile tout le temps. Et le chevalier blanc d'Ikomi n'aide pas spécialement.

Peg l'embrasse et la caresse d'une main, tandis que l'autre joue avec les sangles néons que porte Ikomi en guise de costume de scène. Juste un baudrier et un harnais. À poil donc. Mais c'est elle qui l'a voulu, c;est son choix et, comme elle le dit "de toutes façon, tout ce que j'avais à cacher a été placardé sur les façades d'immeubles pour vendre je ne sais quelle boisson alcoolisée, je peux bien ne pas m'habiller comme je veux".

"Allez, dit Ikomi, c'est parti. Je vous retrouve après le concert, Pak a dit qu'elle voulait danser, vous y allez pas sans moi."

Ikomi franchit le sas qui l'amène dans le Xenon Girl, maintenant vide, ou presque. Le public est à peine à deux cent mètres. Dans la salle, juste les techniciens. Et Peg qui vient de se sangler juste en face, Echo glissant doucement vers la verrière de proue. Un concert range point de vu sensation pour Ikomi, mais elle a des retours du public réuni dans le HTT, projetté sur les cloisons autour d'elle. Et ce n'est pas pour eux qu'elle fait ça. Pas que.

Le beat un peu lent de Song for Peg inonde le bar et elle commence à danser. Elle a décidé que les discours ce serait plus tard. Là elle veut juste chanter. Danser. Pour les gens qui l'aime, ceux qu'elle aime, et le public. Une inspiration, et c'est parti.