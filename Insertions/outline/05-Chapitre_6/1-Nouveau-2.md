title:          Nouveau_2
ID:             30
type:           md
compile:        2
charCount:      5747



Deux Hydrazine™ plus tard, et quelques câlins, Kam est un peu revenue à la vie. La musique n'est presque plus complètement désagréable. Ou alors c'est la quantité d'oxyde d'azote dans le sang. Ou la présence d'Echo tout autour d'elle.

"Tu sais, commence-t-elle, il m'arrive de perdre de vue parfois pourquoi je subit tout ça. Pourquoi je me lève, pourquoi je vais caler mon cul dans ces harnais absolument incomfortable et guider les orbiteurs, capitaines et pilotes au micro pendant des heures. Pour à peine suffisament d'ek pour les dépenser en pate nutritive infecte et payer un cerceuil dans les hôtels-grav.

"Vraiment. Je veux dire, faudrait être folle pour faire ça non? N'importe qui d'autre aurais au moins essayé de, je sais pas, changer les choses non?"

Echo ne répond pas. Pas qu'elle n'ai rien à dire, mais ce n'est pas le moment. Ce dont à besoin Kam c'est d'être écoutée, alors elle écoute, en caressant le crâne rasé de Kam, signe de son opposition aux normes culturelles qu'essayent de passer FDM.

"Mais quand je suis là-bas, au cœur de KPI, avec tout ces vaisseaux qui naviguent, qui, un peu perdus dansleur vision des choses, cherchent juste une solution de navigation qui leur permettra de venir enfin sortir de leurs postes depilotages, de livrer les ressources absolument nécessaires au fonctionnement de la station, de nous débarasser des différents déchets, je ne vois qu'un magnifique objet.

"Et moi, je suis là, et j'envoie ce cargo de touristes sur une orbite particulièrement eccentrée pour les retarder le plus possible et taxer leur système de survie le plus possible. Je fais faire des combustions absolument inefficaces aux cargos qui amène tout ces trucs donc ces connards ont besoin pour se sentir bien. J'oublie d'enregistrer ces navettes individuelles qui ont accosté il y a quelqus heures.

"Non, je ne veux pas savoir ce qu'il y à dedans. Mais je sais reconnaître un transpondeur contrefait quand j'en vois un."

Echo se tend un peu. Pas que ce que Kam fasse soit la mauvaise chose, mais elle va finir par se faire prendre.

"Alors peut-être que je ne fais pas partie d'un groupe organisé. Peut-être que je ne peux pas foutre mon cul dans une combinaison dure, parce que l'imbécile qui les a concues n'a jamais pris en compte les morpologie trop écartées d'une norme gravitationnelle. Et peut-être que, en vrai, l'dée d'être là-bas dehors me terrorise et que je ne pourrais jamais partir d'ici.

"Mais je ne suis pas un traître. Et je ne me laisse pas faire. Et je sais que je ne peux pas le dire à tout le monde, alors je te le dis à toi. Parce que j'ai besoin de le partager avec quelqu'un."

Les larmes de Kam glisse sur son visage, entraînée par la force de coriolis. Echo devrait dire quelque chose, mais ellene trouve rien à dire, alors elle laisse Kam pleurer dans ses bras quelques minutes, la serrant fort dans ses bras, avant de l'embrasser.

"Tout va bien les filles ?" leur demande un uniforme s'asseyant à leur table. Il pose sur la table un enregistreur et une carte.

"Cartier Van Otaxatl, ajoute-t-il de la voix de quelqu'un qui est à la fois très sûr de lui mais éalement habitué aux dissonances culturelles, PanAm Incorporated, branche de Cérès"

"'Ecoute, on passe une soirée tranquille entre nous. Et tout va bien, jusque là." Répond Echo, interrompant ce qui ne saurait être qu'une litanie de titre pompeux et ronflant censé donner à cette vitrine de l'eugénisme une quelconque importance.

"Et vous passez une soirée tranquille dans un bar à grav, connu pour être un repaire d'agent, de collabos et d'aures. Et tout va bien, mais ta copine pleure." Continue-t-il, toujours plein de son insupportable aplomb.

"Oui. Et donc?"

"Rien. Je ne faisait que pointer des contradictions. Déformation professionnelle je suppose."

"Oh, tu es un de ces journalistes en safari hors-sol c'est ça? Tu viens voir c'est quoi la vie chez les sauvages? Tu penses qu'on va, je sais pas, te faire visiter nos piaules Sérieuesement, ça fonctionne?"

"Si je veux visiter ta piaule, il me suffit de dérouter trois gros bras, ceux de Sigma tant qu'à faire. Idéalement quand ta copine y sera seule. Et tu sais ce qu'il se passera. Ces mecs de Sigma sont vraiment des connards, t'imagine pas la paperasse qu'il me faudrait remplir.

"Maintenant que j'ai établi que je suis bien le connard auquel tu pense avoir à faire, comprend bien une chose. On peut avoir cette discussion ici, tranquillement en sirotant ces trucs insipides que vous semblez adorer ici. Ou alors tu peux perdre les personnes auxquel tu tiens."

Pas de sortie possible. Echo a bien une combi, Kam aussi, mais trop de monde n'en a pas ici. Et même si elle pouvait, d'une manière ou d'une autre, réussir à percer les dizaines de centimètres de cloisons qui les séparent du vide, elles se retrouveraient sans vraiment un système de propulsion, trop loin de ce qui pourrait ressembler à un transport hors-d'ici.

"D'accord, laisse échapper Echo entre ses dents, se concentrant sur Kam dans ses bras qui essaye de ne pas trop fixer Cartier Von Otaxatl, tu veux quoi?"

"J'ai, disons, intercepter un message. Pendant que ta copine là est trop occupée à maximiser les coûts de cette opération pour FDM, moi je fais mon travail. Bref. J'ai ce message qui t'es destiné. J'aimerai que tu m'explique ce que tout cela veut dire et j'ai une proposition à te faire ensuite. Je veux juste que tu écoute cette proposition, je n'ai pas besoin d'une réponse, et on ne se reparlera plus. Et les mecs de Sigma te foutrons la paix après ça."

Puis, beaucoup trop content de lui, il ajoute.

" Tu as raison sur un truc. Je suis bien un journaliste, et je suis bien en safari. Mais vous n'êtes pas le gibier. Je cherche quelque chose de plus gros. Allez, écoute."
