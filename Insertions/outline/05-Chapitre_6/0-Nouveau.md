title:          Nouveau
ID:             29
type:           md
compile:        2
charCount:      3226


Le Xenon Girl a perdu de son intérêt depuis que les agents du Fond Culturel Martien y ont pris leurs quartiers pour, soi disant, établir les responsabilités dans l'accident du Hit Me et du détournement de propriété intellectuelle ayant causé des domages à l'image de Shiitake. Du moins c'est ce que les agents agglutiné dans les différents compartiments en rotation autour de KBI disent.

Mais Echo continue de venir régulièrement au Xenon Girl. C'est le lieu de détente le plus proche du centre de dispatch et même les agents du FCM ont besoin de se détendre de temps en temps. Même si l'Hydrazine™ coûte quelques watts de plus, vu que Park Lee est obligée de payer la licence. Et que la musique est devenue la soupe qui fait danser les aristocrates martiens.

Kam aurais du finir sa garde il y a déjà quelques heures quand elle glisse à travers le sas pour atteindre les parois externes. Elle atteint le sol trop vite. La gravité a encore changé légèrment. Officiellement, c'est parce que le système a besoin de maintenance. Officieusement c'est parce que les opérateurs de la station font tout pour pourrir la vie des accrocs à la gravité et changent régulièrement la vitesse de rotation.

Elle se pose à la table qu'elle prend habituellement avec Echo et Shi, quand celle-ci a un le temps de venir ici et Echo lui glisse une canette d'Hydrazine™ dans la main, avant de poser la tête de Kam sur son épaule.

"Dure ronde?"

"Ça irait à peu près si ces connards acceptaient de réinstantier les brigades volontaires au lieu de procéder à leurs réquisitions de personnels. On ne pourra pas continuer longtemps à devoir parquer leurs méga-cargos en orbite à juste quatre. Ou alors on va tous finir par s'accoutumer aux stims et on finira par s'endormir. Et personne ne veut que quatre ving kilo tonnes d'Hydrogène liquide finissent par percuter le cœur de la station.

"Sans même parler du blocus organisé, despénuries débile ou du fait que personne ne peut entrer ou sortir de la station sans signer un de leur putains de NDA. Mais ouais, sinon, j'ai eu une journée au poil."

Du pod, c'est probablement Kam qui vit le moins bien ce débarquement de la FDM. Shi passe son temps aux hydroponique en zéro-G, probablement à fomenter un quelqconaue plan d'évacuation et Echo est, certes, coincée dans la station, mais elle peut circuler librement, profiter de la zéro-G et de ses quartiers, alors que Kam enchaîne les shifts le sunes après les autres, faisant le taff nécessaire et vital et, dans le même temps se fait traiter de traître par à peu près toutes les personnes qui habite les secteurs zéro-G.

Mais la grêve n'a pas marché. Ça a fonctionné un temps, mais il a fallu gérer la mise en orbite de deux super-tanker-lourd en provenance de la surface de 1-Cérès. Et il a fallu reprendre le boulot ou laisser KPI se faire vaporiser.

Et Kam nepeut s'empecher de penser que tout est, en partie, de sa faute. Parce qu'elle n'a pas réussi à éviter que ces connards de la horde n'entrent en collision avec l'objet de leur traque, dont les scripts ont finit par la mettre, littéralement, hors des sphères d'influences de ses maîtres des puits qui, depuis, exerce leur vengeance sur tout ce qui prétend exister indépendamment d'eux.
