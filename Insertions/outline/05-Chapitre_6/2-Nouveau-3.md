title:          Nouveau_3
ID:             31
type:           md
compile:        2
charCount:      3815



"Alors ?" demande Cartier.

"Ben que veux-tu que je te dise? Ouais j'ai plus de sponsor, ouais j'ai demandé à mon parent un coup de main, et cet andouille est probablement trop défoncé sur la vie des autres pour être capable de me répondre directment, donc son mec fait messager."

Cartier marque une pause, son regard perdu dans les entoptiques qui se reflête sur ses cornées.

"Est-ce qu'il serait possible que ce … Hapse … et ton parent soit en train de tenter une opération de sauvetage du Hit Me?"

"Attendez, interviens Kam, sortant de sa semi-léthargie, vous voulez-dire qu'il y a encore des personnes vivantes dans le Hit Me?"

"Hypothèses. Juste des hypothèses. Mais est-ce que ce serait possible?"

"Sans connaître les détails des stocks de carburants ou autre, c'est de la conjecture, de pures estimations. Mais la mécanique orbitale est viable, le coût en dv n'est pas si abberant, et il y a des solutions pour un retour en rétrograde, mais probablement plus sur 4-V que pour 1-C."  Kam est déjà en train de calculer de tête les corrections de trajectoires nécessaire. Déconnectant temporairement de la situation autour d'elle.

"Bien. C'est donc techniquement possible. Reste la partie humaine. Est-ce que Hapse et ton parent dépenserait les dv nécessaires pour l'interception et le sauvetage."

"Tu passes pas beaucoup de temps en espace profond toi hein, trop d'orbite basses." lui dit Echo. "C'est pas une quetsion qui se pose. La probabilité de pouvoir se trouver à portée de quelqu'un en situation de détresse quand on navigue dans le vide spatial son tellement faible que personne n'imaginerais ne pas répondre à un SOS si ils ont la possibilité d'intervenir.

"Mais je suppose que ça neparle pas forcément à quelqu'un qui ne quitte jamais les Puits. C'est chacun pour soi là-bas, c'est ça ? On regarde la faisabilité économique avant la faisabilité technique ?"

"J'adorerais discuter avec vous des problématiques et du coût économique de maintenir ce cirque en orbite. Mais malheureusement, mes maîtres corporatistes vont vouloir mon rapport avant que nous n'ayons pu établir que votre propagande vaut la mienne et que oui, tout est question de domination économique.

"Cela étant, ma proposition. Et vous en faites ce que vous voulez. PanAm ne veut pas que Shiitake s'en sorte. Donc Ikomi ne doit pas refaire surface. Vous la mangez, vous lui offrez l'asile, vous l'expédiez dans sa limo sur Pluton, je m'en fout. Shiitake sera considéré comme responsable de la mort de leur principale artiste, toi tu auras ta nouvelle fronde, et FDM dégagera d'ici. Tout le monde y gagne."

"Sauf Ikomi."

"Sauf Ikomi, oui. Mais à ce stade je doute qu'elle ai encore unepersonnalité cohérente. Tu fais ce que tu veux, tu as les billes pour décider, et je ne vais pas te dire quoi faire. je ne te ferais même pas signer de NDA, je sais bien que ce serit lameilleur façon de tout faire foirer. Tu peux rentrer chez toi avec tes … copines, ajoute-t-il une forme de dégout dans la voix, son conservatisme finissant malgrè tout pararriver à la surface.

"Et comme promis, tu n'entendra plus parler de moi. Oh, et au fait, Kam, cest ça ? Merci de m'avoir permis de passer sous les radars. Je n'ai jamais été là. Bonne soirée."

Il remballe son enregistreur, laisse bien évidemment sa carte sur la table et repart, quittant le Xenon Girl, deux cerbères à la musculature développées par des années d;entrainement en grabité terrestre lui emboitant le pas.

"Tu vas faire quoi? Demande Kam."

"J'en ai aucune idée. J'ai besoin de me changer les idées et d'y réfléchir. Quoi qu'il se passe, on a quoi … six mois pour décider quoi faire ?"

"On va chez toi alors?" demande Kam, finissant son Hydrazine™.

"D'accord." répond Echo, sentant la main de Kam remonter le long de ses cuisses, sous les sangles de son baudrier.