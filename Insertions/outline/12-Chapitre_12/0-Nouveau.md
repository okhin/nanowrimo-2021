title:          Nouveau
ID:             53
type:           md
compile:        2
charCount:      9068


"Tu as le choix. Tu travailles à régler tes problèmes et tu peux rester ici, ou alors je te mets dehors. J'en peux plus de te voir te morfondre, de t'entendre te plaindre et de ne rien faire à ce sujet. Et que l'on soit bien claires, par tes problèmes, je parle de ta relation avec Kam. Tu la casse, tu la relance, comme tu veux. Mais c'est pas avec moi que tu doit en parler, c'est avec elle. T'as une semaine avant que je te jette dehors." Plus ou moins ce que Beth a dit à Echo il y a une semaine, alors qu'elle était encore rentrée défoncée. Et elle est maintenant là au Previously Known as the Xenon Girl — ou Pikaxedji — à attendre que Kam se pointe.

Deux semaines d'abus, commis principalement seule, tout le monde étant occupé à établir des clusters à partir des restes de KIR. Et plus personne n'ayant réellement envie de s'éclater. Parfois elle n'était pas seule. Et c'était presque pire. Les slasheurs ne sont pas les personnes les plus intéressantes qvec qui traîner, obnubilés par leur  gloire passée et leur gloire future, oubliant de devoir y travailler. Mais au moins certains sont de bons coups. Elle croit. Beaucoup trop de chimie a été impliquée ces derniers temps.

Elle est donc sobre, pour la première fois depuis trop longtemps, quand Kam s'installe en face d'elle. Le crâne rasé de près, en soutien aux insurgés qui mènent la vie dure aux agents des corporations des puits depuis quelques jours. Elle a le straits tirés par la faigue, mais quelque chosedans son regard s'est éclairé quand elle s'est assise. Elle pose sa bouteille de NotHydrazine — de l'Hydrazine piratée — sur la table, et, doucement, sa main sur celle d'Echo, qui après une hésitation, décide de ne pas la retirer.

Echo n'arrive pas à soutenir le regard intense de Kam. Elles se sont perdues quelque part quand Shi est morte. Ou que KRI s'est disloquée. Mais la chair de poule sur sa peau et les frissons qui la parcourt au simple contact physique avec Kam lui montre qu'elle a envie d'être avec elle. Harnachée contre une cloison, se tenant la mian dans un couloir, à discuter des heures par message textes, se racontat leurs galères, de se soutenir l'une l'autre.

Elle sait ce qu'elle a probablement perdu. Une boule se forme dans sa gorde à cette pensée. Une boule qui bloque ses larmes. Une boule qui disparaitra au moment où elle ouvrira la bouche pour parler, libérant la peur, la panique, le sentiment d'avoir été abandonnée, les larmes et, donc, l'empêchant de parler. Alors elle garde sa boule dans la gorge et, à défaut de réussir à parler, elle serre la main de Kam. Fort. Ses phalanges blanchissent et elle sent son bras qui tremble. Les épaules qui hoquettent et les larmes qui coulent.

Kam attends. Elle n'a rien à dire qui ne puisse aider maintenant. Et elle sait suffisament bien qu'Echo n'st pas une personne dont la communication passe par les mots. Donc elle attends, elle voit la tristesse et le deuil menacer de submerger une des personnes qu'elle aime, alors elle fait la bouée de détresse. Même si cette personne lui as fait du mal, même si elle a été particulièrement blessée par son comportement. Même si elle ne sait pas si elle veut reprendre cette relation. Echo lui manque, oui. Mais elle lui a fait mal comme seule les personnes proches savent et peuvent le faire.

Echo se redresse doucement, un sourire triste sur les lèvres et elle se penche en avant pour embrasser Kam. Et Kam décide de faire l'autre moitié du chemin. Les lèvres de la pilote lui ont manqué, en particulier ses rouges à lèvres parfumés. Mais elle ne trouve que le sel des larmes et les vapeurs de dégraissant. Mais Kam s'en fout. Elle veut ces lèvres, ces yeux, cette pilote. Elle veut la retrouver, ici ou, plutôt, quelque part dans un module d'habitats ou elles ne seront que toutes les deux. Sans Shi. Cette pensée la perturbe.

ELle glisse sur le côté de la banquette pour prendre Echo dans ses bras, pour pouvoir lui parler doucement à l'oreille. Pas besoin de témoins.

"Je sais qu'on veut baiser toutes les deux. Mais il faut qu'on parle. Avant. Pas pendant, pas après."

"Tu préfères ?"

"Non. Non je préfères pas, clairement, je préfères quand on baise, parce que c'est bon, c'est agréable et c'est cool. Mais on étais trois. Et … Et ça sera pas pareil. Ou peut-être que si. Et tu as pas répondu à mes appels pendant quoi …"

"Deux semaines. Au moins. Je sais, je suis désolée."

"Laisse moi finir, d'accord ?" Le ton monte mais Kam tiens toujours Echo dans ses bras. "Tu m'as pas appellée, tu m'as ignorée. Et je peux comprendre que tu sois en colère, mais moi j'aurais eu besoin de te parler aussi. Parce que je sais pas ce que tu as cru, mais merde, moi aussi ça me fait super mal qu'elle ne soit plus là." Une pause. Des sanglots. Quelques seconde sou quelques minutes, elles ne savent plus vraiment. "Mais voilà. On gère comme on peut, mais on est censée gérer ensemble. Je m'en fout que tu t'envoies en l'air que t'avales des litres de je ne sait quel euphorisant. Juste, tu m'ignores pas, tu m'abandonnes pas. Ou alors au moins tu me le dis."

Encore des secondes-minutes. "C'est bon, tu peux parler, je crois que j'ai dit le plus gros." D'autres seonces-minutes. Echo cherche ses mots. Pas pour ne pas blesser Kam, mais pour trouver les mots reflétant son état d'esprit.

"Je crois que … Pardon, non, je suis sûre que je t'en ai voulu, ou peut-être que je t'en veux encore, parce que je ne peux pas en vouloir à Shi. je ne peux pas lui dire à quelle point sa décision était stupide. Je ne peux pas lui dire que je l'aime. Je ne peux plus faire ça. Et je lui en veut, mais c'est pas juste d'en vouloir aux morts, alors j'ai du transférer ça sur toi. C'est nul, mais c'est comme ça.

"Il va me falloir du temps pour passer outre. Mais je sais aussi que j'ai envie de passer outre, j'ai envie qu'on retrouve un équilibre toi et moi. Et les autres, mais d'abord toi et moi. Parce que t'es quelqu'un de bien, qui a pris une décision, celle qui te paraissait la bonne, celle qui était peut-être la bonne, alors que moi j'étais tétanisée et que j'ai été merdique vis à vis de ça.

"Et j'ai envie qu'on passe du temps ensemble. Mais il faut que je parte d'ici. Je ne peux pas rester ici, voir les choses se faire ici, ou prétendre que des choses se font ici alors que toutle monde est en train de se barrer, et voir tous les jours la tombe de Shi flotter sous les hublots, setir son odeur dans notre compartiment quand je me lève, imaginer sa silhouette acoudée au bar. Trop de fantômes, il faut que je parte.

"Et je veux être avec toi, et je ne veux pas te forcer à partir. Tu as une vie ici, des combats, des amies. Moi j'ai toi. Mais c'est à peu près tout. Je serait plus jamais une pilote, c'est mort, les agences de coopérations ferment les lignes de crédits eou en retourne à des accords sur le copyright. Je veux être avec toi, mais je ne veux pas que tu soit toute ma vie."

Kam réfléchit. Elle prend un peu de vitesse physique avec Echo et la dévisage, elle regarde ces yeux noirs, ses tâches de rousseurs un peu planquée sous la graisse. Les petites cicatrices de coupures sur le nez et les pommettes. Elle jauge Echo, elle essaye d'évaluer dans quel état elle est.

"D'abord, tu peux me demander. Je peux faire mes propres choix tu sais ?"

"Je sais. Je merde partout en ce moment."

"Non. Pas partout. Il y a quelques cœur de clusters qui ont définitivement ta touche, et qui fonctionnent bien mieux, en dépit du traitement sauvage auquel ils ont étés soumis. Mais bref. Oui, j'ai une vie ici, comme tu dis. Mais je veux être avec toi. Et je veux que tu sois bien. Et si pour ça, tu dois partir, alors il faut que j'y réfléchisse un peu."

"C'est non alors ?"

"Non. C'est laisse moi du temps d'accord ? C'est beaucoup, c'est important, je ne veux pas prendre cette décision maintenant. Mais je te promets de prendre une décision dans les prochains jours, et que tu soit la première prévenue quand je l'aurais prise. Je peux pas te promettre mieux. Ça te va ?"

"Est-ce que j'ai le choix ?"

"Oui. Tu peux vouloir partir maintenant. je le vivrais pas forcément bien et toi non plus, mais on finira par s'y habituer. Qui sait, on pourrait se croiser dans vingt ans au hasard d'un café et ne même plsu être en colère l'une contre l'autre. Mais tu as mes conditions."

Elle passe sa main sur la joue d'Echo avant d'ajouter. "Bon. On a fait le tour ?"

"Je pense oui."

Kam attrape Echo par la main, la tire vers elle et l'embrasse avant de la trainer précipitemment derrière-elle, de sortir du Pikaxedji et de l'emmener dans la petite navette de récup qui lui sert en ce moment à faire la liason entre les différents groupes d'habitats semi autonome. Une fois la porte ouverte, Echo — qui a profité du trajet pour se défaire du haut de sa combinaison — bouscule Kam, déclenche la fermeture du sas en passant, se place à califourchon sur Kam et entreprend de détacher les attaches pas assez rapide de la combinaison noire et rouge maintenant plaquée au sol, alors que les mains aux doigts ronds de Kam glissent sous sa combi, et lui attrape les fesses.