title:          Nouveau_2
ID:             47
type:           md
compile:        2
charCount:      4470



Shi est à la limite de l'hypothermie. Nue dans la combi lourde, c'est peut-être la première fois qu'elle regrette ne pas avoir de la micro-fibre pour se tenir chaud. L'atmosphère de sa combi est maintenue à moins dix. Celsius. Au-dessus de cette température et les capteurs d'approche sonnent. Elle tremble, ses lèvres doivent être bleues de froid, elle n'a plus vraiment de sensation après les genoux et les coudes.

Elle serre les gants lourds sur les arrêtes des modules d'hydroponie. La voix d'Echo qui semble gueuler sur tout le monde et oublier que ses coms sont ouvertes l'aide à rester consciente. Ses réserves d'air sont pas géniale non plus. Les filtres à CO² sont beaucoup trop  chaud pour qu'elle puisse les activer dans ce genre de situation. 9% de CO² dans le mélange gazeux qu'elle respire. Essentiellemet de l'oxygène. Les peintures externes des modules d'hyproponique lui indiquent M37. Niveau 3. Le module 38 est quelque part au-dessus d'elle, et il va falloir qu'elle fasse un retournement tendu et traverser vingt mètres de vide.

"Echo", dit-elle, après une profonde inspiration pour maîtriser son claquement de dents.

"Oui ?" Echo semble essouflée, comme après le sexe. Ou comme quand elle fait une crise d'angoisse.

"Je t'aime." Shi ferme les coms. La situation a dégénéré à une vitesse absurde. Ce n'est pas juste une gestion d'incident. C'est un verrouillage culturel et économique. Un exemple. Quelqu'un sur terre, ou mars, à des unités astronomiques de là, à décider qu'il fallait que les spaceux acceptent un peu plus la culture des puits.

Ou alors personne ne décide. Ils ont envoyé les copycops pour nettoyer derrière le désastre Ikomi, et ils ont gérés comme les brutes pesantes qu'ils sont. Et les habitants de KPI l'ont pas non plus joué fine. Et un chef a du vouloir une promotion, ou régler la situation rapidement et exercer du contrôle. Et pour ça il doit briser la solidarité, donc il mets des personnes vaguement soupçonnés de violation de copyright dans des sas et il attends de voir ce qu'il se passe.

Ce qu'il se passe, c'est qu'elle est trop conne pour ça, et que du coup elle s'est désignée volontaire pour le sauvetage. Ou pour le suicide, vu d'ici ça y ressemble. Mais il faut bien que quelqu'un agisse, que quelqu'un refuse de se laisser faire.

Son champ de vision réduit. Le CO² commence à altérer ses perceptions et ses facultés de jugement.

"Aller ma grande, comme à l'entraînement. Poussée, déhancher, et tu percute le module. Sans rebondir. Facile." Sa voix lui paraît étrange. Elle compresse ses jambes, forçant le sang à circuler dans ses muscles engourdit. La douleur irradie de ses mollets, de ses genoux, de ses cuisse alors qu'elle appuie et s'élance vers le module H38.

Le bruit de son casque heurtant la paroi de sa destinatoin la surprend. La réaction commence à la faire pivoter et d'un réflexe salutaire, elle réussit à attraper un des énormes boulons servant d'attache aux modules d'hydroponie. La douleur explose dans sa main gelée alors qu'elle serre de toutes ses forces, transformant encore plus d'oxygène en poison.

Son bras tire, comme si elle était suspendue dans un puits gravitationnel. Mais elle tiens bon, plaquée à l'envers contre les structures. D'ici, le système bien ordonné de la getsion des bassins et modules hydroponique, ressemble à n'importe quelle structure. Un gros tube pressurisé, des vannes et ports divers, des grilles pour se déplacer, et des fixation avec des écrous rivetés pour maintenir l'étanchéité.

Et c'est ces écrous auquel elle se cramponne actuellement, essayant de limiter sa respiration. Et de maintenir l'effort en dépit de l'onglée qui lui cisaille les doigts. Il faut encore qu'elle trouve le panneau externe et, de là, tracer son chemin dans le système d'exploitation du module et activer les fonctions autonomes pour le décrocher. Chaque bout de la station en dispose d'un, c'est plus simple que le module puisse s'amarrer de lui-même que de devoir utuliser une grue inertielle, un bras mécanique ou autre chose.

Neuf pour cent. Au delà des limites de fonctionnements normales. Elle trouve le panneau de contrôle, mais elle n'a plus la force de s'y connecter. Le froid la paralyse, le CO² lui file la nausée et l'empêche de réfléchir. C'est terminé pour elle. Lentement sa prise sur les écrous se desserre, et elle flotte, elle ne sent plus rien et elle accepte le noir qui l'englobe de plus en plus avant que son cœur, à son tour, ne cesse de battre.
