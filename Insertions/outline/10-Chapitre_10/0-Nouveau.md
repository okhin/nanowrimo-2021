title:          Nouveau
ID:             46
type:           md
compile:        2
charCount:      3627


"Kam, respire. Et redis-moi, je suis pas sûre d'avoir compris. Isham est où ?" La détresse de Kam est palpable, même à travers le spectre électromagnétique, et en dépit de l'échantillonage foireux des systèmes à ondes courtes qu'elles utilisent en général pour se parler hors grille. Elle peut entendre la gorge irritée par l'envie de pleurer, l'effort fait pour transmettre les informations pertinenetes et ne pas s'effondrer, la lutte conre le stress, contre le détachement et contre la peur.

"Je suis pas sûre. Il … " Kam ne trouve plus les mots. Ça n'aurais pas du se produire. Isham n'y ets pour rien, c'est elle qui devrait être dans ce module de com maintenant entièrement isolé de la RKP avec moins d'une demi heure avant que l'intoxication au CO² ne commence.

"Je l'ai localisé." La voix de Shi, autoritaire, précise. "Module H34 Niveau 3". Hyprodponique. Pont 34.

"Shi? Merde, t'es où?" Demande Echo, inquiète.

"En route. On a eu le ping d'Isham, t'inquiètes Kam on devrait pouvoir le sortir de là. J'ai du passer par la logistique cependant. Echo, tu dois un verre à Rock. Désolée, j'ai pas eu le temps de finasser." Rock. Un slasheur. Une ancienne star, qui cherche à relancer une carrière hors des puits et qui est, comme de nombreux autrs, atiré par les champs gravitationnels de la ceinture, coincé en basse énergie dans les rade les moins fréquentable de stations déjà pas vraiment connue pour leur facilité d'approche.

Mais Rock est fiable. Et il a une dent contre plusieurs des corpos qui essayent d'imposer un blocus culturel sur RKP. C'est aussi probablemet l'un des meilleurs pilotes de ventouse dans cette partie du système, et sans ventouse, extraire Isham de là où il est n'est pas vraiment possible. Et déjà, forcer la déconnexion du module H34 va pas vraiment être une sinécure, les copycops ne vont pas faciliter la vie de Shi.

"Kam, parles-moi. T'es où? Je viens te trouver." Echo finit d'enfiler le bas d'une combi lourde avant de se précipiter dans les couloris de la station. Elle atteri sur la cloison sur l'épaule, amortissant à peu près le choc, avant de saisir la sangle déambulatoire et de commencer à sauter de poignée en poignée, esquivant — à peu près — les personnes allant à leur vie. Mais Kam ne répond pas, elle fixe la baie de dispatch. L'écrana ffihe les trajectoires de tous les vaisseaux actuellement en mouvement, dessinant un nuage d'orbite, évoquant la structure des atomes.

Mais elle se concentre sur un groupe de trajectoire en particulier. Les échos les signalent comme étant des Comex-6, mais vu leur trajctoires ils ont probablement changé les systèmes d'attitudes. Les deux C-6 sont sur une trajectoire serrée d'interception d'un Kami-TX, une ventouse, comme celle utilisée dans les docks en zéro-G. Comme celle que pilote Rock en ce moment, avec Shi.

"Kam! Répond merde". Echo arrive sur l'axe central de RKI, Il n'est plus vraiment possible de jouer des coudes et des genoux pour avancer plus vite. "Shi, t'en es oú?"

"Shi n'est pas avec moi." Une voix rauque, abimée par les acides et d'avoir trop hurler dans des micros. "Je sais pas où elle est, mais elle avait l'air salement énervée. Moi je fait tourner les copycops, le temps qu'ils comprennent ce qu'il se passe. Mais ils sont bons les cons."

"C'est parce que c'est des assistants. Ya que les slasheurs pour croire que les copycops se risquent dans le vide." Echo peut entendre le sourire narquois de Shi alors qu'elle répond à Rock.

"Oh, va te faire OK. Et magne toi, ils se rapprochent, et j'ai pas l'impression que ces C-6 soient là pour juste poser des questions. Je vais devoir prendre le large."
