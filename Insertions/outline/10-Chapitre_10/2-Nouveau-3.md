title:          Nouveau_3
ID:             48
type:           md
compile:        2
charCount:      5299



"Je t'aime" dit Shi. Echo cesse de bouger. Elle sait qu'elle ne se reverrons plus, que c'en est fini pour Shi. Mais Echo refuse de laisser cette pensée s'installer. Elle ne peut de toutes façon pas faire grand chose et il sera toujours temps plus tard de se morfondre. Pour le moment, elle affiche sur ses lentilles les contantes de Shi, et elle essaye de réfléchir plus vite que le CO² n'augmente dans la combinaison lourde de l'une des balises à laquelle elle s'est ancrée.

Kam est probablement out aussi, trop de stress ces dernières semaines, elle a craqué, ce n'était qu'une question de temps. Reste elle.

"Hey, Echo ? Au sujet de ce verre … " Et Rock donc.

"C'est pas vraiment le moment."

"Je m'en doute. Ça le sera jamais. Les C-6 me mitraillent et ils refusent de décrocher, Et j'ai de toutes façon bientôt plus de jus." Les bonnes nouvelles arrivent toutes en même temps soupire Echo. Même si il est chiant par moment Rock ne mérite pas de se faire exploser en vol. Personne ne le mérite.

"Soit pas si triste meuf, c'est ce que je voulais. Partir dans un feu d'artifice, en pleine gloire."

"Pour la gloire, tu repasseras hein, tout le monde t'as oublié mec."

"Hey, laisse moi rêver encore dix minutes d'accord ? Bref. J'ai pas de testament, de dernière volonté ou quoi. Par contre, tout ce que je peux faire c'est emmerder ces copycop avec moi. Et je sais que tu peux me calculer un vecteur de tête, un qui me mette sur une trajectoire de collision avec … je sais pas. trouve un truc qui les fera chier d'accord?"

Non. Echo refuse d'envoyer quelqu'un s'exploser en kamikaze. Il va mourir, peut-être, mais elle ne veut pas avoir son mot à dire sur ça.

"Ici Kam. Je t'es balancé un vecteur Rock. Éclate-toi."

"Kam, tu fais quoi ?" demande Echo, une colère commençant à la submerger.

"Mon taff. Peut-être pour la dernière fois. Il va se crasher de toutes façon, et il y a moyen de faire quelque chose avec ça." La détermination du désespoir s'incarne dans la voix de Kam, alors qu'elle transmet le vecteur d'approche pour que Rock s'écrase à mi-hauteur du module H30. Juste au point d'accroche avec l'axe central de KPI.

Echo navigue frénétiquement dans les systèmes de surveillance eterne de la station pour voir ce qu'il se passe dehors. Elle ne peut plus déclencher l'alarme, elles sont sous contrôle des Sigmas.

"Kam, ça va fracturer la station! Il y a du monde là-dedans!"

"La station est morte Echo. Les hydroponiques s'effondrent. On est isolées socialement, on ne peut plus rien faire sans devoir signer un NDA. C'est fini. mais on a une chance de sauver Isham. Et peut-être Shi."

Plus la peine de discuter. Trop de choses se bousuclent dans la tête d'Echo au moment où Rock, l'habitacle percé par les projectiles que lui ont balancé les C-6, impacte la station, juste entre les modules d'hydroponie. Quelques instants après, l'explosion du vaisseau de taille ridicule par rapport à l'imensité de la station, envoie des dębris dans toutes les directions.

Les couloirs de transits deviennent instantanément silencieux. D'abord par la stupeur qui saisit l'ensemble des personnes. Puis par le fait que le bourdonnement des systèmes électriques s'arrêtent, les sangles de déambulations s'arrêtent. Quelques secondes après, une secousse agite la station au moment oú l'axe central s'éventre et que les modules hydroponiques sont éjectés par les systèmes de sécurité autonomes, essayant de limiter les collisions.

L'alerte les rattrapes quelques instants après. À moins que, sous le choc, Echo ne l'ai pas remarqué plus tôt. La plupart des modules d'habitations se détachent eux aussi de l'axe, décomposant la station en de nombreux modules hétéroclites qui, quelques instants plus tôt, formaient encore un tout harmonieux.

Et, à peine visible, dans le coin de l'œil d'Echo, les constantes de Shi clignotent en alarme. Trait plat, plus de pulsations cardiaques. Température corporelle trop basse. Morte. Echo est bousculée alors que des équipages ad-hoc commencent à s'organiser et à rétablir des canaux de communications. Quelqu'un la prend par le bras et la tire dans un module gnflable, en dehors du chemin.

"Salut. Je suis Deb. Tu as pas l'air bien et on va être occupé quelques jours à réparer ce bordel, donc tu peux rester là jusqu'à ce que ça aille mieux, d'accord ?" Devant le silence d'Echo dont le monde viens de se fracturer, elle ajoute. "Serre moi les mains si tu as compris ? Et je suis désolée, mais je vais devoir te laisser seule. Toute aide est nécessaire." Echo serre les mains de Deb qui la laisse seule dans ce petit module.

"Echo ?" La voix de Kam. Paniquée, fatiguée, usée par les pleurs.

"Dis moi qu'Isham est vivant. S'il te plaît" Que tout cela ne soit pas pour rien, ajoute-t-elle mentalement.

"Je ne sais pas. Tu veux que je vienne te voir ?"

"Et tu ferais comment ? Tu vas pas dans le vide, tu te rappelle ? Et moi je suis … Ëcoute, j'ai besoin d'être seule là maintenant. Je te recontacte. Peut-être." Echo ferme le communicateur et le projette sur le mur en kevlar. Mur qui absorbe le choc et restitue donc l'énergie dans le communicateur qui part donc en tournoyant dans l'habitacle. Des larmes s'échappent des yeux d'Echo, formant de petites billes d'eau salée, faisant miroiter la lmuière rouge qui baigne l'intégralité des habitats.