title:          Nouveau
ID:             31
type:           md
compile:        2
charCount:      1820


La réponse d'Echo est étrange, pour le moins. Hapse ne la connait pas si bien que ça — le problème de ne se voir que tous les six ou septs ans, augrès des orbites — mais quelque chose ne colle pas. Le vocabulaire, les intonations, les accents. Ça n correspond pas au profil enregistrè dans les bases sociales du WatUp. Qui ne sont peut-être pas tant à jour que ça, mais personne ne change autant en si peu de temps, à moins d'un changement brutal de trajectoire. Et ce genre de changement peu se voir par les déséquilibres apportés aux systèmes liés.

Soit Echo a changé, soit il se passe quelque chose sur KBI. Et étant donné les platitudes contenues dans le message, Hapse ne peut qu'imaginer que quelque chose se passe dans le voisinage de 1-Cérès. Et pour qu'Echo prenne le temps de prévenir qu'il se passe quelque chose, c'est qu'elle se sent en danger Hapse — ou Ark à priori — ne peut rien y faire, ils ont encore pesque un an de route, sans parler du sauvetage du RK-16. Et avant de pouvoir établir un canal de communication clair, Hapse doit maintenir la légende d'Echo et donc ne pas répondre, son message n'appelant pas de réponse.

Une dernière possibilité, c'est que ce ne serait pas Echo qui ai répondu. Ce qui implique que son message ait été intercepté, et qui corrobore que quelque chose sur KBI ne va pas. Parce qu'il n'y a pas vraiment de raison qu'il soit sur écoute, et son message n'est clairement pas quelque chose qui menacerait quelqu'un.

"Analyse. Dernier message reçu d'Echo. Cherche des motifs de synthèse, des artefacts. Et donne moi une liste probable des réseaux qui ont généré ce message."

Tant pis pour la surcharge en cycles d'horloge. Mais ça peut valoir le coup de trouver qui est derrière ce message, si c'est bien un faux. Au moins afin de savoir dans quelle merde ils ont marché.
