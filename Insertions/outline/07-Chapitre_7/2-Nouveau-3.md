title:          Nouveau_3
ID:             33
type:           md
compile:        2
charCount:      2646



Shi est, comme d'habitude, la première levée. Kam et Echo sont encore harnachées ensemble dans le hamac accroché à l'une des cloisons du modules qu'elles ont investis il y a dèjà quelques mois. Elle a enfilé la combi souple d'Echo et profite de l'odeur de sueur, et des phéromones libérées durant les heures précédentes.

Elles sont à l'étroit dans ces quelques mètres cubes, quelque part au bout du bras zénithal, dans l'un des modules gonflables qu'elles partagent avec deux autres pods. Mais c'est chez elles, et ici au moins, les rats corporatistes leurs foutent la paix, pas assez de gravité ou de pression atmoshériques pour leurs pauvres organismes dépendant de la gravité.

"Hey, c'est à moi ça" laisse échapper Echo dans un baîllement.

"Oui. Et, si tu la veux, va falloir venir la reprendre ?" Lui répond Shi, lui jetant un regard narcois tirant sur son pack de café tiède.

"Café d'abord." marmonne Echo, se détachant de Kam et tendant le bras à travers le volume de leur habitat, entre les jambes de Shi, pour fouiller dans le compartiment des réserve alimentaire et attraper un pack de café. D'une main experte, et alors que Shi la coince entre ses jambes, elle déverouille la capsule de CO² qui réchauffe instantanément le liquide noir qui n'a de café que le nom, mais qui à l'avantage de fournir également une bonne partie des nutriments nécessaires.

Shi finit son café et, suite a un subtil mouvement de hanche, fait dériver Echo pour pouvoir l'embrasser tête bẽche avant de refermer la combinaison rouge de la pilote et de descendre à travers le sas, avant d'attraper une des glissières qui la tracte vers la colonne centrale de KIP, laissant Echo à son café, sans sa combinaison.

Ça va faire presque deux mois que les corpos ont débarqués, et la situation ne s'est pas améliorée. L'espace commun est maintenant envahi des pubs vantant divers produits inutiles, réutilisant les images des derniers XPorn d'Ikomi — techniquement pas de la pornographie vu qu'il y a du placement produit — pour vendre maquillage, lingerie, stims. La plupart de ces produits étant de toutes façon inefficaces, inutiles ou dangereux dans les milieux confinés des habitats orbitaux.

Le but, de toutes façon, n'est pas de vendre, mais d'occuper l'espace. De montrer que les Puiseux sont ici chez eux, et que la fête est finie. Le but est aussi, probablement, d'énerver Shi le matin quand elle part s'occuper des hydroponiques. Non pas tellement pour maintenir le système de survie, son intérêt initial, mais plus pour se regrouper avec d'autres et essayer de se créer la marge de manœuvre nécessaire pour reprendre un peu le contrôle sur la station.
