title:          Nouveau_2
ID:             32
type:           md
compile:        2
charCount:      2328



Trois semaines à bouffer des rations déhydratées commencent à payer dans le Unknown Artist. Les aloé vera font leur job et maintiennent les niveaux de CO² suffisament bas pour qu'Ark envisage de redescendre la pression atmosphérique et le niveau d'Azote atmoshpérique.

Trois semaines aussi à manger 0.2g d'accélération, poussé par la pleine puissance des MET, en direction d'un point dans l'espace où se trouvera, normalement, une fronde en détresse. les jambes et l'épine dorsale d'Ark, comme celle de l'Unknown Artist, commencent à accuser le coup, l'un comme l'autre préfèrent en général des poussées plus courte. Encore deux jours avant que les batteries ne puissent plus alimenter la corolle de plasma que pousse de sa masse le vaisseau, ralentissant jusqu'à atteindre la vélocité nécessaire pour l'approche.

Il faudra ensuite plusieurs jours face au soleil pour recharger les blocs de batteries et, de nouveaux recommencer l'opération. Mais au moins, Ark pourra manger autre chose que des nutriments agglomérés et profiter de l'absence de gravité. En attendant il se traîne dans sa combinaison dure, arrimé aux rails qui courent le long de l'épine dorsale multicolore du Unknown, armé de sa torche à plasma et d'un ruban de fullerènes à la recherche des impacts de micro-météorites qui ont frappé l'Unknown.

Rien de critique n'a été touché, elles ont essentellement impacté les coursives qu'Ark maintient sous vide, pas besoin d'y faire circuler une atmosphère, lorsqu'il faut y passer, il est plus simple de verrouiller sa combinaison et de passer par un sas que d'y maintenir une atmosphère. Et en cas de micro météorites, au moins l'Unknown ne perd pas de précieux oxygène.

Mais vu qu'il va y avoir du monde à bord, il va falloir augmenter le volume d'air en circulation dans le système, et notamment connecter les pièges à carbones installés a mi-longueur des espaces d'habitations. Ark se déplace donc avec ses bras à la recherche du néon qu'il a injecté dans les espaces inhabité. Deux heures déjà qu'il cherche les volutes mauves que ses capteurs devraient voir, mais Ark a tendance à être distrait par les iridescenes dégagé par les ions et la vapeur d'eau expulsé par les MET, teintant le spectre électromagnétique de rayonnement que seul les capteurs terahertz d'Ark peuvent voir, se mélant à la voix lactée.
