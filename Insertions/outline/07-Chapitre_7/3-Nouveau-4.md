title:          Nouveau_4
ID:             34
type:           md
compile:        2
charCount:      3842



Les rapports que reçoit Hapse de ce qu'il se passe sur HKI ces dernières semaines ne l'aident pas à dormir. Ce qui l'a amené à légèrement modifier son cocktail hormonal pour mieux dormir. Mais les nouvelles d'HKI ne sont pas bonnes.

Barrages mémétique, création d'une chaîne unique de logistique, exploitations des ressources locales au-delà de leurs capacités de production. Cela ressemble au début des récits de Juni, décrivant la chute de toutes les micro-nations du système solaire, avant que les communautés distantes et hors-grave ne finissent par s'affranchir de la taxe gravitationnelle et ne s'organisent hors Puits.

Mais chaque habitat, a du faire face à ce qu'HKI est en train de subir. Hapse voit les trajectoires des différents mouvements s'aligner. Les modérés être assimilés, les radicaux perdre leur énergie et finir par courber l'échine, et tous semblent graviter non plus les uns autour des autres, ou au sein d'ensemble sociaux complexes, mais sur une orbite stable autour d'un meme-plexe puissant, dans une apparente stabilité.

Tout est une question de masse. Et donner de la masse aux mème est la façon dont les corporations finissent par établir leur contrôle. Ça et une chaîne logistique bien intégrée et efficace. Hapse ne peut pas faire grand chose pour le second, mais Love and Rage a acquis au fil des décennies de luttes quelques compétences de guerre mémétique intéressante.

Par exemple, le meme plexe semble tourner autour de la disparition d'Ikomi. Un simple clivage, une confrontatoin entre deux groupes, autour de ce qui est exposé comme la décision d'une seule personne, et toutes les discussions tournent instantanément autour d'elle. Toutes les publicités utilisent son image, des gros plans de sa langue léchant ses lèvres, ou de ses yeux humides entour de maquillage waterproof, extraits des XPorns diffusés sur les canaux "alternatifs".

Tout le monde veut ce maquillage qui reste en place, peu importe les outrages qu'il subit. Peu importe ce qu'à subit la poupée, défoncée aux chem-mem, aux euphorisants et aux scripts dont l'image est exploitées. Ce qui veut dire que tout le monde regarde ce clip. Et c'est là que les corpos sont un peu moins efficaces. En cherchant à réduire les coûts, elles n'utilisent pas leurs infrastructures pour leur communication parrallèle.

Et personne n'irait se toucher sur un clip si il était diffusé sur un canal pour enfant aux heures de pointes. Pour que ce truc fonctionne, il faut que ce soit vaguement clandestin, il faut aller le chercher. Les corpos diffusent donc leurs merdes sur les fréquences et les serveurs de Love and Rage, comptant sur le fait que LNR se refuse à supprimer du contenu et considère que, au final, les geticulations des corpos sont futiles.

Pas complètement faux. Mais altérer un fichier n'est pas une suppression. Et, bon, techniquement la pub n'est pas vraiment une forme d'expression individuelle. Et il faudrait aussi disposer d'une clef d'accès aux archives de Love and Rage. Et trouver comment modifier le clip pour réduire ce cmplexe mémétique autour duquel tout semble tourner.

Ça tombe bien, Hapse dispose justement de lune de ces clefs. Comme quelques milliers de personnes au final. Love and Rage donnant globalement les clefs à qui les demande. Et cela fiat plusieurs jours qu'Haspe travaille un narcorythme de blackout. Pas vriament une perte de conscience, mais une gueule de bois carabinée. Du genre de carabine qui pourrait désorbiter un de ces maxi-cargos-XL. Quelque chose qui devrait au moins donner la gerbe à quiconque se retrouve ensuite exposé aux pubs en questions.

Pas de manière permanente, juste quelques heures. Et pas toutes les versions du clip, il y en a beaucoup trop qui circule, et toutes les éditer serait impossible. Juste quelques unes. Suffisamment pour que quelque chose de bizarre se passe.
