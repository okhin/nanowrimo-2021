title:          Nouveau_5
ID:             35
type:           md
compile:        2
charCount:      1474



Trois mois que Kam se rejoue encore les trajectoires de la horde à essayer de savoir si elle aurait pu éviter tout ce merdier. Elle sait que la réponse est négative, mais elle se sent tout de même coupable de quelque chose. Coupable peut-être d'avoir suivi des scripts. Coupable peut-être d'avoir été fan d'Ikomi — ou de Jam's qui semble avoir complètemet disparu des radars.

Au moins, le bombardement mémétique a ralenti. Pas comme si tout le monde passait à autre chose, mais plus comme si tout le monde évitait d'en parler. Comme dans le centre de dispatch. Plus personne ne se regarde vraiment dans les yeux. La moitié des présents somnolent à moitié et font parti des réquisitionnés.

Mais personne ne fait plus non plus vraiment attention à ce qu'il se passe autour de 1-Cérès. Les orbites basses sont, certes, plus facile d'accès pour les corpos à la surface, mais ça fait quelques temps que Kam parque de petits chargements sur des orbites Molnya, plus difficiles à remarquer pour les contrôlleurs. Personne ne s'attend à ce qu'elle fasse correctement son travail, ou que n'importe quel spatial le fasse. Les cargaisons manquantes sont juste enregistrée en pertes et profits par les auditeurs corporatistes, qui finiront bien à un moment par réaluer les prix de vente, mais d'ici là kam aura accumulé un stock de carburants, nutriments et produits de première nécessité — et, ok, quelques choses qui n'en sont pas — pour la suite de leur projet à elle, Echo et Shi.