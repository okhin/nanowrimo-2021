title:          Nouveau_2
ID:             77
type:           md
compile:        2
charCount:      3559



Nyasid a fait un gros boulot de mise en forme des notes laissées par Shi, transformant chacune des artères du vaisseau en petit jardin, densément peuplé de fleurs, de lianes et de végétaux remplissant essantiellement des rôles d'agrément, mais pas seulement — les plantes participent toutes au système de purification de l'air et une bonne partie d'entre elles plongent leurs racnes dans les filtres au charbons actifs utilisés pour la filtration des eaux, et Pak avance douvemet au milieu de ces fleurs qui commencent tout juste à sortir de terre, elle s'éloigne de l'axe central du vaisseau pour se diriger vers les petits ballons gonflés à l'extérieur de la station. L'un de ces neufs cubes sert de chambre à Kam. Ça fiat un petit volume pour une personne, mais cette espace sert essentiellement à s'isoler socialement et à dormir, la plupart des activités quotidiennes ayant, de toutes façon, lieux dans un des modules d'habitation — un à chaque extrémité de la configuration.

Suivant des yeux un hyménoptère à la recherche de nectar, volant paresseusement dans l'environnement en faible gravité, Pak finit par trouver le cube de Kam. Bordé d'une salle d'eau et d'un module énergétique, le sas est ouvert et laisse filtrer une lumière mauve. Passant la tête par l'embrasure, Kam salue Pak de la main et l'invite à l'intérieur. Les cloisons en kevlar et fullerenes sont encore relativement vierge de décoration. Le hublot sous leur pieds permet de voir passer, une fois toutes les trois minutes, le Xenon Girl.

"Bon, bienvenue chez moi, c'est un peu le bordel encore, mais on s'sinstalle. Et c'est pas vraiment la priorité là, il faut qu'on se barre assez rapidement. Donc fait vite, si possible."

"OK. Vous avez besoin d'un endroit pour acceuillir un concert, ou quelque chose du genre. Moi j'ai besoin de me barrer d'ici. J'ai plus la licence de mon bar et, même si le bâtient sera réhabilité un jour, ça sera plus le truc pour lequel j'ai trimé ces cinq dernières années. Vous me aissez m'occuper du module de proue, de le convertir en ce dont on a envie. Je suis autonome, et je peux m'installer en … " Pak jette un œil à son poignet sur lequel se trouve un afficheur baignant l'envronnement proche d'une lumière turquoise " … quatre heure. Dix pour être fonctionnelle. Et le temps qu'on arrive où vous voulez, j'aurais une escale prette à accueillir du public, à diffuser du son, et aprovisionnée."

"J'ai une question", demande Peg qui, a l'invitation de Kam, arrive elle aussi dans le module, se glissant juste à la droite de Kam. "Pourquoi nous ? Il y a d'autres équipages qui se barrent, et il y en a beaucoup qui t'attireront moins d'emmerdes."

"À cause de ce que Kam a dit tout à l'heure. Rapport au fqit que ce serait bien de trouver d'autres moyens d'exister et de défendre des idées qu'en mourrant pour une cause. Et bon, ça va être le concert du siècle. Je serait stupide de ne pas saisir l'ooportunité, non ?"

"Ok, ça me va", répond Peg. "Echo est partante aussi à priodi, et Nyasid ne s'y oppose pas. Reste Kam qui doit se décider."

"Je suis décidée depuis le début moi, répond Kam, bienvenue à bord Pak."

"Euh … ça fait combien de temsp que vous avez décidé que je venais?"

"À peu près depuis le moment où Ikomi a fait son annonce. Peg me doit quelques eks, elle pensait que tu nous aurais demandé plus tôt. Allez viens, je te fais visiter et je vais te montrer ton cube. T'as deux heures pour transbahuter ce dont tu as besoin, mais Nyasid est déjà en route avec une pince. Tu fera ton installation une fois qu'on aura décoller."
