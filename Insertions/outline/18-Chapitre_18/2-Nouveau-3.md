title:          Nouveau_3
ID:             78
type:           md
compile:        2
charCount:      5428



Kam retrouve Echo dans le module de commande. Echo est silencieuse et fait face au transpondeur, encore vierge de données. Pas qu'il soit absoument nécessaire de le remplir, mais c'est quelque chose de plutôt bien vu dans les environs de spatioports autonomes.

"Alors ?" Demande Echo, à Kam.

"Elle est partante. Et Myasid arrime les derniers container de ses affaires et tout le monde est prêt à partir. Il ne reste plus que toi. Capitaine."

"Ouais, ouais, moque toi. Mais c'est vous qui avez fait ce choix hein. Et avec tous vos arguments vous avez finit apr me convaincre que oui, il faut une personne qui puisse trancher une décision. Je suis pas certaine qu'il n'y ai pas d'autres possibilités, mais soit. Capitaine."

"Et donc, t'as trouvé comment on s'appelle ?"

"Non". Kam remarque le curseur du transpondeur qui va à rebours, effaçant les caractères entrés pas Echo sur le transpondeur, ne laissant que les trois lettres du patronyme de leur partenaire décédée. "Je … C'était son projet à la base. Moi je voulais juste faire des courses, et toi tu voulais juste passer ta vie dans les réseaux logistiques. C'est elle qui aurait voulu ça."

"Et on l'aurai suivi, et elle nous aurai amené au bout de l'univers, et elle n'aurais pas eu à nous demander. Et on va faire pareil. Et différemment. Et on ne l'oublieras pas, et on va avancer une impulsion de plasma après l'autre, droit dans la confusion et le chaos. Et on sera là. Toi, moi, Peg, Ikomi, Pak, et d'autres. Certains partirons, d'autres nous rejoindront. Mais on sera toujours là."

"Tu cites encore Ikomi toi", depuis quelques semaines, elles parlent d'Ikomi comme si elle était déjà là, à bord du vaisseau.

"Oui, et elle a raison."

"Je sais. Mais quand même, ça me rends triste. Mais du coup ça me donne une idée de nom." Les doigts d'Echo courrent sur le clavier et complètent l'invite du transpondeur.

		"Le dernier jardin de Shi"
		
"T'en penses quoi ?"

"Mmmmm … je sais pas. Mais je m'habituerai, si toi ça te conviens ça ira pour le reste d'entre nous." Kam appuie sur le bouton de validation et leur configuration devient le dernier jardin de Shi, et dans al foulée elle embrasse son capitaine.

"Bon, je te laisse à ta passerelle. je t'ai chargée ta trajectoire de sorti et c'est Lila qui va t'aider à naviguer hors d'ici.

"Oh, et j'ai failli oublier, laisse le verrières ouvertes pendant la manœuvre. Les autres ont insistés pour préparer un petit quelque chose. Ils m'ont pas dit quoi, juste qu'il faudra le voir."

"Ok. Tu vas avec Pak ?"

"Ouais, faut que je l'aide à arrimer deux petits trucs doux et ferme contre la cloison de son neuf cube."

Echo laisse Kam partir, elle aurais préféré qu'elle reste là, mais Kam a raison, Echo doit faire ça seule. Elle remonte les verrières et laisse l'environnement extérieur prendre le pas sur les interfaces de pilotages habituelles.

Et alors que la capitainerie demande les commandes du dernier jardin, Echo peut voir tous les autres assemblages, toutes les configurations, les frondes, les pinces, tout ce que le voisinnage d'HBI contient de pilotes et de vaisseaux, tous forment un tunnel autour de la trajectoire du dernier jardin.

"Ici Lila, capitainerie du voisinnage temporaire, ici pour vous guide Dernier Jardin de Shi. Et je vous souhaite bonne route de la part de tout le monde."

"Ici Echo, capitaine du Dernier Jardin de Shi, qui vous laisse les commandes. Et merci de l'escorte."

Echo sait que tous ne sont pas là pour les encourager ou leur dire au-revoir. Elle ne connaît pas la plupart d'entre eux. Mais Ikomi, son concert, le Xenon Girl, la mort de Shi, et la destruction de ce qui était leur habitat a transférer une grande partie de la sympathie et du soutien des habitants sur leur vaisseau, sur le Dernier Jardin de Shi.

Echo se saisit des coms internes.

"Hey, ici Echo." Un blanc. "Votre Capitaine pour ce trajet. Je ne sais pas pour vous, mais j'ai hâte de partir d'ici. Je ne sais pas trop ce qui nous attends sur 4-Vesta, mais je sais qu'on sera toutes ensemble pour ça. Alors accrochez-vous à ce que vous pouvez, parce qu'une fois le double Vasmir en route, le dernier jardin de Shi ne fera pas demi-tour."

Pas terrible. Elle est définitivement pas une capitaine qui fait de grands discours, mais ce n'est pas pour ça que les autres ne lui ont pas trop laissé le choix.

Elle se campe sur ses jambes derrière sa console, se préparant à encaisser les quelques dixièmes de G d'accélération que vont lui infliger les Vasimr, l'espace d'un instant elle imagine Kam se tenant au-dessus de Pak, lui tombant dessus au moment oú la poussée s'établit, lui faisant esquisser un petit sourire au milieu des larmes de tristesse et de bonheur de la vague d'émotion qui la saisit alors que le dernier jardin s'élance doucement á travers la haie disparate d'engins qui l'escortent quelques temps avant que Lila ne lui rende les commandes et qu'elle ne démarre la post-combustion des moteurs, envoyant le jardin sur une orbite d'insertion avec un gigantesque tas d'emmerde.

Mais Echo va bien. Elle est entourée par les souvenirs de Shi, les nombreuses fleurs qui parsèment le vaisseau et qui lui donneplus l'aspect d'un système organique que de froides coursives d'habitat, elle est entourée de gens qui veulent vivre et aimer. Des amies, des amantes. Et elle va aussi revoir son père. Et enfin rencontrer Ikomi, même si elle a l'impression que celle-ci fait déjà partie de son pod.