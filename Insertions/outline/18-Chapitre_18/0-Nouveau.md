title:          Nouveau
ID:             76
type:           md
compile:        2
charCount:      10122


Les derniers tests d'allumage sont concluant et la configuration semble équilibrée. Trois jours à passer l'ensemble des systèmes à l loupe pour s'assurer qu'ils fonctionnent tous, sont correctement étalonnés, et qu'il n'y a pas de résonance étrange, mais enfin, elles sont prêtes à partir. Il reste encore quelques menus détails logistique, mais Kam laisse les autre s'en occuper, elle, elle doit régler un problème un peu plus conséquent.

Ikomi revient et, d'après les mssages qu'elle a diffusé sur l'ensemble du spectre des média sociaux, elle entend bien mettre les choses au clair lors d'un ultime concert. Et, d'après les messages que Peg a reçu, et dont elle a fiat part — partiellement — à Kam, Sigma et d'autres copycop vont essayer de faire en sorte que ça n'arrive pas.

Un petit groupe, principalement issus de Love And Rage, s'est constitué autour de leur configuration Sandy afin de mettre en place des systèmes et techniques permettant de tenir tête à Sigma. Idéalement, sans passer par la case armement même si Kam sait que ce n'est qu'un vœux pieu. PIS fait également parti de cette coallition — qui continue d'utiliser le nom du Love And Rage — ainsi que quelques collectifs et coopératives de mineurs.

Kam a convoqué une réunion de travail au Formerly Known as the Xenon Girl avec Pak — qui gère comme elle peut l'anneau rotatif qui fût autrefois le principal lieu de sociabilité de KBI — et Abdu Xe — qui passe beaucoup trop de temps à écrire des guides de stratégies militaires fonctionnant sans chaîne de commandement.

"La clef ce n'est pas l'échange d'information au final. C'est le partage d'information avant le début de l'engagement" Abdu Xe continue son exposé. "A trop se concentrer sur l'échange d'information, on perd de vue que le réseau de communication est la première chose qui est attaqué par un adversaire.

"Ce qui a amené à l'effondrement du bloc occidental sur Terre, c'est jsutement cette doctrine. Cette idée que leur adversaire utilise les mêmes infrastructures qu'eux et que donc ils n'ont besoin que de protéger leurs commuications, pas de fournir de l'aide à la décision à leurs troupes.

"L'autre problème que celq amène, c'est le fait que, si la communication est faites pendant l'engagement, alors certains informqtions ne peuven pas être échangées à moins de risquer d'être interceptée et de révéler une faiblesse. Celq implique de mettre en place des contrôle d'accès sur certaines informations et message et donc de créer une hiérarchie, et une chaîne de commandement. Ce dont personne ne veut."

"On a compris ça", dit Pak, gardant un œil sur la salle du bar au cas où quelqu'un voudrait commander quelque chose et ne soit pas capable de se servir. "Ce que je ne comprends pas, moi, c'est ce que tu proposes. Tu proposes de fournir quoi … des assistants ?"

"Non. Un modus operandi. Un entraînement. Une expérience collective de la lutte engrangée après des décennies, voir des siècles de luttes. Une connaissance viscérales des manœuvres utilisées par les adversaires, une compréhension intime des comportements de ses camarades, une intelligence distribuée entre les différents membres qui deviennent alors un macro-organisme capable de réagir aux attaques de manière rapide et ne nécessitant pas de coordination."

"Des chem-mems donc." Kam laisse échapper un soupir. "Tu veux qu'on mette à disposition de ce … groupe, groupe qu'il fuat encore constituer, une banque de chem-mem reprenant leur permettant d'intégrer leur entraînement en quelques secondes, c'est ça ?"

"C'est ça." Abdu Xe est très content de lui et de sa démonstration. Pak est sceptique, mais Kam réfléchit. Ça pose pas mal de problème, notamment celui de devoir intégrer de nouveaux souvenirs à cette banque de donnée, ou le fait qu'un usage prolongé amènera nécessairement à des troubles de la personnalité grave.

"J'ai deux questions en fait. Premièrement, tu gères comment l'intégration de nouvelles expériences ? Et deuxièmement, ces modus operandi, tu compte les récupérer comment ?"

"Tu n'envisages pas sérieusement cette idée ?" Demande Pak, manifestement pas à l'aise à l'idée.

"On en discute. Et c'est pas à moi de faire des choix à ce niveau, mais il va bien falloir tenir les Sigma à l'écart, d'une façon ou d'une autre. Et vu l'étât des discussions sur la coallition, j'ai bien peur que cette coallition ne survive plus très longtemps. Bref, Abdul Xe, t'as des réponses ?"

"Pour la seconde oui, j'ai passé les dernières années de ma vie à collecter ça.  U peu de hack pour chopper les softs Sigma, et beaucoup de temps passé avec mon pack à s'entraîner en simulateur ou autre, à créer des manœuvres de combat, enfermés dans nos dojos.

"Pour la première c'est un peu complexe. J'ai une solution, mais elle nécessite un fort contrôle émotionnel, parce qu'il faut des XClips le plus neutre possible émotionnellement. Du moins, si on ne veuxt pas que qtout le monde soit extrêmement confus."

"Ou alors on peut aussi se dire que nos émotions, sont des choses importantes, et qui sont moteurs de nos actions. Pas quelque chose qu'il faudrait supprimer." interviens Pak. "On est pas obligé de tous devenir des machines sans émotions pour survivre. C'est ce que fait Sigma, on a pas à devenir notre adversaire."

"Et comment tu compte vaincre ?" demande Abdul Xe.

"Justement, je ne compte pas vaincre, je ne veux pas les écraser, les détruire ou autre. Je veux juste qu'ils nous foutent la paix, qu'ils comprennent qu'ils ne pourront jamais exercer leur contrôle sur nous. Et je refuse de devenir ce qu'ils sont."

"Et si … " commence à réfléchir Kam, " … on utilisait justement cette énergie émotionnelle. Cette colère, cette affirmation de soi, ces conflits internes et personnels, tout ces trucs là qui font nos vies, qui font que l'on brille si intensément et qui font que l'on préfère mourir que de laisser quelqu'un d'autre crever, même si au final personne ne survit, et si, justement, c'est ce qui nous donnera l'avantage, la résilience, l'énergie de vivre nos vies, complexe, bordéliques, douloureuses, chaotiques et joyeuses ?" Évoquer la mort de Shi et Isham devient un peu plus facile pour Kam. Elle peut le faire sans s'effondrer. Ça fait mal, mais ça fait partie du processus, du moins d'après Peg.

"C'est une approche … intriguante" répond Abdul Xe. "Je ne vois pas pourquoi ça ne marcherai pas. Mais la raison pour laquelle je préconise des enregistrement un peu plus zen, c'est, à la base, pour que justement chacun puisse rester tel qu'il le veut. Oui, l'intégration est plus complexe, mais tout le monde n'as pas vocation à intégrer ses expériences au corpus. On n'a besoin que de quelques extraits et d'assemblage narcorythmique génératif.

"Et le corpus est conçu pour que les expériences ne soient, justement, pas trop alliénante. Ça nous coupe déjà de pas mal de choses, mais ça permet à chaque personne de garder son libre arbitre. C'est juste au niveau des réponses au stress que le corpus change des choses.

"Et bon, nous nous sommes déjà porté volontaire comme ligne de défense. Donc même si cette solution est jugée trop … je sais pas … quelque chose par le collectif, nous l'utilisons, et nous continuerons de le faire. Je pense qu'on pourrait bénéficier de plus de monde pour élargir le corpus, mais vous avez une ligne de défense, quoi qu'il en soit. Et pet-être qu'on y passera toutes, mais, comme tu le dis, au moins on aura vécu une vie intéressante."

Pak frappe sur la table du plat de sa main. "On peut arreter de parler de mourir deux minutes ? C'est une réalité, oui, mais les martyrs et les gens morts, on en a plein. Et c'est pas ce qui fait avancer les choses. Personne ne veut que qui que ce soit se sacrifie — ok, peut-être certains dans le collectif le veule — mais il faut qu'on pense autrement."

"Je suis avec Pak sur ce coup" dit Kam. "Et oui, les Sigma vont probablement nous cogner et nous butter, mais j'en ai juste de marre de voir des gens mourir et, pour une fois, j'aimerai juste qu'on ne réfléchisse pas en terme de qui veut mourir ou qui doit mourir. Mais juste qu'on se débrouille pour que ce concert ait lieu sur 4-Vesta, que l'on puisse fire bouger les choses et que l'on arretes de considérer les morts, les blessés, les cassés comme des martyrs d'une cause qui nécessiterai notre sacrifice.

"Je reste intrigué par tes idées hein, et j'apprécie l'aide que tu proposes. Mais peut-être que vouloir mourir pour une cause est quelque chose dont il faudrait s'occuper. Et que l'on trouve des raisons de vivre plutô que des raisons de mourir. Sur ce, moi il faut que je retourne sur ma configuration. On va pas tarder à devoir se mettre en route, et si je suis pas là pour la navigation, ça va encore mal se finir."

Kam sait qu'il leur reste encore, en vrai, un peu de temps avant de partir. Au moins quelques dizaines d'heures. Mais elle sait que cette conversation ne bougeras plus. Elle quitte la table et, au moment oú elle pénètre le sas de sortie, son communicateur de poignet lui annonce un message. De Pak.

"Il faut qu'on parle. Du Xenon. Chez toi ?" Kam sait de quoi Pak veut parler. Elle veut embarquer le Xenon Girl sur leur configuration. Pas forcément l'ensemble du lieu, mais une partie non négligeable. Elle sait aussi que Pak veut partit d'ici, comme tout le monde ou presque, ce qu'il reste de KBI devenant un cauchemar de corporatisme et de désolation, tout le monde ayant abandonné la station maintenue sous perfusion pqr quelques cargos volontaires qui se font de plus en plus rare.

"Ouais. Laisse moi deux-heures." Kam veut faire un détour d'abord. Elle a besoin d'être seule un peu, et elle apprécie vraiment ses sorties extra véhiculaire pour ça. Le sas se dépressurise et la porte s'ouvre, sa navette est amarrée un peu plus loin, le long d'un mat soudé rapidement au cœur de l'anneau et permettant aux plus petit vaisseau de s'amarrer au complexe. Kam attrape la longe de déambulation et se laisse tracter doucement, flottant librement ou presque. La faisant presque regretter de ne pas avoir bataillé plus ferme et plus tôt pour obtenir une combinaison dure à sa taille et adapté à sa morphologie.
