title:          Nouveau
ID:             60
type:           md
compile:        2
charCount:      6365


C'est Nyasid qui lui a délivré le message. La semaine dernière, il lui a transmis sur un disque sur lequel il a stocké un version hors-ligne de l'imposant fichier. Elle sat que c'est Ikomi qui lui a envoyé, mais elle a attendu d'être seule et un peu disponible émotionnellement avant de se résoudre à le lire.

Les derniers jours ont étés stressant, avec l'assemblage d'une configuration en fleur autour du module d'abitation sur lequel Kam et Echo ont jetté leur dévolu, et depuis lequel elles ont commencé à assembler un glisseur pour se tirer de l'influence de 1-Cérès. Peg donne un coup de main et se pose encore la question de rejoindre de manière permanente le duo qui semble avoir retrouvé un équilibre, incluant plus ou moins Peg dans leurs dynamiques sentimentales. Surtout avec Echo en fait.

Mais elle n'as pas eu tant de temps que ça seule. Pendant que le module central du projet subissait sa réorganisation, les ballons qui servent de cabines individuelles ont été dégoncflés et l'ensemble n'a été rpessurisé qu'il y a trois jours, permettant enfin à Echo et Kam de ne plus squatter dans les dix mètres cubes que constitue le volume d'habitation de Peg.

Elle se retrouve donc enfin seule et reposée chez elle, avec ce disque contenant un message qui, d'après Nyasid, est passé par quelques intermédiaires étrange et trainait sur des listes de diffusions de Love And Rage, n'ayant pas vraiment d'adresse de destination autre que Peg, quelque part autour de 1-Cérès.

S'affichant sur les membranes en kevlar de sa chambre, là où elle projette habituellement sa console, s'affiche le visage d'Ikomi. Pause. Peg scrute le visage que ses doigts connaissent par cœur à force de le tenir au creux de ses mains. Maquillage subtil, cheveux mauves attachés en arrière et, pour une fois, l'ai reposé et détendu, que Peg n'avait pas vu depuis de trop nombreuses années. 

En arrière plan, la cabine de ce qui doit être un vaisseau. Ou un habitat gravitationnel. Peg laisse l'information faire son chemin dans ses systsèmes émotionnels, dans ses souvenirs, activer les rappels. Les souvenirs, difficiles, durs, mais bien présent. Mais aussi ls bons souvenirs, les moments avant que leur histoire ne soit dévouverte par son agent, et que tout ne devienne que scripts, scandales, poursuites et porns. Mais même au cœur de cet ouragan de souffrance, elles arrivaient à se retrouver toutes les deux, à étabir une connexion, un lien, hors des paramètres, des paparazzis et des fans. Entre les deux portes d'un sas souvent. Le temps d'un couple de respiration, au cœur de la tempete alors que tout autor n'st que frénésie et destructions, elles arrivaient dans ces moments là à être là l'une pour l'autre.

Lecture.

"Bon, c'est probablement la trentième version de ce message. Je sais même plsu comment aborder les choses. Alors on va commencer par la partie importante les détaisl logistiques tu les trouveras plus loin." Profonde inspiratoin d'Ikomi. Peg ne respire plus, elle attends la suite. "Je t'aime. Tu me manques. Je veux te voir. Je ne suis pas sûre de grand chose d'autres dans ma vie que ça. Je veux être avec toi. Je sais aussi que là, je suis un gros tas de bordel à déméler. Et je ne veux pas t'imposer quoi que ce soit. Mais je me dis que tu a ston mot à dire là dedans. Donc voilà. Je veux qu'on se revoit, qu'on reprenne un peu avant cette poursuite. Ou aillurs si tu veux. Mais je comprendrai que tu ne veuille pas." Pause. Peg ne voit plus très net. Des larmes mouillent ses yeux. Bien sûr qu'elle veut la rvoir, la prendre dans ses bras, se sentir bien dans ceux d'Ikomi, s'isoler du monde le laisser brûler sans s'en soucier, au moins l'espace de quelques instants. Et puis … Et puis elles verront la suite.

Essuyant ses larmes, souriant, parcouru de frissons de bonheur, elle relance la lecture. "Dans tous les cas, je me suis remise à la musique. J'ai peur de montrer ce que j'ai fait, donc tu es la première à l'entendre. Tu en fais ce que tu veux, mais j'apprécierai que tu l'écoutes. C'est à propos de moi. De mes différents mois. De nous aussi, un peu. Des connards, beaucoup. Mais c'est moi, mes tripes, mes sentimenst je crois. Et tu en fait ce que tu veux."

La caméra perd le focus quelque secondes, alors qu'Ikomi se penche pour bidouiller quelque chose sur la console. La focale se stabilise, alors que les traits d'Ikomi se précisent. "Je t'aime, au fait. Et si tu veux, je t'ai mis un petit extra, un truc qui t'as manqué je suis sûre. Et, pour une fois c'est juste entre toi et moi, personne d'autre n'as ça. Tu déchiffres ça comme d'habitude. Et laissons ces excités de La Horde en dehors de ça."

Deux fichiers joints. Un appelé Fuck My Lives qui contient diverses pistes musicales. Un autre appelé Fuck me, contenant un XClip. L'enveloppe du message contient un code de transpondeur pour un vaisseau non enregistré dans la base locale, mais ce n'est pas vraiment ce qui intéresse Peg maintenant.  Ce qu'elle veut c'est le contenu du message chiffré. La clef de chiffrement devrat être celles qu'elles utilisaient pour se laisser de petits messages dans les rares espaces d'intimitś qu'elles arrivaient à négocier. Un petit token qu'elles se sont échangées et qu'elles ont parvenu à dissimuler aux images, ou à le cacher. Un motif apparemment anodin codé dans la trame des tatouages presque symétrique qu'elles ont toutes les deux sur l'avant bras. Le dessin de deux couleuvres entrelacés l'une autour de l'autre, leurs écailles, et l'alternance des motifs tantôt verticaux tanttôt horizontaux définissant l'encodage de coordonnées astronomique, de l'endroit où elles se sont croisées pour la première fois.

Tatouages qu'elles se sont faites elle-même, Peg exploitant une des crises scriptées pour profiter de cette opportunité et laisser cette paire de clef dans leur peau, inaltérable et inaltérée. 

Elle attrape le connecteur de sa console et l'enfiche dans sa broche neurale de poignet, témoignage de sa vie au milieu des spaceux qui préfèrent leur broche implantée sous les moniteurs de poignets qu'ils portent tous et par opposition aux puiseux qui préfèrent les leurs proches de leur tête. Elle attache son harnais à une des sangles de fixation qui parcourt son habitat avant de lancer la lecture du XClip, pas moyen qu'elle dérive trop ou qu'elle doivent faire attention à son environnement.

Lecture. Enregistrement.
