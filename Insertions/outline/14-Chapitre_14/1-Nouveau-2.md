title:          Nouveau_2
ID:             61
type:           md
compile:        2
charCount:      5728



"Hey meuf, bien sûr que je veux te voir. J'ai demandé aux nanas avec qui on se construit un glisseur, et on devrait pouvoir se trouver du côté de 4-Vespa. Je crois que c'est par là que tu te diriges.

"Et sinon, j'aime beaucoup ce que tu as écris. Et je suis pas la seule apparemment. Bon, il est pas improbable qu'on se prenne encore des emmerdes avec Shiitake Overdrive, mais on les emmerde.

"On se revoit bientôt, t'inquiètes pas." Peg ajoute son XClip à sa réponse. La baise à ce genre de distance implique un manque de spontanéité, mais à défaut de mieux, les XClips font des merveilles. Et pour une fois, elles sont les seules pariipante et témoins de ces échanges. De nombreux routeurs, relais, serveurs de sauvegarde et admin verrons passer les échanges, et pourront déduire qu'elles baisent. Mais leurs émotions sont protégées par les clefs qu'elles seules savent trouver. L'initimité à travers plusieurs unités astronomiques, alors qu'elles en ont été globalement privées lorsqu'elles n'étaient qu'à quelques centimètres l'une de l'autre.

Au moment où elle envoie le message, à destination du code de transpondeur du Unknown Gravity Dog — la base de donnée a été mise à jour par Echo — la chanson Strings of my Thoughts, extraite de Fuck My Lives, excite les membranes de l'OpEx-2 qui sert, pour le moment, de micro-chantier d'assemblage à la configuration Sany qui doucement prend forme. Elle glisse vers le Zénith du futur vaisseau, à la rencontre d'Echo et de Kam, leur annoncer qu'elle aura besoin d'n peu de place pour elle, et d'un autre module pour Ikomi.

Strings of my Thoughts fait parti du top 10 ds écoutes sur LNR. Pour remplacer les shows permanents des médiacorps, une collection de curateurs divers proposent depuis peu des contenus diffusé par et pour le système solaire. Certains animteurs ou équipes d'animateurs se développant ne petite notoriété au sein des équipages culturels qui travaillent à assembler une collections de communautés sur les restes de KRI. Kam et Echo sont particulièrement fan de Vulva Exquisita aui alterne les morceaux ultra dansants avec des choses beaucou plus psychés et de nombreuses parodies et satires, reprenant des segments de show des puits pour les tourner au ridicule. La portée politique est pas énorme, mais il faut admettre que c'est particulièrement drôle, ce qui facilite le travail d'asseblage et de construction.

"T'es sûre qu'on peut alimenter tout ça avec une si petite centrale ?". Kam a encore des doutes sur la configuration Sany, et elle ne cesse de les exprimer lorsqu'elles se réunissent avec Echo pour discuter de la marche à suivre. Et la discussion commence à user Echo et, aussi, Peg, qui entre à ce moment dans la pièce centrale du module OpEx-2.

"Oui, je suis sûre. Le principe c'est que, justement, c'est une config NPE. T'aime pas l'idée de sortir un réacteur nucléaire. Mais sur  une si petite config, pour sortir la puissance requise pour le Vasimir, c'est le seul choix possible. C'est la base de la config Sandy. Et ça nous permet d'économiser sur le bus de puissance pour les pétales."

La conversation dure un peu, mais n'avance pas. Kam proteste plus pour l'attention que par réelle incomprension. Cette convesation et un peu leur routine. Ça met Peg toujours un peu mal à l'aise, mais elle sait qu'elles ne sont pas en colère l'une contre l'autre. juste un peu fatiguée. Si elle veulent atteindre 4-Vesta de manière relativement économe, elles doivent finir leur configuration la tester et la démarrer dans les trois prochaines semaines, et ça leur fait un planning un peu serré, surtout qu'il faut essayer d'équilibrer la fleur centrale et donc trouver au moins des paires de modules identiques, et docn procéder à de nombreuses discussions et échanges avec d'autres collectifs. Ce qui épuise Kam.

"Hey, les filles. Désolée d'interrompre votre conversation." ou pas, pense Peg, "Juste pour vous dire que, si votre invitation tiens toujours, je pars avec vous si ça vous va. Et que j'ai aussi trouvé une seconde paire de module PIS-H. Ça devrait compléter la config."

"Cool", répond Kam, laissant échapper un soupir de soulagement. Leur config est bouclée, il ne reste plus qu'à assembler. Mais le plus dur est fait. "Tu peux les amener quand ?"

"Nyasid s'en occupe. Quelques heures ou un peu plus si vous préférez attendre."

"Dans … une trentaine d'heure, ça m'arrange" répond Echo, glissant un bisou dans le cou de Peg. "Ça nous laisse le temps de nous poser un peu. On en a besoin je pense." Dit-elle, énonçant l'évidence.

"Par contre … Il est pas improbable qu'il falle garder un module pour Ikomi."

"On s'en doutait" lui dit Kam, glissant pour se rapprocher d'Echo et la prendre dans ses bras. "Il y a proablement que toi qui ne le savait pas encore. De toutes façon, on a de la place pour pas mal de monde encore. Si Nyasid veut venir, on lui garde un mod au chaud."

Nyasid. Peg n'est pa certain qu'il accepterait de venir, mais il est évident qu'il cherche à partir d'ici. Un peu comme tout le monde. Et il est vrai qu'elle s'entend bien avec lui. Kam aussi s'entend particulièrement bien avec lui. Elle essaye de reconstruire un pod un peu plus large autour d'Echo qui, depuis la mort de Shi, semble se contenter de simplement avancer en suivant le mouvement, cachant une forme de dérpime derrière une fausse assurance qui lui permet de ne pas se remettre en question.

Vivre avec elles ne va pas être facile. Mais ce n'est jamais facile. Et Peg préfère ça à se retrouver seule comme les distants. Et elle aime bien Echo. Pas autant que l'attachement qu'elle a à Ikomi, mais elles ont définitivement une connexion, quelque chose qui la tire vers elle, comme absorbée par l'énergie gravitationnelle d'un trou noir.