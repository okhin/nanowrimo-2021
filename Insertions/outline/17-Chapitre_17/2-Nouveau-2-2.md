title:          Nouveau_2_2
ID:             74
type:           md
compile:        2
charCount:      7332



Quatre heures de scan, à attendre dans une combi lourde pour finalement trouver que ce n'était pas une fuite, mais un capteur de pression défaillant. Ou plutôt devenu un peu vieux, et glissant progressivement vers la désactivation. Transpirante — le système interne des combis qu'elle taxe à Ark et Hapse n'est pas calibré sur son métabolisme — et vu que le sas antirad est safe, elle se débarasse de l'encombrante tenue et, simplement vétue du harnais qu'elle ne quitte plus vraiment depuis qu'elle est sortie de l'infirmerie de l'Unknown Artist, d'un baudrier assorti et d'un boxer fort comfortable, elle entreprend de se faufiler dans les étroits interstices du vaisseau, à la recherche du capteur, fouillant dans les gaines techniques abritant différents faisceaux de cables.

Les polymères des systèmes techniques et les métaux anodisés caressent sa peau, alors qu'elle fouille toujours plus profondément avant de localiser l'appareil défectueux. La moitié supérieure de son corps est enfoncée profondément dans le fatras de gaines offrant à Ark et Hapse, qui viennent d'entrer dans le sas à sa recherche, une vue plongeante sur ses jambes et ses hanches dépassant de la plaque de visite qu'elle a démonté et duquel jaillissent, comme d'étranges tentacules, entre les jambes couvertes des petites ocupures que tout mécanicien finit par accumuler, les gaines contenant les câbles des systèmes auxiliaires du sas antirad.

"Dis, il faut qu'on parle d'un truc tous les trois" dit Hapse, après avoir réussit à contrôler un four-rire face à la situation.

"Donnes moi juste deux minutes, le temps de rebrancher ce capteur stupide, et je suis à vous." Ikomi, du bout de ses doigts, finit par enclencher le détrompeur du connecteur DF et le capteur de remplacement bascule en ligne, affichant une lumière verte de validation et mettant à jour son status dans les systèmes de supervision du vaisseau.

Glissant hors de son étui de câble, illustrant une certaine souplesse et conscience corporelle, Ikomi s'extrait de la trappe de visite et s'assied sur le sol métallique et rugueux. "Bon, de quoi voulez-vous parler ?"

"De Fuck my Lives. Ou plutôt de ses conséquences." Hapse est en retrait, il parle pour lui, mais pas que. "Tu es dans la merde, pour faire court. Et il y a au moins trois mouvemement d'auto-détermination qui considèrent que l'album est leur manifeste."

"D'accord. Et ? Qui y a t-il a discuter ?" Demande Ikomi, adossée à la chambre du réacteur à fusion.

"Comment tu comptes survivre à ça. Parce que tu compte survivre à ça, non ?" Demande Ark qui semble avoir la tête claire pour la première fois depuis pas mal de temps.

"Ça dépend de ta définition de survie je suppose. Je replonge pas avec la horde et ce genre de plan. Je veux être le leader de personne."

"On sait ça, reprend Hapse, ce qu'onveut savoir c'est ce que tu compte faire quand on va franchir la ceinture. Parce que Shiitake a obtenu un mandat de saisie du matériel copyrighté, et il me semble que ça concerne ta personnalité. Et, avant que tu ne dises quoi que ce soit, ils ont déjà commandité Sigma pour ça. Par contre leurs sphères sociales sont saturées d'outrage."

"Pas que à ton sujet d'ailleurs, ajoute Ark. Il s'avère que tu n'est clairement pas la seule victime de leur système de production d'idole. Ils ont d'ailleurs été obligé de retirer leur Ikomi, ton album l'ayant complètement oblitérée hors de la sphère média."

Ark et Hapse sont de nouveaux dans leur routine de proximité mentale, quelque chose dont ils n'avaient pas fait l'expérience depuis longtemps et qui leur fait du bien. Ikomi le voit aux détails subtils, à leur respiration synchronisé, au fait qu'ils n'ont pas besoin de se regarder pour savoir quand ils ont finis de parler, comme s'ils partageaient un lien télépathique.

"Bien fait pour leur gueule." répond Ikomi, un sourire sur les lèvres. Une forme de soulagement la traverse rapidement.

"On est d'accord. mais toi, tu veux faire quoi ?" Demande Hapse.

"Moi ? Je veux retrouver Peg. C'est à peu près tout. Je peux pas être une meneuse. Pas moyen. Mais rien ne se mettra plus entre moi et Peg. Ni une horde de fan qui veut que je mène leur révolution, pas un studio, pas des mercenaires, pas vous non plus."

"On est avec toi, la rassure Ark. On voulais juste être sûrs que c'était ce que tu voulais."

"Et on est avec toi. Au moins jusque 4-Vesta. Que ce soit clair. On va activer quelaues contacts perso, parce qu'il faudra bien mettre quelque chose sur la route des Sigmans, et il va falloir aussi — et je sais que tu ne le veux pas mais des personnes on fait ce choix à ta place — que tu adresses cette position de meneuse.

"Et ça se gère. Il y a des mèmeplexe complets construits autour de ça. Il te suffit de les activer. Mais il nous paraît important, et ce nous s'étend au-delà de ce simple équipage, que tu adresse ce sujet avant qu'on arrive aux premiers astéroïdes intérieurs."

"Je peux avoir des détails sur ce nous ?" demande Ikomi.

"Pas vraiment. C'est … un groupe de personnes qui travaillent à faire circuler les informations et à aider à la coordination. Quelque chose entre la coop d'Ark et Love and Rage. C'est surtout une liste de diffusion, que tu es bienvenue à joindre si l'envie t'en prends. Mais si tu ne veux pas te retrouver à la tête d'un culte, alors il va pas falloir tarder à agir. Du moins, c'est ce que nous pensons."

"D'accord". Ikomi prend quelques instants pour réfléchir. Devoir adresser le problème une fois pour toutes. Elle sait que ce ne sera jamais vraiment possible et définitif, mais elle peut faire quelque chose de spectaculaire. Et quelque chose qui lui manque aussi.

"On va faire un concert. Et qu'ils viennent me chercher, je les emmerde. Il n'y a que comme ça que je me sent bien et que je peux adresser une foule."

"Ça … ne va pas aller dans le sens de calmer les choses tu sais ?"

"Oui. Et je vois pas pourquoi je devrais calmer le jeu en fait. Ces connards veulent exercer tout leur pouvoir sur moi ? Je suis imortelle. Je vis déjà dans plusieurs personnes, j'ai vécue plusieurs vies, et ils ont abusés de moi dans chacune d'entre elles. Maintenant ils vont cramer. Et ils ne peuvent pas m'empêcher de faire ça. Qu'est-ce qu'il peuvent faire de pire que de vouloir sculpter ma personnalité ?"

"Te tuer, probablement", répond Ark, laconique.

"Je suis immortelle. Ils me tuent, ils ne font que me renforcer. Et moi j'en aurai finit avec eux et cette vie. Qu'ils viennent, je n'ai plus peur. Je n'aurai plus peur."

Hapse sourit. Il sait qu'il n'est plus vraiment possible de faire changer Ikomi d'avis et, quelque part, cette idée l'amuse. Et, qui sait, ça peut leur permettre de se rappeler comment c'étqit d'être en colère. Ark a peur. Il sait qu'elle a peur et que sa façon de le cacher c'est de prendre les devants. Il sait qu'elle devrait pouvoir faire pareil, mais la peur est plus grande.

"On va faire ça bien. On va trouver des gens pour bosser avec toi si tu veux. Tu choisis. Bref, c'est ton concert. Nous on s'occupe du reste. Les détails orbitaux, ce genre de choses. Toi tu fais ce que tu fais de mieux. De la musique. Et, étrangement, de la mécanique. Hapse tend une main vers Ikomi qui la saisit et, tirant de toute ses forces sur son bras, se redresse dans la gravité de l'accélération d'encore une autre correction de trajectoire.