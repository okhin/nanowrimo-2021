title:          Nouveau_2
ID:             73
type:           md
compile:        2
charCount:      2537



Le sas anti-rad, et le reste du vaisseau jusqu'à la poupe et les buses des arc-jets est une partie du vaisseau dans laquelle Ikomi passe peu de temps. Personne n'y passe beaucoup de temps de manière générale, mais Ikomi en particulier n'y viens que peu. Le sas, contrairement à la plupart des sas à double portes omniprésent dans les divers habitats, est constitués d'épaisse porte dépourvue de hublot. Il est également en légère surpression, afin de pouvoir expulser vers l'arrière du vaisseau tout débris en cas de dépressurisation violente. Ce qui nécessiterait, tout de même, de percer quelques décimètres d'alliages.

Mais le cœur de la fusion au Deutérium reste chaud. Suffisament haud pour, probablement, rendre inutile ces précuations. Ce qui explique les écrous explosifs présent dans le sas. Il vaut mieux détacher le réacteur en cas de problème et de survivre en autonomie sur les batteries de ce qui sera devenu un radeau dérisoire, que de tenter de contenir la fusion qui serait, alors, hors de contrôle.

Et quelque part, ce sont ces écrous explosifs qui la mette mal à l'aise quand elle vient ici. Un des nombreux rappel que, malgrès toutes les merveilles d'ingénierie déployées, il est nécessaire de prévoir un tel mécanisme d'une rudimentaire violence pour gagner quelques pourcent de chance de survie de ce qui serait un accident hautement improbable.

Se détachant mentalement de ces évocations de potentielles apocalypse, elle se concetre sur la tâche en cours. Trouver sur les quelques dizaines de mètres carrés de surface des parois externe du sas, la micro fuite qui laisse échapper du précieux mélange d'Helium et d'Oxygène. La fuite est trop petite pour créer un courant suffisament fort pour observer la dérive de fumée ou de nuage de poussière et l'observer à l'œil nu.

En revanche un scan de type sonar à onde courte permet de voir immédiatement où se trouve une fuite, l'onde électromagnétique utilisée n'étant jamais réfléchit, elle apparaît comme un point noir sur un scan télémétirque. La difficulté est de trouver un point depuis lequel scanner depuis lequel aucun objet ne se trouve entre la fuite et le scanner, nécessitant de procédr avec méthode.

C'est donc sur une vis d'archimède téléscopique qu'Ikomi a monté une caméra EM sur toute la longueur du sas et, elle attend que celui-ci ait balayé l'ensemble du module, progressant milimèter après milimètre, de la proue vers la poupe, effectuant à chaque pas une rotation complète et scrutant de son œil magnétique les échos de ses cris ultrasonique.
